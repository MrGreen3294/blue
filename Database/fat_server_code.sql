# HARD TABLE AND AUTO_INCREMENT RESET
DELETE FROM
            bluedb.usuario_roles
WHERE 1=1;

DELETE FROM
            bluedb.codigo_generico
WHERE 1=1;

DELETE FROM
            bluedb.usuario
WHERE 1=1;

DELETE FROM
            bluedb.rol
WHERE 1=1;

DELETE FROM
            bluedb.usuario_datos_adicionales
WHERE 1=1;

ALTER TABLE
    bluedb.usuario_datos_adicionales
AUTO_INCREMENT = 1;

ALTER TABLE
    bluedb.usuario_roles
AUTO_INCREMENT = 1;

ALTER TABLE
    bluedb.usuario
AUTO_INCREMENT = 1;

ALTER TABLE
    bluedb.rol
AUTO_INCREMENT = 1;

ALTER TABLE
    bluedb.codigo_generico
AUTO_INCREMENT = 1;
-- SHOW PROCEDURE STATUS WHERE Db = 'bluedb';

-- CALL SHOW_USER_NAME();


-- THIS FUNCTION VALIDATE IF THE USER EXIST
SET GLOBAL log_bin_trust_function_creators = 1;
DROP FUNCTION IF EXISTS bluedb.VALIDATE_USER_EXISTENCE;
CREATE FUNCTION bluedb.VALIDATE_USER_EXISTENCE(
    IDUsuario INT(11)
)
RETURNS INT
BEGIN
    IF(EXISTS(SELECT * FROM bluedb.usuario
        WHERE usuario.IDUsuario = IDUsuario
        )) THEN
        RETURN 1;
    end if;
    RETURN 0;
END;


-- THIS STORED PROCEDURE ADD THE USER ROLES
DROP PROCEDURE IF EXISTS bluedb.ADD_USER_ROL;
CREATE PROCEDURE ADD_USER_ROL(
    UserID INT,
    RoleID INT
)
begin
    INSERT INTO bluedb.usuario_roles SET
                                  usuario_roles.IDUsuario = UserID,
                                  usuario_roles.IDRol = RoleID;
end;

-- THIS STORED PROCEDURE INSERTS THE USER CODE
DROP PROCEDURE IF EXISTS bluedb.ADD_USER_CODE;
CREATE PROCEDURE ADD_USER_CODE(

V_UserID INT,

V_ValorCodigo VARCHAR(45)
)
begin
    INSERT INTO bluedb.codigo_generico SET
                                    codigo_generico.IDUsuario = V_UserID,
                                    codigo_generico.ValorCodigo = V_ValorCodigo;
end;

-- THIS STORED PROCEDURE INSERTS THE USER ADDITIONAL INFORMATION
-- RULE IN HERE: FOR GOOD PRACTICE, THE TABLE ADDITIONAL_USER_INFORMATION WILL BE AFFECTED AT LEAST WITH ONE VALUE.
-- THE USER ID WILL NOT BE IN HERE IF THERE IS NOT INSERTED DATA.
-- THIS ONE INSERTS THE USER BIRTHDAY
DROP PROCEDURE  IF EXISTS bluedb.ADD_USER_BIRTHDAY;
CREATE PROCEDURE ADD_USER_BIRTHDAY(
V_UserID INT,
V_FechaNacimiento DATE
)
begin
    DECLARE BIRTHDAY DATE;
    -- VALIDATE IF THERE IS ALREADY A RECORD IN INSIDE THIS ID, IF NOT MAKE THE INSERTION
    -- NOTE: FOR UPDATING PURPOSES THIS STORED PROCEDURE IS NOT GOING TO WORK, BECAUSE IS NOT RESPONSIBLE OF THAT TASK.
    IF(EXISTS(
        SELECT FechaNacimiento
        FROM usuario_datos_adicionales
        WHERE IDUsuario = V_UserID
        AND FechaNacimiento IS NOT NULL)) THEN SET BIRTHDAY = NULL;
    ELSE SET
        BIRTHDAY =  V_FechaNacimiento;
        INSERT INTO bluedb.usuario_datos_adicionales SET
                                usuario_datos_adicionales.IDUsuario = V_UserID,
                                usuario_datos_adicionales.FechaNacimiento = BIRTHDAY;
    END IF;
end;

-- AND THIS ONE INSERT THE USER IDENTIFICATION
DROP PROCEDURE IF EXISTS bluedb.ADD_USER_IDENTIFICATION;
CREATE PROCEDURE ADD_USER_IDENTIFICATION(
V_UserID INT,
V_Identificacion VARCHAR(30)
)
begin
        DECLARE IDENTIFICATION INT;
        -- VALIDATE IF THERE IS ALREADY A RECORD IN INSIDE THIS ID, IF NOT MAKE THE INSERTION
        -- NOTE: FOR UPDATING PURPOSES THIS STORED PROCEDURE IS NOT GOING TO WORK, BECAUSE IS NOT RESPONSIBLE OF THAT TASK.
        IF(EXISTS(
            SELECT Identificacion
            FROM usuario_datos_adicionales
            WHERE IDUsuario = V_UserID
            AND Identificacion IS NOT NULL)) THEN SET IDENTIFICATION = NULL;
        ELSE SET
            IDENTIFICATION = V_Identificacion;
        INSERT INTO bluedb.usuario_datos_adicionales SET
                                usuario_datos_adicionales.IDUsuario = V_UserID,
                                usuario_datos_adicionales.Identificacion = IDENTIFICATION;
        END IF;
end;

-- THIS STORED PROCEDURE INSERTS AN USER
DROP PROCEDURE IF EXISTS bluedb.INSERT_USER;
CREATE PROCEDURE INSERT_USER(

V_ApodoUsuario varchar(45),

V_NombreUsuario varchar(45),

V_PrimerApellidoUsuario varchar(45),

V_SegundoApellidoUsuario varchar(45),

V_CorreoElectronicoUsuario varchar(90),

V_AvatarUsuario varchar(900),

V_ClaveUsuario varchar(45)
)
BEGIN

    -- INSERT THE USER INTO THE USER TABLE
    INSERT INTO usuario SET
                           ApodoUsuario = V_ApodoUsuario,
                           NombreUsuario = V_NombreUsuario,
                           PrimerApellidoUsuario = V_PrimerApellidoUsuario,
                           SegundoApellidoUsuario = V_SegundoApellidoUsuario,
                           CorreoElectronicoUsuario = V_CorreoElectronicoUsuario,
                           AvatarUsuario = V_AvatarUsuario,
                           ClaveUsuario = V_ClaveUsuario;
END;

-- THIS STORED PROCEDURE INSERTS AN USER



-- THIS STORED PROCEDURE INSERT ROLES
DROP PROCEDURE IF EXISTS bluedb.INSERT_ROL;
CREATE PROCEDURE INSERT_ROL(
    TIPO_ROL VARCHAR(20)
)
BEGIN
    INSERT INTO rol SET
                        rol.ValorRol = TIPO_ROL;
END;


-- ADDRESS PROCEDURES

-- THIS STORED PROCEDURE INSERTS A COUNTRY
DROP PROCEDURE IF EXISTS bluedb.INSERT_COUNTRY;
CREATE PROCEDURE bluedb.INSERT_COUNTRY(
V_IDPAIS INT,
V_DESCRIPCION VARCHAR(50)
)
BEGIN
    INSERT INTO PAIS SET
                         IDPAIS = V_IDPAIS,
                         DESCRIPCION = V_DESCRIPCION;
end;

-- THIS STORED PROCEDURE INSERTS A PROVINCE
DROP PROCEDURE IF EXISTS bluedb.INSERT_PROVINCE;
CREATE PROCEDURE bluedb.INSERT_PROVINCE(
V_IDPROVINCIA INT,
V_IDPAIS INT,
V_DESCRIPCION VARCHAR(50)
)
BEGIN
    INSERT INTO PROVINCIA SET
                              IDPAIS = V_IDPAIS,
                              IDProvincia = V_IDPROVINCIA,
                              Descripcion = V_DESCRIPCION;
end;

-- THIS STORED PROCEDURE INSERTS A CANTON
DROP PROCEDURE IF EXISTS bluedb.INSERT_CANTON;
CREATE PROCEDURE bluedb.INSERT_CANTON(
V_IDPROVINCIA INT,
V_IDCANTON INT,
V_IDPAIS INT,
V_DESCRIPCION VARCHAR(50)
)
BEGIN
    INSERT INTO CANTON SET
                           IDPAIS = V_IDPAIS,
                           IDProvincia = V_IDPROVINCIA,
                           IDCanton = V_IDCANTON,
                           Descripcion = V_DESCRIPCION;
end;

-- THIS STORED PROCEDURE INSERTS A DISTRICT
DROP PROCEDURE IF EXISTS bluedb.INSERT_DISTRICT;
CREATE PROCEDURE bluedb.INSERT_DISTRICT(
V_IDPAIS INT,
V_IDPROVINCIA INT,
V_IDCANTON INT,
V_IDDISTRITO INT,
V_DESCRIPCION VARCHAR(50)
)
BEGIN
    INSERT INTO DISTRITO SET
                             IDPais = V_IDPAIS,
                             IDProvincia = V_IDPROVINCIA,
                             IDCanton = V_IDCANTON,
                             IDDistrito = V_IDDISTRITO,
                             Descripcion = V_DESCRIPCION;
end;

CALL INSERT_ROL(
        'Administrador'
    );

CALL INSERT_ROL(
        'UsuarioFinal'
    );


CALL INSERT_USER('Administrador',
    'Pablo',
    'Fonseca',
    'Moncada',
    'pablofonsecam2019@gmail.com',
    'https://www.avatar.com/htdx.png',
    'Hx!dA23');

CALL ADD_USER_CODE(1, 'S12D4F');


Call ADD_USER_BIRTHDAY(1, '2001-05-14');
CALL ADD_USER_IDENTIFICATION(1,  '118110145');
CALL ADD_USER_BIRTHDAY(1, '2002-06-12');
CALL ADD_USER_IDENTIFICATION(1, '125547212');
SELECT * FROM Usuario;



# The following SQL statement selects all customers, and all orders:
#
# SELECT Customers.CustomerName, Orders.OrderID
# FROM Customers
# FULL OUTER JOIN Orders ON Customers.CustomerID=Orders.CustomerID
# ORDER BY Customers.CustomerName;
CREATE OR REPLACE VIEW FIRST_USER_VIEW AS
SELECT * FROM usuario;

-- CALL THE USER VIEW

SELECT * FROM FIRST_USER_VIEW;

SELECT VALIDATE_USER_EXISTENCE(1);

CALL INSERT_COUNTRY(599, 'Curacao');
CALL INSERT_PROVINCE(02, 506, 'Alajuela');
CALL INSERT_CANTON(03, 205, 506, 'Atenas');
CALL INSERT_DISTRICT(506, 02, 205, 20503, 'Mercedes');

-- THIS SELECT GETS THE COUNTRY, THE PROVINCE, THE CANTON AND THEIR DISTRICTS
SELECT CONCAT(pa.Descripcion, ', ', pr.Descripcion, ', ', ca.Descripcion, ', ', di.Descripcion) AS DIRECCION
FROM pais AS pa, provincia AS pr, canton AS ca, distrito AS di
WHERE pa.IDPais = pr.IDPais AND
      pa.IDPais = ca.IDPais AND
      pa.IDPais = di.IDPais AND
      pr.IDProvincia = ca.IDProvincia AND
      di.IDProvincia = ca.IDProvincia;

-- THIS SELECT GETS THE COUNTRY, THE PROVINCE, THE CANTON AND THEIR DISTRICTS. THE INFORMATION IS CONCATENATED
SELECT pa.Descripcion AS PAIS, pr.Descripcion AS PROVINCIA, ca.Descripcion AS CANTON, di.Descripcion AS DISTRITO
FROM pais AS pa, provincia AS pr, canton AS ca, distrito AS di
WHERE pa.IDPais = pr.IDPais AND
      pa.IDPais = ca.IDPais AND
      pa.IDPais = di.IDPais AND
      pr.IDProvincia = ca.IDProvincia AND
      di.IDProvincia = ca.IDProvincia;

-- THIS SELECT ALL THE COUNTRIES
SELECT pais.Descripcion FROM PAIS;

-- THIS SELECTS ALL THE PROVINCES OF ONE COUNTRY
SELECT provincia.Descripcion
FROM provincia
WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = 'Costa Rica');

-- THIS SELECTS ALL THE CANTONS OF ONE PROVINCE
DESC canton;

SELECT canton.Descripcion
FROM CANTON
WHERE canton.IDProvincia IN (SELECT IDProvincia FROM provincia WHERE provincia.Descripcion = 'Alajuela');

-- THIS SELECTS ALL THE DISTRICTS OF ONE CANTON
SELECT distrito.Descripcion
FROM distrito
WHERE distrito.IDCanton = (SELECT IDCanton FROM canton WHERE canton.Descripcion = 'Atenas');

-- THIS IS AN EXAMPLE
SET @COUNTRY_ID = (SELECT IDPais from pais WHERE Descripcion = 'Costa Rica');
CALL INSERT_PROVINCE('1', @COUNTRY_ID, 'San José');

SELECT * FROM USUARIO;
CALL ADD_USER_IDENTIFICATION(2, '118110145');
SELECT * FROM usuario_datos_adicionales;


 CALL INSERT_USER('?',
     '?',
     '?',
     '?',
     '?',
     '?',
     '?');

    INSERT INTO usuario SET
                           ApodoUsuario = ?,
                           NombreUsuario = ?,
                           PrimerApellidoUsuario = ?,
                           SegundoApellidoUsuario = ?,
                           CorreoElectronicoUsuario = ?,
                           AvatarUsuario = ?,
                           ClaveUsuario = ?;


SELECT * FROM pais;
SELECT IDPAIS from pais WHERE PAIS.Descripcion = 'Costa Rica';

DESC Provincia;

SELECT Descripcion FROM PROVINCIA WHERE IDPais = 506;

SELECT IDProvincia FROM Provincia WHERE IDPais = 506 AND Descripcion = 'Alajuela';

SELECT Descripcion FROM Canton WHERE IDPais = 506 AND IDProvincia = 2;

SELECT Descripcion FROM Canton WHERE IDPais = ? AND IDProvincia = ?;

SELECT IDCanton FROM canton WHERE IDPais = 506 AND IDProvincia = 2 AND Descripcion = 'Atenas';

SELECT IDCanton FROM canton WHERE IDPais = ? AND IDProvincia = ? AND Descripcion = ?;

SELECT Descripcion from distrito WHERE IDPais = 506 AND IDProvincia = 2 AND IDCanton = 205;

SELECT Descripcion from distrito WHERE IDPais = ? AND IDProvincia = ? AND IDCanton = ?;

SELECT IDDistrito from distrito WHERE IDPais = 506 AND IDProvincia = 2 AND IDCanton = 205 AND Descripcion='Mercedes';

SELECT IDDistrito from distrito WHERE IDPais = ? AND IDProvincia = ? AND IDCanton = ? AND Descripcion= ?;

SELECT * FROM usuario;

SELECT IDENTIFICACION FROM usuario_datos_adicionales WHERE IDENTIFICACION = '1181101426';

SELECT IDENTIFICACION FROM usuario_datos_adicionales WHERE IDENTIFICACION = ?;

UPDATE USUARIO SET IDPais = 506,
                        IDProvincia = 3,
                        IDCanton = 205,
                        IDDistrito = 20506
WHERE IDUsuario = 2;

UPDATE USUARIO SET IDPais = ?, IDProvincia = ?, IDCanton = ?, IDDistrito = ?;

UPDATE USUARIO SET IDPais = NULL,
                        IDProvincia = NULL,
                        IDCanton = NULL,
                        IDDistrito = NULL
WHERE IDUsuario = 3;




SELECT * FROM usuario_datos_adicionales;


INSERT INTO usuario_datos_adicionales SET IDUsuario = 2, Identificacion = '118110146', FechaNacimiento = '2001-05-14';


SELECT * FROM rol;

DESC ROL;

INSERT INTO ROL SET ValorRol = ?;



SELECT * from usuario_roles;

SELECT IDUsuario from usuario;

INSERT INTO usuario_roles SET IDUsuario = ?, IDRol = (SELECT IDRol FROM ROL WHERE ValorRol = ?);

DELETE FROM ROL WHERE 1=1; ALTER TABLE ROL AUTO_INCREMENT = 1;
DELETE FROM USUARIO WHERE 1=1; ALTER TABLE USUARIO  AUTO_INCREMENT = 1;
DELETE FROM USUARIO_ROLES WHERE 1=1; ALTER TABLE USUARIO_ROLES AUTO_INCREMENT = 1;
DELETE FROM CODIGO_GENERICO WHERE 1=1; ALTER TABLE CODIGO_GENERICO AUTO_INCREMENT = 1;
DELETE FROM usuario_datos_adicionales WHERE 1=1; ALTER TABLE USUARIO_DATOS_ADICIONALES AUTO_INCREMENT = 1;

INSERT INTO codigo_generico SET IDUsuario = 1, ValorCodigo = 'ABC';
INSERT INTO codigo_generico SET IDUsuario = ?, ValorCodigo = ?;
SELECT ValorCodigo FROM Codigo_generico Where IDUsuario  = 1;
SELECT ValorCodigo FROM Codigo_generico Where IDUsuario  = ?;

SELECT IDCodigo FROM codigo_generico WHERE ValorCodigo = ? AND IDUsuario = ?;

UPDATE Usuario SET ESTADO = 'Y' WHERE IDUsuario = 1;
UPDATE Usuario SET ESTADO = 'Y' WHERE IDUsuario = ?;

SELECT IDUsuario FROM usuario_roles WHERE IDRol = (SELECT IDRol FROM ROL WHERE ROL.ValorRol = 'Administrador');

INSERT INTO usuario_roles SET IDUsuario = ?,  IDRol = (SELECT IDRol FROM Rol WHERE Rol.ValorRol = 'Administrador');

SELECT * FROM USUARIO;
SELECT NombreUsuario FROM USUARIO WHERE ApodoUsuario = 'Pablo Fonseca' AND ClaveUsuario = 'a!A23a' AND Estado = 'Y';
SELECT NombreUsuario FROM USUARIO WHERE ApodoUsuario = ? AND ClaveUsuario = ? AND Estado = 'Y';

SELECT IDUsuario FROM Usuario WHERE IDUsuario = (SELECT IDUsuario FROM usuario_datos_adicionales WHERE Identificacion = '118110145')
AND Estado = 'Y';

SELECT IDUsuario FROM Usuario WHERE IDUsuario = (SELECT IDUsuario FROM usuario_datos_adicionales WHERE Identificacion = ?) AND Estado = 'Y';

-- PARA CONSULTAR EL CORREO ELECTRONICO
SELECT IDUsuario FROM Usuario WHERE CorreoElectronicoUsuario = ? AND Estado = 'Y';

SELECT IDUsuario FROM Usuario WHERE IDUsuario IN (SELECT IDUsuario FROM usuario_datos_adicionales WHERE Identificacion = ?) AND Estado = 'Y';

UPDATE Usuario SET ClaveUsuario = 'helloFr1end' WHERE IDUsuario = 1;
UPDATE Usuario SET ClaveUsuario = ? WHERE IDUsuario = 1;

SELECT NombreUsuario FROM Usuario WHERE IDUsuario = ? AND IDUsuario IN (SELECT IDUsuario FROM Usuario_roles WHERE IDRol = (SELECT IDRol FROM ROL WHERE ValorRol = 'Administrador')) AND Estado = 'Y';

SELECT * FROM USUARIO WHERE ESTADO = 'Y';

DELETE FROM USUARIO WHERE Estado = 'N';
SELECT IDUsuario FROM Usuario WHERE ApodoUsuario = 'PabloFM' AND ClaveUsuario = 'a!As12' AND Estado = 'Y';

SELECT * FROM categoria_video;

SELECT IDCategoria, DescripcionCategoria, NombreCategoria FROM Categoria_video;

SELECT IDCategoria from categoria_video;

SELECT * FROM VIDEO;
SELECT * from usuario;

DESC VIDEO;

INSERT INTO video SET IDVideo = ?, IDUsuario = ?, NombreVideo = ?, DescripcionVideo = ?, FechaCreacionVideo = ?, CalificacionVideo = ?, DuracionTotalVideo = ?, UltimoTiempoVideo =  ?, URLVideo = ?, IDCategoria = ?;

SELECT identificacion from usuario_datos_adicionales where IDUsuario = 9;

Select NombreCategoria from categoria_video;
SELECT video.NombreVideo, video.DescripcionVideo,
       video.FechaCreacionVideo, video.CalificacionVideo,
       cv.NombreCategoria, video.DuracionTotalVideo
FROM video
INNER JOIN categoria_video cv  on video.IDCategoria = cv.IDCategoria
WHERE IDUsuario = ?;

SELECT URLVideo  FROM video WHERE NombreVideo = ? AND IDUsuario = ?;

DESC VIDEO;
SELECT * FROM VIDEO;

DESC lista_videos;

SELECT IDUsuario FROM usuario;
SELECT * FROM tema_lista;
SELECT * FROM lista_videos;

INSERT INTO lista_videos SET Nombre = 'Favoritos', FechaCreacion = DATE('2020-04-08'), Descripcion = 'Mis Videos Favoritos',
                             IDUsuario = 13, IDTema = 1, RutaLista = 'https://www.ruta.com/';

INSERT INTO lista_videos SET Nombre = ?, Descripcion = ?, FechaCreacion = ?, RutaLista = ?, IDUsuario = ?;

UPDATE lista_videos SET IDTema = 1 WHERE IDLista = 5;
#DATOS QUE SE NECESITAN: NOMBRE, DESCRIPCION, TEMA CON COMBOBOX, RUTA CON UN BOTONCITO, CALIFICACION DE ESTRELLAS


DELETE FROM VIDEO WHERE IDUsuario = ? AND NombreVideo = ?;

UPDATE VIDEO SET IDCategoria = ? WHERE IDVideo = ?;

SELECT IDCategoria FROM Video where IDVideo = 43;

SELECT * from categoria_video;

select * from tema_lista;
select * from lista_videos;
select IDVideo from video;
select * from videos_x_lista;

INSERT INTO videos_x_lista SET IDLista = 9, IDVideo = 7;

SELECT IDLista FROM lista_videos WHERE IDUsuario = 16 AND Nombre = 'LOVE_MACHINE';

SELECT Nombre, Descripcion, FechaCreacion, Calificacion, DuracionTotal FROM lista_videos where IDUsuario = ?;



SELECT * FROM video WHERE NombreVideo NOT LIKE 'IRON_MAN.mp4';

SELECT URLVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM videos_x_lista WHERE IDLista = 27);

SELECT DuracionTotalVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM videos_x_lista WHERE IDLista = 33);

SELECT CalificacionVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM videos_x_lista WHERE IDLista = 33);
UPDATE lista_videos SET DuracionTotal = ? WHERE IDLista = ?;

SELECT * FROM lista_videos;

SELECT * FROM TEMA_lista;

UPDATE video set UltimoTiempoVideo = ? WHERE IDVideo = ?;

SELECT UltimoTiempoVideo from video where IDVideo = 43;

SELECT * FROM LISTA_VIDEOS;

SELECT * FROM VIDEO;
SELECT NombreVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM videos_x_lista WHERE IDLista = 39);

SELECT IDUsuario FROM Usuario WHERE Estado = 'Y';
SELECT Nombre FROM lista_videos WHERE IDUsuario = ?;

SELECT * FROM lista_videos WHERE IDUsuario = 16;
DELETE FROM lista_videos WHERE IDUsuario = ? AND Nombre = ?;

SELECT * FROM Usuario ;

DELETE FROM Usuario WHERE IDUsuario = ?;
DELETE FROM video WHERE IDUsuario = ?;
DELETE FROM lista_videos WHERE IDUsuario = ?;


INSERT INTO TEMA_LISTA SET NombreTema = ?, DescripcionTema = ?;
DELETE FROM TEMA_LISTA WHERE NombreTema = ?;

INSERT INTO categoria_video SET NombreCategoria = ?, DescripcionCategoria = ?;

SELECT * FROM categoria_video WHERE 1=1;

SELECT * FROM usuario;
SELECT * FROM pais;

INSERT INTO pais SET Descripcion = ?, IDPais = ?;

DESC provincia;

INSERT INTO Provincia SET Descripcion = ?, IDPais = ?, IDProvincia = ?;

SELECT * from provincia;

DELETE FROM provincia WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = ?) AND provincia.Descripcion = ?;

DELETE FROM canton WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = ?) AND
                         IDProvincia = (SELECT IDProvincia FROM provincia WHERE provincia.Descripcion = ?) AND
                         canton.Descripcion = ?;

DELETE FROM distrito WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = ?) AND
                           IDProvincia  = (SELECT IDProvincia FROM provincia WHERE provincia.Descripcion = ?) AND
                           IDCanton = (SELECT IDCanton FROM canton WHERE canton.Descripcion = ?) AND
                           distrito.Descripcion = ?;


INSERT INTO canton set IDPais = ?, IDProvincia = ?, Descripcion = ?, IDCanton = ?;

INSERT INTO distrito set IDPais = ?, IDProvincia = ?, IDCanton = ?, Descripcion = ?, IDDistrito = ?;

SELECT * FROM Canton;

SELECT * FROM Usuario;

SELECT IDDistrito FROM distrito WHERE 1=1;