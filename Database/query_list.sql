CREATE PROCEDURE SHOW_USER_NAME() 
SELECT NombreUsuario, 
		PrimerApellidoUsuario, 
		SegundoApellidoUsuario 
FROM bluedb.Usuario

-- If you want to show stored procedures in a specific database
SHOW PROCEDURE STATUS WHERE DB = 'bluedb';

-- If you want to call a stored procedure
CALL SHOW_USER_NAME();

