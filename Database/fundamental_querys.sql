USE blue;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM blue.administrador WHERE 1=1;
SELECT * FROM blue.administrador;
ALTER TABLE blue.administrador auto_increment = 1;
DROP TABLE blue.administrador;
ALTER TABLE blue.codigo_generico auto_increment = 1;
DELETE FROM blue.codigo_generico WHERE 1=1;

