#USUARIO

-avatar: String
-claveSeguridad: String
-correoElectronico: String 
-nombreUsuario: String 
-tipoUsuario: String 
 
+Usuario()
+Usuario(String nombre, String primerApellido, String segundoApellido, String avatar, String claveSeguridad, String correoElectronico, String nombreUsuario, String tipoUsuario)
+Usuario(String nombre, String primerApellido, String segundoApellido, String identificacion, int diaNacimiento, int mesNacimiento, int annoNacimiento, String avatar, String claveSeguridad, String correoElectronico, String nombreUsuario, String tipoUsuario)

+getAvatar(): String
+getClaveSeguridad(): String
+getCorreoElectronico(): String
+getNombreUsuario(): String 
+getTipoUsuario(): String 

+setAvatar(String avatar): void
+setClaveSeguridad(String claveSeguridad): void
+setCorreoElectronico(String correoElectronico): void
+setNombreUsuario(String nombreUsuario): void 
+setTipoUsuario(String tipoUsuario): void 

+toString(): String
+equals(Object o): boolean


