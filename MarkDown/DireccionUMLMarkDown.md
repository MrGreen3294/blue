#DIRECCION

#pais: String
#provincia: String
#canton: String
#distrito: String

+Direccion()
+Direccion(String provincia, String canton, String distrito)

+getPais(): String
+getProvincia(): String
+getCanton(): String
+getDistrito(): String

+setPais(String pais): void
+setProvincia(String provincia): void
+setCanton(String canton): void
+setDistrito(String distrito): void

+toString(): String
+equals(Object o): boolean 
