#PERSONA 

#nombre: String 
#primerApellido: String 
#segundoApellido: String
#fechaNacimiento: LocalDate
#identificacion: String
#edad: int 

direcciones: ArrayList<Direccion>

+Persona()
+Persona(String nombre, String primerApellido, String segundoApellido)
+Persona(String nombre, String primerApellido, String segundoApellido, String identificacion, int diaNacimiento, int
mesNacimiento, int annoNacimiento)

+getNombre(): String
+getPrimerApellido(): String
+getSegundoApellido(): String
+getFechaNacimiento(): LocalDate 
+getIdentificacion(): String
+getEdad(): int

+setNombre(String nombre): void
+setPrimerApellido(String primerApellido): void
+setSegundoApellido(String segundoApellido): void
+setFechaNacimiento(LocalDate fechaNacimiento): void
+setIdentificacion(String identificacion): void

+toString(): String 
+equals(Object o): boolean

-calculatePersonAge(LocalDate birthday): int 
-formatBirthday(int year, int month, int day): LocalDate


