package cr.ac.ucenfotec.UI.LifeCycles;

import cr.ac.ucenfotec.TL.ControllerAdministrador;
import cr.ac.ucenfotec.TL.ControllerRol;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXApplicationBoot extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    //This two variables are used to get the x and y axis position in the screen.
    private double xOffset = 0;
    private double yOffset = 0;

    //Variable temporal que será utilizada para analizar el estado de los roles al inicio de la aplicación.
    ControllerRol controllerRol = new ControllerRol();

    //Variable temporal que será utilizada para analizar el estado del administrador al inicio de la aplicación.
    ControllerAdministrador controllerAdministrador = new ControllerAdministrador();

    //Constantes que contendrán el nombre de los roles necesarios a implementar en el sistema.
    final String USUARIO_FINAL = "UsuarioFinal";
    final String ADMINISTRADOR = "Administrador";

    final Boolean CICLO = true;
    @Override
    public void start(Stage primaryStage) {

        if (CICLO) {
            try {

                //Primero se valida si existe un administrador.
                //En caso de que no exista uno, se obliga al usuario a comenzar con el registro de una cuenta.
                //En este caso la portada de inicio será SitioInicioFXML.
                Parent root;
                if (!controllerAdministrador.validarExistenciaAdministrador()) {
                    root = FXMLLoader.load(getClass().getResource("../ApplicationViews" +
                            "/SitioInicioFXML" + ".fxml"));
                } else {
                    root = FXMLLoader.load(getClass().getResource("../ApplicationViews" +
                            "/IniciarSesionUsuarioFXML.fxml"));
                }
                //When the root is clicked the y and x variable gets the x and y position of the mouse.
                //Lambda Expressions.
                root.setOnMousePressed(mouseEvent -> {
                    xOffset = mouseEvent.getSceneX();
                    yOffset = mouseEvent.getSceneY();
                });

                //When the root is dragged set the position to the new x and y position of the mouse.
                root.setOnMouseDragged(mouseEvent -> {
                    primaryStage.setX(mouseEvent.getScreenX() - xOffset);
                    primaryStage.setY(mouseEvent.getScreenY() - yOffset);
                });

                /*
                 * Se valida que los roles estén insertados en la base de datos, si no es así se insertan los dos que
                 * usará la aplicación.
                 */
                boolean rolExiste = controllerRol.validarExistenciaRol(USUARIO_FINAL);

                /*
                 * Si el rol usuario final no existe se inserta en la base de datos.
                 */
                if (!(rolExiste)) {
                    controllerRol.insertarRol(USUARIO_FINAL);
                }

                /*
                 * Se valida que el rol del administrador esté insertado en la base de datos, si no es así se inserta.
                 */
                rolExiste = controllerRol.validarExistenciaRol(ADMINISTRADOR);

                /*
                 * Si el rol Administrador no existe se inserta en la base de datos.
                 */
                if (!(rolExiste)) {
                    controllerRol.insertarRol(ADMINISTRADOR);
                }
                fullScreenClicksFunctionality(primaryStage, root);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            //Solo se utiliza para realizar pruebas en escenas específicas.
            //No sigue el ciclo del programa.
            try{
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelRolAdmin.fxml"));

                //When the root is clicked the y and x variable gets the x and y position of the mouse.
                //Lambda Expressions.
                root.setOnMousePressed(mouseEvent -> {
                    xOffset = mouseEvent.getSceneX();
                    yOffset = mouseEvent.getSceneY();
                });

                //When the root is dragged set the position to the new x and y position of the mouse.
                root.setOnMouseDragged(mouseEvent -> {
                    primaryStage.setX(mouseEvent.getScreenX() - xOffset);
                    primaryStage.setY(mouseEvent.getScreenY() - yOffset);
                });

                fullScreenClicksFunctionality(primaryStage, root);

            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void fullScreenClicksFunctionality(Stage primaryStage, Parent root) {
        Scene initScene = new Scene(root);

        initScene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getClickCount() == 2){
                    primaryStage.setFullScreen(true);
                }else if(mouseEvent.getClickCount() == 3){
                    primaryStage.setFullScreen(false);
                }
            }
        });
        primaryStage.setScene(initScene);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }
}
