package cr.ac.ucenfotec.UI.ApplicationCustomClasses;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import jdk.dynalink.support.SimpleRelinkableCallSite;

/**
 * Clase que será utilizada única y exclusivamente por el controller para manejar los datos de tipo ListaReproduccion,
 * esta clase solo implementa las propiedades necesarias de una lista, haciendo que el acceso de la clase lista
 * no se realice en el controller de JavaFx, en su lugar se utilizará esta clase que tiene la responsabilidad
 * de manejar los datos necesarios para un Table View.
 * @author Pablo Fonseca.
 * @version 1.0
 *
 */
public class PlayListModelTable {

    private StringProperty nombre;
    private StringProperty descripcion;
    private StringProperty fechaRegistro;
    private StringProperty calificacion;
    private StringProperty tema;
    private StringProperty duracion;

    public PlayListModelTable(String nombre, String descripcion, String fechaRegistro, String calificacion, String duracion) {
        this.nombre = new SimpleStringProperty(this, "nombre", nombre);
        this.descripcion = new SimpleStringProperty(this, "descripcion", descripcion);
        this.fechaRegistro = new SimpleStringProperty(this, "fechaRegistro", fechaRegistro);
        this.calificacion = new SimpleStringProperty(this, "calificacion", calificacion);
        this.duracion = new SimpleStringProperty(this, "duracion", duracion);
    }

    public PlayListModelTable(String nombre, String descripcion, String fechaRegistro, String calificacion,
                              String tema, String duracion) {
        this.nombre = new SimpleStringProperty(this, "nombre", nombre);
        this.descripcion = new SimpleStringProperty(this, "descripcion", descripcion);
        this.fechaRegistro = new SimpleStringProperty(this, "fechaRegistro", fechaRegistro);
        this.calificacion = new SimpleStringProperty(this, "calificacion", calificacion);
        this.tema = new SimpleStringProperty(this, "tema", tema);
        this.duracion = new SimpleStringProperty(this, "duracion", duracion);

    }

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public StringProperty descripcionProperty() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public String getFechaRegistro() {
        return fechaRegistro.get();
    }

    public StringProperty fechaRegistroProperty() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro.set(fechaRegistro);
    }

    public String getCalificacion() {
        return calificacion.get();
    }

    public StringProperty calificacionProperty() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion.set(calificacion);
    }

    public String getTema() {
        return tema.get();
    }

    public StringProperty temaProperty() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema.set(tema);
    }

    public String getDuracion() {
        return duracion.get();
    }

    public StringProperty duracionProperty() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion.set(duracion);
    }
}
