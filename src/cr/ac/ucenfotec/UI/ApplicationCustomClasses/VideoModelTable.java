package cr.ac.ucenfotec.UI.ApplicationCustomClasses;

import javafx.beans.property.*;

/**
 * Clase que será utilizada única y exclusivamente por el controller para manejar los datos de tipo Video,
 * esta clase solo implementa las propiedades necesarias de un video, haciendo que el acceso de la clase video
 * no se realice en el controller de JavaFx, en su lugar se utilizará esta clase que tiene la responsabilidad
 * de manejar los datos necesarios para un Table View.
 * @author Pablo Fonseca.
 * @version 1.0
 *
 */
public class VideoModelTable {


    private StringProperty nombre;
    private StringProperty descripcion;
    private StringProperty fechaRegistro;
    private StringProperty calificacion;
    private StringProperty categoria;
    private StringProperty duracion;

    public VideoModelTable(String nombre, String descripcion, String fechaRegistro, String calificacion,
                           String categoria, String duracion) {
        this.nombre = new SimpleStringProperty(this, "nombre", nombre);
        this.descripcion = new SimpleStringProperty(this, "descripcion", descripcion);
        this.fechaRegistro = new SimpleStringProperty(this, "fechaRegistro", fechaRegistro);
        this.calificacion = new SimpleStringProperty(this, "calificacion", calificacion);
        this.categoria = new SimpleStringProperty(this, "categoria", categoria);
        this.duracion = new SimpleStringProperty(this, "duracion", duracion);
    }

    public VideoModelTable(String nombre, String descripcion, String fechaRegistro, String calificacion,
                           String duracion) {
        this.nombre = new SimpleStringProperty(this, "nombre", nombre);
        this.descripcion = new SimpleStringProperty(this, "descripcion", descripcion);
        this.fechaRegistro = new SimpleStringProperty(this, "fechaRegistro", fechaRegistro);
        this.calificacion = new SimpleStringProperty(this, "calificacion", calificacion);
        this.duracion = new SimpleStringProperty(this, "duracion", duracion);
    }

    //Getters

    public String getNombre() {
        return nombre.get();
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public StringProperty descripcionProperty() {
        return descripcion;
    }

    public String getFechaRegistro() {
        return fechaRegistro.get();
    }

    public StringProperty fechaRegistroProperty() {
        return fechaRegistro;
    }

    public String getCalificacion() {
        return calificacion.get();
    }

    public StringProperty calificacionProperty() {
        return calificacion;
    }

    public String getCategoria() {
        return categoria.get();
    }

    public StringProperty categoriaProperty() {
        return categoria;
    }

    public String getDuracion() {
        return duracion.get();
    }

    public StringProperty duracionProperty() {
        return duracion;
    }
}
