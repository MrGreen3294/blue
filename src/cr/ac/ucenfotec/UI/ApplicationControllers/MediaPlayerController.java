package cr.ac.ucenfotec.UI.ApplicationControllers;
import com.jfoenix.controls.JFXSlider;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class MediaPlayerController extends Controller implements Initializable {

    @FXML
    private MediaView mediaView;

    @FXML
    private JFXSlider progressBar;

    @FXML
    private JFXSlider volumeSlider;

    @FXML
    private ImageView playPauseImage;

    @FXML
    private ImageView reloadVideo;

    @FXML
    private Label tiempo;

    @FXML
    private ImageView minimize;

    @FXML
    private ImageView fullscreen;

    @FXML
    private ImageView exit;


    //Variable temporal que va a almacenar el nombre de un video.
    private String nombreVideo = Controller.getNombreVideo();

    //Variable temporal que va a almacenar el ID del usuario.
    private int IDUsuario = Controller.getIDUsuario();

    //Variable temporal que almacena un counter para cambiar el tipo de animación de los botones.
    private static int playSwitch = 0;

    //Variable temporal para almacenar un objeto de tipo media.
    private Media media;

    //Variable temporal para almacenar un objeto de tipo media player.
    private MediaPlayer mediaPlayer;

    //Variable temporal que almacena este stage.
    Stage thisStage;

    //Variable booleana que almacenará el valor de un switcher para implementar el método full-screen
    boolean switcher = false;

    //Constante que establece el path relativo de la imagen para el botón de play.
    final String PLAY_BTN = "src/cr/ac/ucenfotec/UI/Pictures/PLAY_BTN.png";

    //Constante que establece el path relativo de la imagen para el botón de pausa.
    final String PAUSE_BTN = "src/cr/ac/ucenfotec/UI/Pictures/PAUSE_BTN.png";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Se obtiene el URL del video con el nombre.
        String URLVideo = controllerVideo.obtenerURLRelativo(IDUsuario, nombreVideo);

        //Se obtiene el ID del video.
        int IDVideo = controllerVideo.obtenerIDVideo(nombreVideo, IDUsuario);

        //Se indica el path del video
        String MediaURL = new File(URLVideo).getPath();

        //Se genera el archivo
        File archivo = new File(MediaURL);

        //Se convierte el archivo en un path absoluto dependiendo de cada sistema de archivo.
        String URLAbsoluto = archivo.toURI().toString();

        //Se convierte el path absoluto en un objeto de tipo media
        media = new Media(URLAbsoluto);

        //Se construye el objeto de tipo media player
        mediaPlayer = new MediaPlayer(media);

        //Se ajusta en el view del media player.
        this.mediaView.setMediaPlayer(mediaPlayer);

        //Ajuste automático del tamaño del video.
        DoubleProperty mediaViewWidth = mediaView.fitWidthProperty();
        DoubleProperty mediaViewHeight = mediaView.fitHeightProperty();
        try {
            mediaViewWidth.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
            mediaViewHeight.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));
        }catch(Exception ignored){

        }
        mediaView.setPreserveRatio(false);

        //Implementación del volúmen.
        volumeSlider.setValue(mediaPlayer.getVolume() * 100);
        volumeSlider.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                mediaPlayer.setVolume(volumeSlider.getValue() / 100);
            }
        });

        //Se establece el valor por defecto de la barra de progreso.
        progressBar.setValue(0); //Valor por defecto.

        //Se ajusta el movimiento automático de la barra de progreso.
        mediaPlayer.currentTimeProperty().addListener(new ChangeListener<Duration>() {

            @Override
            public void changed(ObservableValue<? extends Duration> observableValue, Duration oldValue,
                                Duration newValue) {
                progressBar.setMax(mediaPlayer.getTotalDuration().toSeconds());
                progressBar.setValue(mediaPlayer.getCurrentTime().toSeconds());
                tiempo.setText(formatTime(Duration.seconds(mediaPlayer.getCurrentTime().toSeconds()),
                        Duration.seconds(mediaPlayer.getTotalDuration().toSeconds())));
            }
        });

        //Se ajusta el cambio manual en segundos de la barra de progreso.
        //HAY QUE HACER ALGO AQUI
        progressBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                //Se guardará en la base de datos el último tiempo de reproducción.
                controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);
                mediaPlayer.seek(Duration.seconds(progressBar.getValue()));
            }

        });

        //Se obtiene el último tiempo de reproducción.
        this.mediaPlayer.setOnReady(new Runnable() {
            @Override
            public void run() {
                mediaPlayer.seek(Duration.seconds(controllerVideo.obtenerUltimoTiempoReproduccion(IDVideo)));
                playPauseImage.setOnMouseClicked(mouseEvent -> {
                    String imagePath;
                    if(playSwitch == 0){
                        //Change Image Path
                        imagePath = new File(PAUSE_BTN).getPath();
                        playSwitch = 1;
                        double ultimoTiempo = controllerVideo.obtenerUltimoTiempoReproduccion(IDVideo);
                        progressBar.setValue(ultimoTiempo);
                        mediaPlayer.seek(Duration.seconds(ultimoTiempo));
                        mediaPlayer.play();
                    }else{
                        //Change Image Path
                        imagePath = new File(PLAY_BTN).getPath();
                        playSwitch = 0;
                        mediaPlayer.pause();
                        //Se guardará en la base de datos el último tiempo de reproducción.
                        controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);
                    }
                    File imageFile = new File(imagePath);
                    String imageFileConversion = imageFile.toURI().toString();
                    Image image = new Image(imageFileConversion);
                    playPauseImage.setImage(image);
                });
            }
        });

        //Se recarga el video cuando se presiona el boton reload.
        this.reloadVideo.setOnMouseClicked(mouseEvent -> {
            mediaPlayer.seek(mediaPlayer.getStartTime());
            double startTimeValue = mediaPlayer.getStartTime().toSeconds();
            controllerVideo.actualizarUltimoTiempoVideo(startTimeValue, IDVideo);
            mediaPlayer.play();
            String imagePath = new File(PAUSE_BTN).getPath();
            File imageFile = new File(imagePath);
            String imageFileConversion = imageFile.toURI().toString();
            Image image = new Image(imageFileConversion);
            playPauseImage.setImage(image);
        });

        //Action Event para minimizar la ventana
        minimize.setOnMouseClicked(mouseEvent -> {
            thisStage = (Stage) minimize.getScene().getWindow();
            thisStage.setIconified(true);
        });

        //Mouse Event para ajustar la ventana en pantalla completa.
        fullscreen.setOnMouseClicked(mouseEvent -> {
            thisStage =  (Stage) fullscreen.getScene().getWindow();
            if(switcher){
                thisStage.setFullScreen(true);
                thisStage.setResizable(false);
                switcher = false;
            }else{
                thisStage.setFullScreen(false);
                switcher = true;
            }
        });

        //Mouse Event para cerra la ventana.
        exit.setOnMouseClicked(mouseEvent ->  {
            thisStage =  (Stage) exit.getScene().getWindow();
            //Se pausa cualquier video
            if(mediaPlayer != null) {
                mediaPlayer.pause();
            }
            //Se guardará en la base de datos el último tiempo de reproducción.
            controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);

            //Se cierra la ventana
            thisStage.hide();
        });
    }

    /**
     * Método que calcula el time line de reproducción de un video.
     *
     */
    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int)Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
                - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int)Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 -
                    durationMinutes * 60;
            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds,durationMinutes,
                        durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d",elapsedMinutes,
                        elapsedSeconds);
            }
        }
    }


}
