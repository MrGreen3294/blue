package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXComboBox;
import cr.ac.ucenfotec.TL.ControllerDireccion;
import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class AgregarDireccionController extends Controller implements Initializable {

    //Atributos de la vista FXML
    @FXML
    private JFXComboBox<String> paises;

    @FXML
    private JFXComboBox<String> provincias;

    @FXML
    private JFXComboBox<String> cantones;

    @FXML
    private JFXComboBox<String> distritos;

    @FXML
    private Button agregarDireccion;



    //Variable que indica si es una ventana del registro de usuario final o una ventana común.
    //Simplemente cuando la dirección se registra va a la ventana para confirmar el código del usuario.
    private static boolean ventanaRegistro; //Por defecto es una ventana común, no una ventana de registro.

    /**
     * Setter que indica si es una ventana del registro de usuario final o una ventana común.
     * @param ventanaRegistro valor booleano.
     */
    public void setVentanaRegistro(boolean ventanaRegistro) {
        AgregarDireccionController.ventanaRegistro = ventanaRegistro;
    }

    /**
     * Getter que retorna el valor de la variable temporal ventanaRegistro
     */
    public boolean getVentanaRegistro(){
        return ventanaRegistro;
    }

    //Variable para enviar información al Controller de la dirección.
    ControllerDireccion controller = new ControllerDireccion();

    //Variable para enviar información al Controller del usuario.
    ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Variable temporal que almacena el ID de un país seleccionado en tiempo de ejecución.
    int IDPais = -1;

    //Variable temporal que almacena el ID de la provincia seleccionada en tiempo de ejecución.
    int IDProvincia  = -1;

    //Variable temporal que almacena el ID del cantón seleccionado en tiempo de ejecución.
    int IDCanton = -1;

    //Variable temporal que almacena el ID del distrito seleccionado en tiempo de ejecución.
    int IDDistrito = -1;

    //Variable temporal que almacena el nombre del país seleccionado.
    String pais;
    //Variable temporal que almacena el nombre de la provincia seleccionada.
    String provincia;
    //Variable temporal que almacena el nombre del cantón seleccionado.
    String canton;
    //Variable temporal que almacena el nombre del distrito seleccionado.
    String distrito;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*
         * Esta sección del código inicializa los JFXComboBox para cada uno de los datos.
         */
        this.paises.setVisibleRowCount(5);
        this.provincias.setDisable(true);
        this.cantones.setDisable(true);
        this.distritos.setDisable(true);


        //En caso de que el usuario presione el ComboBox de los paises, se llamará al método desplegarPaises().
        this.paises.setOnMouseClicked(mouseEvent -> {
            desplegarPaises();
        });

        //En caso de que el usuario seleccione un elemento, se llamará al método solicitarIDPais()
        this.paises.setOnAction(actionEvent -> {
            solicitarIDPais();
        });

        //En caso de que el usuario presione el ComboBox de las provincias, se llamará al método desplegarProvincias.
        this.provincias.setOnMouseClicked(mouseEvent -> {
            if(IDPais != -1)
                desplegarProvincias(IDPais);

        });

        //En caso de que el usuario seleccione un elemento, se llamará al método solicitarIDProvincia()
        this.provincias.setOnAction(actionEvent -> {
            if(IDPais != -1)
                solicitarIDProvincia();
        });

        //En caso de que el usuario seleccione un elemento, se llamará el método desplegarCantones()
        this.cantones.setOnMouseClicked(mouseEvent -> {
                desplegarCantones();
        });

        //En caso de que el usuario seleccione un elemento, se llamará al método solicitarIDCanton()
        this.cantones.setOnAction(actionEvent -> {
            if(IDPais != -1 && IDProvincia != -1)
            solicitarIDCanton();
        });

        //En caso de que el usuario seleccione un elemento se llamará al método desplegarDistritos()
        this.distritos.setOnMouseClicked(mouseEvent -> {
            if(IDPais != -1 && this.IDProvincia != -1 && this.IDCanton != -1){
                desplegarDistritos();
            }
        });

        //En caso de que el usuario seleccione un elemento, se llamará al método solicitarIDDistrito()
        this.distritos.setOnAction(actionEvent -> {
            if(IDPais != -1 && this.IDProvincia != -1 && this.IDCanton != -1) {
                solicitarIDDistrito();
            }
        });

        //En caso de que el usuario seleccione el botón de agregar dirección se llamará al método registrarDireccion()
        this.agregarDireccion.setOnAction(actionEvent -> {
            registrarDireccion();
        });
    }


    /**
     * Despliega los países registrados en la base de datos.
     */
    public void desplegarPaises(){
        //En caso de que este método sea llamado a la hora de estár listando otro elemento, se deben de deshabilitar.
        this.distritos.setDisable(true);
        this.cantones.setDisable(true);
        this.provincias.setDisable(true);
        paises.getItems().clear();
        String[] paisesArray = controller.listarPaises();
        for (String s : paisesArray) {
            paises.getItems().add(s);
        }
        paises.show();
    }

    /**
     * Se encarga de almacenar en una variable temporal el ID del pais seleccionado.
     * Almacena el nombre del país seleccionado en una variable temporal.
     * También se encarga de habilitar el JFXComboBox de Provincias.
     * @author Pablo Fonseca
     */
    private void solicitarIDPais(){
        if(paises.getValue() != null && !(paises.getValue().equalsIgnoreCase(""))){
            //Se establece el nombre del país es una variable temporal.
            this.pais = paises.getValue().trim();
            //Ahora se establece el ID del País en una variable temporal.
            this.IDPais = controller.solicitarIDPais(paises.getValue());
            //Finalmente se deshabilita el JFXComboBox de las provincias
            this.provincias.setDisable(false);
        }
    }

    /**
     * Despliega las provincias del país seleccionado en la base de datos.
     * @author Pablo Fonseca
     */
    public void desplegarProvincias(int IDPais){
        this.distritos.setDisable(true);
        this.cantones.setDisable(true);
        provincias.getItems().clear();
        String[] provinciasArray = controller.listarProvincias(IDPais);
        for (String s : provinciasArray) {
            provincias.getItems().add(s);
        }
        provincias.show();
    }

    /**
     * Se encarga de almacenar en una variable temporal el ID de la provincia seleccionada.
     * Almacena el nombre de la provincia seleccionada en una variable temporal.
     * También se encarga de habilitar el JFXComboBox de Cantones.
     * @author Pablo Fonseca.
     */
    public void solicitarIDProvincia(){
        if(provincias.getValue() != null && !(provincias.getValue().equalsIgnoreCase(""))){ ;
            //Se almacena el nombre de la provincia en una variable temporal.
            this.provincia = provincias.getValue().trim();
            //Ahora se establece el ID de la provincia en una variable temporal.
            this.IDProvincia = controller.solicitarIDProvincia(this.IDPais, provincias.getValue());
            //Finalmente se deshabilita el JFXComboBox de los distritos.
            this.cantones.setDisable(false);
        }
    }

    /**
     * Despliega los cantones de una provincia de un país seleccionados en la base de datos.
     * @author Pablo Fonseca.
     */
    public void desplegarCantones(){
        this.distritos.setDisable(true);
        this.cantones.getItems().clear();
        String[] cantonesArray = controller.listarCantones(IDPais, IDProvincia);
        for(String s: cantonesArray){
            this.cantones.getItems().add(s);
        }
        cantones.show();
    }

    /**
     * Se encarga de almacenar en una variable temporal el ID del cantón seleccionado.
     * Almacena el nombre del cantón en una variable temporal.
     * También se encarga de habilitar el JFXComboBox de los distritos.
     * @author Pablo Fonseca.
     */
    public void solicitarIDCanton(){
        if(cantones.getValue() != null && !(cantones.getValue().equalsIgnoreCase(""))){
            //Se almacena el nombre del cantón.
            this.canton = cantones.getValue();
            //Ahora se establece el ID del cantón en una variable temporal.
            this.IDCanton= controller.solicitarIDCanton(this.IDPais, this.IDProvincia, this.cantones.getValue());
            //Finalmente se deshabilita el JFXComboBox de los distritos.
            this.distritos.setDisable(false);
        }
    }

    /**
     * Despliega los distritos de la provincia seleccionada en la base de datos.
     * @author Pablo Fonseca.
     */
    public void desplegarDistritos(){
        this.distritos.getItems().clear();
        String[] distritosArray = controller.listarDistritos(IDPais, IDProvincia, IDCanton);
        for(String s: distritosArray){
            this.distritos.getItems().add(s);
        }
        distritos.show();
    }

    /**
     * Se encarga de almacenar en una variable temporal el ID del distrito seleccionado.
     * También se almacena el nombre del distrito en una variable temporal.
     * @author Pablo Fonseca.
     */
    public void solicitarIDDistrito(){
        if(this.distritos.getValue() != null && !(this.distritos.getValue().equalsIgnoreCase(""))){
            //Se almacena el nombre del distrito.
            this.distrito = this.distritos.getValue();
            //Ahora se establece el ID del distrito en una variable temporal.
            this.IDDistrito = controller.solicitarIDDistrito(this.IDPais, this.IDProvincia, this.IDCanton,
                    this.distritos.getValue());
        }
    }

    public void registrarDireccion(){
        //Primero se validan los identificadores
        if(validarIdentificadores()){
            //Se crea un HashMap para enviar los datos más asegurados.
            HashMap<String, String> map = new HashMap<>();
            try {
                map.put("IDUsuario", String.valueOf(Controller.getIDUsuario()));
                map.put("NombrePais", pais);
                map.put("IDPais", String.valueOf(IDPais));
                map.put("NombreProvincia", provincia);
                map.put("IDProvincia", String.valueOf(IDProvincia));
                map.put("NombreCanton", canton);
                map.put("IDCanton", String.valueOf(IDCanton));
                map.put("NombreDistrito", distrito);
                map.put("IDDistrito", String.valueOf(IDDistrito));
            }catch(NullPointerException e){
                super.errorNotification.setContentText("Hubo una excepción a la hora de constuir el Map");
                super.errorNotification.showAndWait();
                return;
            }
            //Se envía la información empaquetada a la base de datos.
            boolean procesoFinalizado = enviarDatosDireccion(map);
            if(procesoFinalizado){
                super.informationNotification.setContentText("La dirección ha sido registrada.");
                super.informationNotification.setTitle("DIRECCIÓN REGISTRADA");
                super.informationNotification.showAndWait();
            }else{
                super.errorNotification.setTitle("NO SE PUDO REGISTRAR LA DIRECCIÓN");
                super.errorNotification.setContentText("Ha ocurrido un error a la hora de registrar la dirección");
                super.errorNotification.showAndWait();
            }

            //Si es una ventana de registro se envía al usuario a el FXML de ConfirmarUsuarioFXML
            if(ventanaRegistro){
                finalizarRegistro();
            }

            //Finalmente se deshabilitan todas los JFXComboBoxes y el botón de registrar dirección.
            this.paises.setDisable(true);
            this.provincias.setDisable(true);
            this.cantones.setDisable(true);
            this.distritos.setDisable(true);
            this.agregarDireccion.setDisable(true);

        }else{
            super.errorNotification.setContentText("Los datos no son válidos. \n Recuerde llenar todos los campos.");
            super.errorNotification.showAndWait();
        }
    }

    /**
     * Valida los identificadores del país, provincia, cantón y distrito.
     * @return true si todos los identificadores son correctos.
     */
    public boolean validarIdentificadores(){
        return (this.IDPais > -1 &&
                this.IDProvincia > -1 &&
                this.IDCanton > -1 &&
                this.IDDistrito > -1);
    }

    /**
     * Se envía al Controller un HashMap Collection con los datos de la dirección
     * @param datos HashMap con los datos de la dirección.
     * @return true si el proceso fue completado satisfactoriamente.
     */
    public boolean enviarDatosDireccion(HashMap<String, String> datos){
        //Se transfieren los datos por el Controller
        return controllerUsuario.insertarDireccionUsuario(datos);
    }

    /**
     * En caso de que la dirección se agregue a la hora de realizar un registro,
     * este método lo finalizará, enviando el correo de confirmación y posteriormente
     * enviando al usuario al FXML de Confirmar Codigo Usuario.
     */
    public void finalizarRegistro(){
        //Se obtiene del cualquier nodo la información de la ventana
        super.stage = (Stage) provincias.getScene().getWindow();

        //Si ya existe una escena, entonces se esconde.
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            //Se carga el FXML de la nueva escena.
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/ConfirmarCodigoUsuarioFXML.fxml"));
            Parent root = fxml.load();
            super.setDraggable(root);

            //Se muestra la excena con el FXML
            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();

        }catch(IOException | NullPointerException ignored){
            super.changeSceneToLogin(this.agregarDireccion);
        }

    }
}
