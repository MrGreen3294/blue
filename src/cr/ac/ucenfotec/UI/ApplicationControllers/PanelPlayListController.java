package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.PlayListModelTable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class PanelPlayListController extends Controller implements Initializable {

    @FXML
    private VBox agregarLista;

    @FXML
    private VBox listasRegistradas;

    @FXML
    private VBox eliminarLista;

    @FXML
    private StackPane subirListasStackPane;

    @FXML
    private TextField campoNombreLista;

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoDescripcionLista;

    @FXML
    private Label notificacionDescripcion;

    @FXML
    private Label notificacionVideo;

    @FXML
    private Label notificacionEstrellas;

    @FXML
    private JFXButton subirListaBtn;

    @FXML
    private JFXComboBox<String> temaLista;

    @FXML
    private StackPane baulListasStackPane;

    @FXML
    private TableView<PlayListModelTable> tablaListas;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaNombre;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaDescripcion;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaFechaRegistro;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaCalificacion;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaTema;

    @FXML
    private TableColumn<PlayListModelTable, String> columnaDuracion;

    @FXML
    private StackPane eliminarListasStackPane;

    @FXML
    private JFXListView<String> listViewPlayList;

    @FXML
    private JFXButton eliminarListaBtn;

    @FXML
    private JFXListView<String> listViewVideos;

    //Variable temporal que almacena el ID del usuario.
    private int IDUsuario = Controller.getIDUsuario();

    @FXML
    void irMenu(MouseEvent event) {
        if (controllerAdministrador.validarUsuarioAdministrador(this.IDUsuario)) {
            super.changeSceneToMenu(this.subirListaBtn, true);
        } else {
            super.changeSceneToMenu(this.subirListaBtn);
        }
    }

    //Variable temporal que servirá para seleccionar la localización del video.
    private FileChooser fileChooser = new FileChooser();

    //Variable temporal que almacenará el ID de una lista de reproducción en memoria.
    private int IDListaReproduccion = -1;

    //Variables temporales para transferir y recibir información del controller de las listas.
    String nombreLista = "";
    String descripcionLista = "";
    String temaSeleccionadoLista = "";
    String URLLista = "";


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Se inicializan datos importantes.
        this.temaLista.setValue("");

        //Se agrega el stack pane y sus funcionalidades de booteo.
        this.stackPaneAgregarLista();

        //Se agrega action event al botón para seleccionar la localización de una lista.
        this.subirListaBtn.setOnAction(actionEvent -> {
            registrarLista();
        });

        //Lista los temas de las listas de reproducción cuando se produzca un mouse event en el combo box.
        this.temaLista.setOnMouseClicked(mouseEvent -> {
            listarTemas();
        });

        //Se agrega mouse event al botón para registrar un playlist.
        this.agregarLista.setOnMouseClicked(mouseEvent -> {
            stackPaneAgregarLista();
        });

        //Se agrega mouse event al botón para listar los playlist.
        this.listasRegistradas.setOnMouseClicked(mouseEvent -> {
            stackPaneListarPlayList();
        });

        //Se agrega mouse event al botón para eliminar los playlist.
        this.eliminarLista.setOnMouseClicked(mouseEvent -> {
            stackPaneEliminarPlayList();
        });

        this.eliminarListaBtn.setOnAction(actionEvent -> {
            removerListas(this.IDUsuario, this.listViewPlayList.getSelectionModel().getSelectedItems());
        });
        //Indica el se pueden seleccionar múltiples elementos del list view con SHIFT.
        this.listViewVideos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Cuando el usuario presione dos clicks en la tabla, se llamará al método reproducirLista().
        this.tablaListas.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                String nombre = this.tablaListas.getSelectionModel().getSelectedItem().getNombre();
                if (!(nombre.isEmpty())) {
                        mostrarVideosLista(nombre);
                }
            }
        });
    }

    /**
     * Elimina las listas seleccionadas por el usuario.
     * @param IDUsuario: ID del usuario al que le pertenecen las listas de reproducción.
     * @param listaPlayList: ObservableList con los nombres de las listas de reproducción que se quieren eliminar.
     */
    private void removerListas(int IDUsuario, ObservableList<String> listaPlayList) {

        for(String lista: listaPlayList){
            controllerListaReproduccion.eliminarLista(IDUsuario, lista);
        }

        //Se borra la memoria del list view.
        this.listViewPlayList.getItems().clear();

        //Se listan nuevamente las listas de reproducción disponibles.
        this.listarPlayListPorEliminar(IDUsuario);

        super.informationNotification.setTitle("NOTIFICACION");
        super.informationNotification.setHeaderText("CONTENIDO ELIMINADO");
        super.informationNotification.setContentText(" ");
        super.informationNotification.showAndWait();

    }

    /////////////////////////////////////////////////////
    // STACK PANE PARA AGREGAR LISTAS
    /////////////////////////////////////////////////////
    private void stackPaneAgregarLista() {
        //Se controla el cambio de color en el VBox
        this.agregarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));
        this.listasRegistradas.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.eliminarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));

        //Se controla el nivel visual de los StackPanes.
        this.baulListasStackPane.setVisible(false);
        this.eliminarListasStackPane.setVisible(false);
        this.subirListasStackPane.setVisible(true);

         /*
          Se borra la memoria del list view
         */
        this.listViewVideos.getItems().clear();

        //Se listan los videos disponibles
        listarVideosPorAgregar(this.IDUsuario);
    }

    /////////////////////////////////////////////////////
    // STACK PANE PARA VISUALIZAR LAS  LISTAS
    /////////////////////////////////////////////////////
    public void stackPaneListarPlayList(){
        //Se controla el cambio de color en el VBox
        this.agregarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.listasRegistradas.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));
        this.eliminarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));

        //Se controla el nivel visual de los StackPanes.
        this.baulListasStackPane.setVisible(true);
        this.eliminarListasStackPane.setVisible(false);
        this.subirListasStackPane.setVisible(false);

        //Se consultan las listas de la base de datos.
        obtenerListas();
    }

    /////////////////////////////////////////////////////
    // STACK PANE PARA ELIMINAR LAS  LISTAS
    /////////////////////////////////////////////////////
    public void stackPaneEliminarPlayList(){
        //Se controla el cambio de color en el VBox
        this.agregarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.listasRegistradas.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.eliminarLista.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));

        //Se controla el nivel visual de los StackPanes.
        this.baulListasStackPane.setVisible(false);
        this.eliminarListasStackPane.setVisible(true);
        this.subirListasStackPane.setVisible(false);

        /*
          Se borra la memoria del list view
         */
        this.listViewPlayList.getItems().clear();

        this.listViewPlayList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        listarPlayListPorEliminar(this.IDUsuario);

    }



    public void registrarLista(){

        //Almacena los datos escritos en variables temporales con un formato adecuado.
        this.nombreLista = this.campoNombreLista.getText().trim();
        this.descripcionLista = this.campoDescripcionLista.getText();
        this.temaSeleccionadoLista = this.temaLista.getSelectionModel().getSelectedItem();

        //Primero se valida que todos los datos sean correctos.
        boolean camposViolados = validarDatos();

        if(!(camposViolados)){

            //Se verifica que se hayan registrado al menos dos videos
                if(!(listViewVideos.getSelectionModel().getSelectedItems().size() < 2)) {
                    //Ahora valida si se puede alojar una lista con ese nombre.
                    if(seleccionarLocalizacionLista()) {
                        //REGISTRO FUNDAMENTAL
                        HashMap<String, Object> datos = new HashMap<>();
                        datos.put("NombreLista", this.nombreLista.replace(" ", "_").trim());
                        datos.put("DescripcionLista", this.descripcionLista);
                        datos.put("URLLista", this.URLLista);
                        datos.put("IDUsuario", this.IDUsuario);


                        //SE REGISTRA LOS DATOS FUNDAMENTALES DE LA LISTA
                        this.IDListaReproduccion = super.controllerListaReproduccion.registrarListaReproduccion(datos);

                        //SI EXISTE UN TEMA ENTONCES SE INDICA
                        if(!(this.temaSeleccionadoLista.equalsIgnoreCase(""))){
                            int IDTema = controllerTema.obtenerIDTema(this.temaSeleccionadoLista);
                            //Ahora se actualiza en la lista.
                            controllerListaReproduccion.agregarTemaLista(IDTema, this.IDListaReproduccion);
                        }

                        if(this.IDListaReproduccion != -1){
                            super.informationNotification.setTitle("NOTIFICACION");
                            super.informationNotification.setHeaderText("LISTA REGISTRADA");
                            super.informationNotification.setContentText("");
                            super.informationNotification.showAndWait();
                        }

                        //LUEGO SE ASOCIAN LOS VIDEOS.
                        this.enlazarVideos(this.listViewVideos.getSelectionModel().getSelectedItems());

                    }else{
                        super.errorNotification.setTitle("ERROR");
                        super.errorNotification.setHeaderText("YA EXISTE UN PLAYLIST CON EL NOMBRE " + this.nombreLista.toUpperCase());
                        super.errorNotification.setContentText("");
                        super.errorNotification.showAndWait();
                    }
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("DEBE ASIGNAR AL MENOS DOS VIDEOS.");
                    super.errorNotification.setContentText("");
                    super.errorNotification.showAndWait();
                }
            }
        }

    private void enlazarVideos(ObservableList<String> selectedItems) {
        //SE INTENTA OBTENER EL ID DE CADA VIDEO Y SE ENLAZA A LA LISTA DE REPRODUCCIÓN.
        int index = 0;
        for(String e: selectedItems){
            int IDVideo = controllerVideo.obtenerIDVideo(selectedItems.get(index), this.IDUsuario);
            if(!(controllerListaReproduccion.enlazarVideo(this.IDListaReproduccion, IDVideo))){
                super.errorNotification.setTitle("ERROR");
                super.errorNotification.setHeaderText("NO SE PUEDE REGISTRAR UN VIDEO REPETIDO.");
                super.errorNotification.setContentText("Inténtelo más tarde...");
                super.errorNotification.showAndWait();
            }
            index++;
        }
    }



    /**
     * Método que se encarga de validar que todos los datos de la lista a registrar tengan un formato adecuado.
     * @return false si los campos son válidos.
     */
    public boolean validarDatos(){
        boolean camposViolados = false;

        if (validarCampoComunNumerosCenfotec(this.nombreLista, this.notificacionNombre)) {
            camposViolados = true;
        } else if (validateMaxChars(nombreLista, 100)) {
            this.notificacionNombre.setVisible(true);
            this.notificacionNombre.setText("No puede ingresar más de 100 caracteres.");
            camposViolados = true;
        } else {
            this.notificacionNombre.setVisible(false);
        }

        //Si la descripción no está vacía, porque puede estarlo.
        if (!this.descripcionLista.isEmpty()) {
            if (this.validarCampoComunCenfotec(this.descripcionLista, notificacionDescripcion)) {
                camposViolados = true;
            }
        }

        return camposViolados;
    }

    /**
     * Selecciona la localización de una lista de videos.
     * @return true si la localización puede ser ocupada por la lista con el nombre ingresado.
     */
    public boolean seleccionarLocalizacionLista(){
        if (this.campoNombreLista.getText().isEmpty()) {
            super.errorNotification.setTitle("ERROR");
            super.errorNotification.setHeaderText("VERIFIQUE QUE TODOS LOS CAMPOS ESTEN LLENOS");
            super.errorNotification.setContentText("");
            super.errorNotification.showAndWait();
            return false;
        } else {
            this.notificacionNombre.setVisible(false);

            File directorioDestino;
            String rutaLista =
                            ".\\src\\Usuarios\\" + controllerUsuario.obtenerIdentificacionUsuario(IDUsuario) +
                                    "\\PlayList\\" + nombreLista.replace(" ", "_").trim();

            directorioDestino = new File(rutaLista);

            /*
             ESTO SUCEDE CUANDO LA RUTA DEL ARCHIVO ESTÁ EN EL DISCO LOCAL.
              1. VALIDARÁ SI EN LA BASE DE DATOS SE ENCUENTRA REGISTRADA UNA RUTA CON EL URL.
              2. SI NO HAY UNA RUTA CON ESE URL, ENTONCES EL ARCHIVO SE ELIMINA Y SE RETORNA TRUE.
              3. SI HAY UN ARCHIVO CON ESA RUTA, SE RETORNA FALSE Y SE MUESTRA UNA NOTIFICACIÓN ARRIBA.
            */
            if(directorioDestino.exists()) {
                if(controllerListaReproduccion.validarExistenciaLista(this.IDUsuario, nombreLista)){
                    //Si la lista existe en la base de datos.
                    return false;
                }else {
                    //Si no existe un registro en la base de datos, se elimina la carpeta
                    //que se encuentra en el disco duro y luego se reemplaza por la nueva.
                    directorioDestino.delete();
                    if (directorioDestino.mkdirs()) {
                        this.URLLista = directorioDestino.getPath();
                        return true;
                    }
                }
            }else{
                if (directorioDestino.mkdirs()) {
                    this.URLLista = directorioDestino.getPath();
                    return true;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Método que se encarga de listar los temas de las listas.
     */
    public void listarTemas() {
        this.temaLista.getItems().clear();
        ArrayList<String> temas = controllerTema.obtenerTemas();
        for (String e : temas) {
            this.temaLista.getItems().add(e);
        }
        this.temaLista.show();
    }

    /**
     * Método que se encarga de listar en un list view los videos que están disponibles en la base de datos.
     * Este método solo está disponible para la vista de agregar videos.
     * @param IDUsuario: ID del usuario.
     */
    private void listarVideosPorAgregar(int IDUsuario) {
        ArrayList<String> videos = controllerVideo.obtenerNombreVideos(IDUsuario);
        this.listViewVideos.getItems().addAll(videos);
        this.listViewVideos.setVisible(true);
    }


    public void listarPlayListPorEliminar(int IDUsuario){
        ArrayList<String> playlist = controllerListaReproduccion.obtenerNombreListas(IDUsuario);
        this.listViewPlayList.getItems().addAll(playlist);
        this.listViewPlayList.setVisible(true);
    }

    public void obtenerListas(){
        //Se indica el valor por defecto cuando no hay videos subidos.
        this.tablaListas.setPlaceholder(new Label("No hay listas registradas"));

        //Se indican los value factories de las columnas.
        this.columnaNombre.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>("nombre"));
        this.columnaDescripcion.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>("descripcion"));
        this.columnaFechaRegistro.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>("fechaRegistro"));
        this.columnaCalificacion.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>(
                "calificacion"));
        this.columnaTema.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>("tema"));
        this.columnaDuracion.setCellValueFactory(new PropertyValueFactory<PlayListModelTable, String>("duracion"));

        //Se consultan los videos y se agrega el ObservableList retornado.
        this.tablaListas.setItems(ajustarListas(this.IDUsuario));
    }

    /**
     * Método que se encarga de obtener las listas de obtener las listas del usuario y ajustarlas al TableView.
     * @param IDUsuario: ID del usuario que inició sesión.
     * @return ObservableList de tipo VideoModelTable.
     */
    public ObservableList<PlayListModelTable> ajustarListas(int IDUsuario){
        ObservableList<PlayListModelTable> listasRegistradas = FXCollections.observableArrayList();
        PlayListModelTable vmtObj;
        HashMap<Integer, PlayListModelTable> map = controllerListaReproduccion.obtenerListas(IDUsuario);

        for (Map.Entry<Integer, PlayListModelTable> entry : map.entrySet()) {
            double duracionSegundosLista = Double.parseDouble(entry.getValue().getDuracion());
            int calificacionLista = (int) Double.parseDouble(entry.getValue().getCalificacion());
            String nombreLista = entry.getValue().getNombre();
            String descripcionLista = entry.getValue().getDescripcion();
            String fechaCreacionLista = entry.getValue().getFechaRegistro();

            //Se obtiene la categoría con su ID.
            //Si el usuario definió un tema, se consulta su nombre en la base de datos.
            String tempTema;
            int IDTempTema = -1;
            //Se consulta el ID temporal de la lista.
            int IDListaReproduccion = controllerListaReproduccion.obtenerIDLista(nombreLista, this.IDUsuario);
            //Si la lista de reproducción existe, se consulta el ID del tema de la lista.
            if (IDListaReproduccion > -1) {
                IDTempTema = controllerListaReproduccion.obtenerIDTema(IDListaReproduccion);
            }
            //Si el tema de la lista es válido, se obtiene el nombre del tema de la lista.
            if (IDTempTema > -1) {
                tempTema = controllerTema.obtenerNombreTema(IDTempTema);
            }
            //Si el tema de la lista es inválido, es porque la lista no tiene tema, por lo cual se muestra como
            // indefinido.
            else {
                tempTema = "INDEFINIDO";
            }
            String calificacionDetalle;
            if (calificacionLista == 1) {
                calificacionDetalle = "MALA";
            } else if (calificacionLista == 2) {
                calificacionDetalle = "BUENA";
            } else {
                calificacionDetalle = "MUY BUENA";
            }
            long duracionLista = (long) duracionSegundosLista;
            String duracionTotalStr;
            if (duracionSegundosLista >= 60) {
                duracionTotalStr =
                        (TimeUnit.SECONDS.toMinutes(duracionLista - (TimeUnit.SECONDS.toHours(duracionLista * 60))) +
                                " min.");
            } else {
                duracionTotalStr = duracionLista + " seg.";
            }
            vmtObj = new PlayListModelTable(
                    nombreLista,
                    descripcionLista,
                    fechaCreacionLista,
                    calificacionDetalle,
                    tempTema,
                    duracionTotalStr);

            listasRegistradas.add(vmtObj);

        }
        return listasRegistradas;
    }

    /**
     * Método que se encarga de generar un media player para reproducir el video seleccionado.
     *
     * @param nombreLista: Nombre de la lista que se va a reproducir.
     */
    private void mostrarVideosLista(String nombreLista) {

        Controller.setNombreLista(nombreLista);
        Controller.setIDLista(this.IDListaReproduccion);

        generarMediaPlayer();


    }



    /**
     * Se encarga de generar un stage con un media player.
     */
    private void generarMediaPlayer() {
        Stage stage = new Stage();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/MediaPlayerPlayListFXML" +
                    ".fxml"));
            scene = new Scene(root);

            stage.centerOnScreen();
            stage.setScene(scene);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
            stage.setFullScreen(true);

            stage.setFullScreenExitHint(null);
            stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);

            scene.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    stage.close();
                }
            });

            stage.setResizable(false);

            super.stage = stage;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}