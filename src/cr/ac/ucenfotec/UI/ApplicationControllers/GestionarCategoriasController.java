package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class GestionarCategoriasController extends Controller implements Initializable {

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombre;

    @FXML
    private Label notificacionDescripcion;

    @FXML
    private TextField campoDescripcion;

    @FXML
    private JFXButton agregarCategoriaBtn;

    @FXML
    private JFXButton eliminarCategoriaBtn;

    @FXML
    private JFXListView<String> listaCategorias;

    @FXML
    private ImageView irMenu;


    private String nombre;
    private String descripcion;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        listarCategoriasRegistradas();
        this.agregarCategoriaBtn.setOnAction(actionEvent -> {
            agregarCategoria();
        });

        this.irMenu.setOnMouseClicked(mouseEvent -> {
            super.changeSceneToMenu(irMenu, true);
        });

        this.eliminarCategoriaBtn.setOnMouseClicked(mouseEvent -> {
            eliminarCategorias();
        });
    }

    public void agregarCategoria(){
        this.nombre = this.campoNombre.getText().trim();
        this.descripcion = this.campoDescripcion.getText();

        boolean datosIncorrectos = validarDatosCategoria();

        if(!(datosIncorrectos)){
            if(controllerCategoria.agregarCategoria(nombre, descripcion)){
                super.informationNotification.setTitle("Categoría Registrada");
                super.informationNotification.setContentText("La categoría ha sido registrada");
                super.informationNotification.setHeaderText("Categoría " + nombre + " Registrada.");
                super.informationNotification.showAndWait();

                this.listarCategoriasRegistradas();
            }
        }
    }

    public void eliminarCategorias(){
        if(this.listaCategorias.getSelectionModel().getSelectedItems().size() > 0) {
            boolean error = false;
            for (String categoria : this.listaCategorias.getSelectionModel().getSelectedItems()) {
                boolean categoriaEliminada = controllerCategoria.eliminarCategoria(categoria);
                if (!(categoriaEliminada)) {
                    error = true;
                }
            }
            if (!(error)) {
                super.informationNotification.setTitle("Categoria(s) Eliminada(s)");
                super.informationNotification.setHeaderText("El contenido ha sido eliminado.");
                super.informationNotification.showAndWait();

                this.listarCategoriasRegistradas();
            }
        }
    }
    public void listarCategoriasRegistradas(){
        this.listaCategorias.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.listaCategorias.getItems().clear();
        ArrayList<String> categoriasRegistradas = controllerCategoria.obtenerCategorias();
        this.listaCategorias.getItems().addAll(categoriasRegistradas);
    }

    public boolean validarDatosCategoria(){
        boolean campoIncorrecto = false;

        if(super.validarCampoComunCenfotec(nombre, this.notificacionNombre)){
            campoIncorrecto = true;
        }
        if(super.validarCampoComunCenfotec(descripcion, this.notificacionDescripcion)){
            campoIncorrecto = true;
        }
        else{
            this.notificacionDescripcion.setVisible(false);
            this.notificacionNombre.setVisible(false);
        }

        return campoIncorrecto;
    }



}
