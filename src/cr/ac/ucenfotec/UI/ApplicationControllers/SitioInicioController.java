package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SitioInicioController extends Controller implements Initializable{

    @FXML
    private Button iniciarApp;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.iniciarApp.setOnAction(actionEvent ->  {
            Stage stage = new Stage();
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/RegistrarUsuarioFXML.fxml"));
                //When the root is clicked the y and x variable gets the x and y position of the mouse.
                //Lambda Expressions.
                root.setOnMousePressed(mouseEvent -> {
                    xOffset = mouseEvent.getSceneX();
                    yOffset = mouseEvent.getSceneY();
                });

                //When the root is dragged set the position to the new x and y position of the mouse.
                root.setOnMouseDragged(mouseEvent -> {
                    stage.setX(mouseEvent.getScreenX() - xOffset);
                    stage.setY(mouseEvent.getScreenY() - yOffset);
                });
                setDraggable(root);
                scene = new Scene(root);
                stage.centerOnScreen();
                stage.setScene(scene);
                stage.initStyle(StageStyle.TRANSPARENT);
                this.iniciarApp.getScene().getWindow().hide();
                stage.show();
                super.stage = stage;
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }
}
