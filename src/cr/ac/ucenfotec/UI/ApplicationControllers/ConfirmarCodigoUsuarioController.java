package cr.ac.ucenfotec.UI.ApplicationControllers;

import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class ConfirmarCodigoUsuarioController extends Controller implements Initializable{

        @FXML
        private Label correoElectronico;

        @FXML
        private TextField campoCodigo;

        @FXML
        private Label notificacionCodigo;

        @FXML
        private Button confirmarCuenta;

        @FXML
        private Label iniciarSesion;

        //Variable temporal que almacena el correo electrónico del usuario que se está registrando.
        private String correoElectronicoUsuario = Controller.getCorreoElectronicoUsuario();

        //Variable temporal que almacena el ID del usuario que se está registrando.
        private int IDUsuario = Controller.getIDUsuario();

        //Variable temporal que almacena el código genérico del usuario que se está registrando.
        private String codigoGenerico = Controller.getCodigoGenericoUsuario();

        //Variable temporal que almacena el apodo del uuario que se está registrando.
        private String nombreUsuario = Controller.getApodoUsuario();

        //Variable temporal que almacena la clave de seguridad del usuario que se está registrando.
        private String claveSeguridad = Controller.getClaveUsuario();

        //Constante que guarda el número de caracteres que debe contener el código generado para validarlo.
        final int MIN_COD_CHARS = 6;

        //Variable temporal para manejar el controller del usuario general.
        ControllerUsuario controllerUsuario = new ControllerUsuario();

        @FXML
        void changeSceneToLogin(MouseEvent event) {
            //Se envía cualquier nodo para cambiar la escena al login.
            super.changeSceneToLogin(iniciarSesion);
        }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
            //Inicializa las variables temporales para el envío del correo electrónico.
            Controller.fromEmailAccount = "blueartsapplication@gmail.com";
            Controller.emailPassword = "Qs7Cq85CQfDq";

            //Valida si se ha asignado un correo electrónico en la variable correoElectronicoUsuario
            if(correoElectronicoUsuario != null && !(correoElectronicoUsuario.equalsIgnoreCase(""))){
                        this.correoElectronico.setText(correoElectronicoUsuario);
                        this.correoElectronico.setVisible(true);
            }

            //Valida que el correo electrónico y el ID del usuario sean válidos.
            boolean datosValidos = validarDatos();
            if(datosValidos){
                //Se establece el email donde se enviará el mensaje
                Controller.toEmailAccount = this.correoElectronicoUsuario;
                Controller.genericCode = this.codigoGenerico;
                //Valida que todos los datos requeridos para enviar el mensaje sean correctos.
                boolean datosMensajeViolados = validarDatosMensaje();
                if(!datosMensajeViolados){
                    //Se crea el mensaje
                    Controller.messageToSend = definirMensaje();
                    //Se envía el mensaje
                    try {
                        this.enviarMail();

                    } catch (MessagingException e) {
                        super.errorNotification.setTitle("ERROR");
                        super.errorNotification.setHeaderText("NO HAY CONEXIÓN A INTERNET");
                        super.errorNotification.setContentText("Es posible que no se haya podido establecer, " +
                                "una conexión a internet.\n Inténtelo más tarde...");
                        super.errorNotification.showAndWait();
                        this.changeSceneToLogin(iniciarSesion);
                    }
                }else{
                    super.errorNotification.setTitle("ERROR AL ENVIAR EL MENSAJE");
                    super.errorNotification.setHeaderText("DATOS INVÁLIDOS");
                    super.errorNotification.setContentText("Los datos necesarios para enviar el mensaje de " +
                            "confirmación no son válidos");
                    super.errorNotification.showAndWait();
                }
            }else{
                super.errorNotification.setTitle("PROBLEMA AL CARGAR LOS DATOS");
                super.errorNotification.setHeaderText("DATOS NO ENCONTRADOS");
                super.errorNotification.setContentText("Los datos necesarios para esta escena no fueron encontrados");
                super.errorNotification.showAndWait();
            }

            //Cuando se presiona el botón confirmarCuenta se llama al método verifyAdminCode();
            this.confirmarCuenta.setOnAction(actionEvent -> {
                this.verificarCodigo();
            });
    }

    /**
     * Valida que los datos de esta escena sean válidos.
     * @return true si todos los datos son correctos.
     */
    public boolean validarDatos(){
        boolean datosValidos = false;

        if(
                this.correoElectronicoUsuario != null &&
                !(this.correoElectronicoUsuario.isEmpty()) &&
                this.IDUsuario > -1 &&
                !(this.nombreUsuario.isEmpty()) &&
                !(this.claveSeguridad.isEmpty()))
            datosValidos = true;
        return datosValidos;
    }

    /**
     * Se encarga de validar el campo del código de la vista.
     * @param codigo: Código a validar.
     * @return false si el campo es correcto.
     */
    public boolean validarCampoCodigo(String codigo){
        boolean violacionCampo = false;
        //Se habilida el label
        this.notificacionCodigo.setVisible(true);
        //Valida si el código no está vacío.
        if(super.validateEmpty(codigo)){
            violacionCampo = true;
            notificacionCodigo.setText("El código no puede estar vacío.");
        }
        //Valida si el código tiene los caracteres requeridos.
        else if(super.validateMinChars(codigo, this.MIN_COD_CHARS)){
            violacionCampo = true;
            notificacionCodigo.setText("El código no puede tener menos de " + this.MIN_COD_CHARS + " caracteres.");
        }
        //Si todas las validaciones pasaron entonces se desactiva la visibilidad del Label.
        else{
            this.notificacionCodigo.setVisible(false);
        }
        return violacionCampo;
    }

    /**
     * Valida que ambos correos para el manejo de datos sean válidos, al
     * igual que el código generico proporcionado.
     * @return false si todos los datos son correctos.
     */
    public boolean validarDatosMensaje(){
        boolean camposViolados = false;
        if(super.validarCorreoCenfotec(this.fromEmailAccount, null))
            camposViolados = true;
        if(super.validarCorreoCenfotec(this.toEmailAccount, null))
            camposViolados = true;
        if(this.codigoGenerico.isEmpty() || this.codigoGenerico.length() < MIN_COD_CHARS)
            camposViolados = true;
        return camposViolados;
    }


    /**
     * Se encarga de validar y verificar que:
     * 1. El código sea insertado correctamente.
     * 2. Que el código sea igual al código generado.
     */
    private void verificarCodigo(){
        String codigo = this.campoCodigo.getText().trim();

        boolean errorEnCampo = validarCampoCodigo(codigo);

        boolean verificacionExitosa;

        //Si no hay error en los campos entonces se verificará si el código insertado es equivalente al digitado por
        // el usuario.
        if(!(errorEnCampo)){
            verificacionExitosa = controllerUsuario.validarCodigoConfirmacion(this.IDUsuario, codigo);
            if(verificacionExitosa){
                super.informationNotification.setTitle("CÓDIGO");
                super.informationNotification.setHeaderText("EL CÓDIGO ES CORRECTO");
                super.informationNotification.setContentText("Su cuenta será activada");
                super.informationNotification.showAndWait();

                //Se activa la cuenta
                if(this.activarCuenta(this.IDUsuario)){
                    super.informationNotification.setTitle("CUENTA");
                    super.informationNotification.setHeaderText("CUENTA ACTIVADA");
                    super.informationNotification.setContentText("Su cuenta ahora está activa.");
                    super.informationNotification.showAndWait();
                    //Se cambia a la escena para elegir el rol.
                    asignarRolUsuario();
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("ERROR EN LA ACTIVACIÓN DE SU CUENTA");
                    super.errorNotification.setContentText("Hubo un error a la hora de activar su cuenta, inténtelo " +
                            "más tarde.");
                    super.errorNotification.showAndWait();
                }
            }else{
                super.errorNotification.setTitle("ERROR");
                super.errorNotification.setHeaderText("CÓDIGO INVÁLIDO");
                super.errorNotification.setContentText("El código insertado es inválido");
                super.errorNotification.showAndWait();
            }
        }
    }

    /**
     * Se encarga de enviar un correo electrónico con el mensaje generado.
     * @throws MessagingException excepción que es lanzada en caso de que haya ocurrido un error.
     */
    private void enviarMail() throws MessagingException {
        //Properties es una estructura Key: Value
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        //Se crea una nueva sesión con los datos.
        Session session = Session.getInstance(properties, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(fromEmailAccount, emailPassword);
            }
        });

        Message message = generarMensaje(session, fromEmailAccount, toEmailAccount, messageToSend);
        Transport.send(message);

    }

    public Message generarMensaje(Session session, String fromEmailAccount, String recipientEmailAccount,
                                  String message){
        Message tempMessage = new MimeMessage(session);
        try {
            tempMessage.setFrom(new InternetAddress(fromEmailAccount));
            tempMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmailAccount));
            tempMessage.setSubject("ACTIVA TU CUENTA");
            tempMessage.setContent(message, "text/html; charset=UTF-8");
            return tempMessage;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String definirMensaje(){
        StringBuilder builder = new StringBuilder();
        builder.append("<h1>Su registro en <strong>BLUE Arts</strong> fue exitoso</h1>").append("\n");
        builder.append("<p>Su correo electrónico proporcionado en el registro es").append(" ").append("<a href=\"").append(this.toEmailAccount).append("\">").append(this.toEmailAccount).append("</a>").append("</p>").append("\n");
        builder.append("<p>Para activar su cuenta ingrese el siguiente código de confirmación</p>").append("\n");
        builder.append("<h3>Código Generado</h3>");
        builder.append("<h1><strong>").append(this.genericCode).append("</strong></h1>").append("\n");
        builder.append("<h3>DATOS IMPORTANTES</h3>").append("\n");
        builder.append("<p>Necesitará estos credenciales para iniciar sesión una vez su cuenta esté activada.</p>").append(
                "\n");
        builder.append("<p>Por favor, guarderlos en un lugar seguro, preferiblemente en una caja de contraseñas</p>").append("\n");
        builder.append("<h3>Nombre de Usuario</h3>").append("\t");
        builder.append("<h1>").append(this.nombreUsuario).append("</h1>").append("\n").append("<br/>");
        builder.append("<h3>Clave de Seguridad</h3>").append("\t");
        builder.append("<h1>").append(this.claveSeguridad).append("</h1>").append("\n").append("<br/>");
        return builder.toString();
    }

    /**
     * Activa la cuenta del usuario.
     * @param IDUsuario ID del usuario al que se le activará su cuenta.
     * @return true si la cuenta pudo ser activada.
     */
    public boolean activarCuenta(int IDUsuario){
        return controllerUsuario.activarCuentaUsuario(IDUsuario);
    }

    /**
     * Cambia la escena para que el usuario escoja su papel en la aplicación.
     */
    public void asignarRolUsuario(){

        //Se valida que la escena no esté nula para esconderla.
        if(super.scene != null){
            super.stage.hide();
        }

        //Se identifica el stage actual
        super.stage = (Stage) this.confirmarCuenta.getScene().getWindow();

        try {
            Controller.setIDUsuario(this.IDUsuario);

            //Se carga el FXML de la nueva escena.
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/MenuRolesFXML.fxml"));
            Parent root= fxml.load();
            super.setDraggable(root);
            super.scene = new Scene(root);
            super.stage.setScene(super.scene);
            super.stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
