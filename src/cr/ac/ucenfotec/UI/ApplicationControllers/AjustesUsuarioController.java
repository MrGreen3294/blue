package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class AjustesUsuarioController extends Controller implements Initializable {

    @FXML
    private Button eliminarCuentaBtn;

    @FXML
    private Button eliminarVideosBtn;

    @FXML
    private Button eliminarListasBtn;

    @FXML
    private Button salirBtn;

    //Variable temporal que almacena el ID del usuario que está utilizando la aplicación.
    private int IDUsuario = Controller.getIDUsuario();
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        salirBtn.setOnAction(actionEvent -> {
            if (controllerAdministrador.validarUsuarioAdministrador(this.IDUsuario)) {
                super.changeSceneToMenu(this.salirBtn, true);
            } else {
                super.changeSceneToMenu(this.salirBtn);
            }
        });

        eliminarCuentaBtn.setOnAction(actionEvent -> {
            eliminarCuenta();
        });

        eliminarVideosBtn.setOnAction(actionEvent -> {
            eliminarVideos();
        });

        eliminarListasBtn.setOnAction(actionEvent -> {
            eliminarListas();
        });
    }

    private void eliminarCuenta(){
        super.confirmationNotification.setHeaderText("¿Está seguro que desea eliminar su cuenta?");
        super.confirmationNotification.setContentText("Se eliminarán todos sus videos y sus listas de reproducción.");
        Optional<ButtonType> result = super.confirmationNotification.showAndWait();
        if(result.get() == ButtonType.OK){
            boolean resultado = controllerUsuario.eliminarCuentaUsuario(IDUsuario);
            if(resultado){
                super.informationNotification.setHeaderText("LA CUENTA HA SIDO ELIMINADA");
                super.informationNotification.setContentText(" ");
                super.informationNotification.setTitle("Cuenta Eliminada");
                super.informationNotification.showAndWait();
                String identificacionBaul = controllerUsuario.obtenerIdentificacionUsuario(this.IDUsuario);
                File directorioUsuario = new File(".\\src\\Usuarios\\" + identificacionBaul);
                //Se elimina el baúl del usuario.
                directorioUsuario.delete();
                changeSceneToLogin(eliminarCuentaBtn);
            }
        }
    }

    private void eliminarVideos(){
        super.confirmationNotification.setHeaderText("¿Está seguro que desea eliminar sus videos?");
        super.confirmationNotification.setContentText("No podrá recuperar ningún video.");
        Optional<ButtonType> result = super.confirmationNotification.showAndWait();
        if(result.get() == ButtonType.OK){
            boolean resultado = controllerUsuario.eliminarVideosUsuario(IDUsuario);
            if(resultado){
                super.informationNotification.setHeaderText("LOS VIDEOS HAN SIDO ELIMINADOS");
                super.informationNotification.setContentText(" ");
                super.informationNotification.setTitle("Videos Eliminados");
                super.informationNotification.showAndWait();
            }
        }
    }

    private void eliminarListas(){
        super.confirmationNotification.setHeaderText("¿Está seguro que desea eliminar sus listas de reproducción?");
        super.confirmationNotification.setContentText("No podrá recuperar ninguna lista.");
        Optional<ButtonType> result = super.confirmationNotification.showAndWait();
        if(result.get() == ButtonType.OK){
            boolean resultado = controllerUsuario.eliminarListasUsuario(IDUsuario);
            if(resultado){
                super.informationNotification.setHeaderText("LAS LISTAS HAN SIDO ELIMINADAS");
                super.informationNotification.setContentText(" ");
                super.informationNotification.setTitle("Listas de Reproducción Eliminadas");
                super.informationNotification.showAndWait();
            }
        }
    }
}
