package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController extends Controller implements Initializable {

    @FXML
    private VBox videos;

    @FXML
    private VBox playlist;

    @FXML
    private VBox cerrarSesion;

    @FXML
    private VBox ajustes;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.videos.setOnMouseClicked(mouseEvent -> {
            cambiarEscena("videos");
        });

        this.playlist.setOnMouseClicked(mouseEvent -> {
            cambiarEscena("playlist");
        });

        this.cerrarSesion.setOnMouseClicked(mouseEvent -> {
            cerrarSesionUsuario();
        });

        this.ajustes.setOnMouseClicked(mouseEvent -> {
            cambiarEscena("ajustes");
        });
    }

    private void cerrarSesionUsuario() {
        changeSceneToLogin(this.ajustes);
    }

    /**
     * Método que se encarga de cambiar de escena, dependiendo de la selección del usuario.
     * @param escena: Nombre de la escena a la que se va a cambiar.
     */
    private void cambiarEscena(String escena){

        super.stage = (Stage) this.ajustes.getScene().getWindow();

        if(super.scene != null){
            super.stage.hide();
        }
        Parent root;

        FXMLLoader fxml;

        try {
            switch (escena) {
                case "videos":
                    fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/PanelVideosFXML.fxml"));
                    root = fxml.load();
                    super.setDraggable(root);
                    super.scene = new Scene(root);
                    super.stage.setScene(super.scene);
                    super.stage.show();
                    break;
                case "playlist":
                    fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/PanelPlayListFXML.fxml"));
                    root = fxml.load();
                    super.setDraggable(root);
                    super.scene = new Scene(root);
                    super.stage.setScene(super.scene);
                    super.stage.show();
                    break;
                case "ajustes":
                    fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/AjustesUsuarioFXML.fxml"));
                    root = fxml.load();
                    super.setDraggable(root);
                    super.scene = new Scene(root);
                    super.stage.setScene(super.scene);
                    super.stage.show();
                    break;
            }
        }catch(IOException ignored){}

    }
}
