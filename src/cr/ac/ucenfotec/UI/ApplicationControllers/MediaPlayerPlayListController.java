package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXListView;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;

import com.jfoenix.controls.JFXSlider;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


public class MediaPlayerPlayListController extends Controller implements Initializable {

    @FXML
    private ImageView minimize;


    @FXML
    private ImageView exit;

    @FXML
    private Label tiempo;

    @FXML
    private JFXSlider progressBar;

    @FXML
    private ImageView reloadVideo;

    @FXML
    private JFXSlider volumeSlider;

    @FXML
    private ImageView videoAnterior;

    @FXML
    private ImageView playPauseImage;

    @FXML
    private ImageView videoSiguiente;


    @FXML
    private MediaView mediaView;

    @FXML
    private JFXListView<String> listaReproduccion;

    //Variable temporal que almacenará el nombre de la lista de reproducción que se está utilizando.
    private String nombreLista = Controller.getNombreLista();

    //Variable temporal que almacenará el ID del usuario que está utilizando la aplicación.
    private int IDUsuario = Controller.getIDUsuario();

    //Variable temporal que almacenará el ID de la lsita de reproducción que se está utilizando.
    private int IDLista = controllerListaReproduccion.obtenerIDLista(this.nombreLista, this.IDUsuario);

    //Variable temporal que almacena un counter para cambiar el tipo de animación de los botones.
    private static int playSwitch = 0;

    //Variable temporal para almacenar un objeto de tipo media.
    private Media media;

    //Variable temporal para almacenar un objeto de tipo media player.
    private MediaPlayer mediaPlayer;

    //Variable temporal que almacena este stage.
    private Stage thisStage;

    //Variable booleana que almacenará el valor de un switcher para implementar el método full-screen
    private boolean switcher = false;

    //Constante que establece el path relativo de la imagen para el botón de play.
    private final String PLAY_BTN = "src/cr/ac/ucenfotec/UI/Pictures/PLAY_BTN.png";

    //Constante que establece el path relativo de la imagen para el botón de pausa.
    private final String PAUSE_BTN = "src/cr/ac/ucenfotec/UI/Pictures/PAUSE_BTN.png";

    //Variable global que almacena el URL de todos los videos.
    private String URLVideo;

    //Variable global que almacena el ID de todos los videos
    private int IDVideo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Se listan los elementos de la lista de reproducción.
        actualizarListaReproduccion();

        //Configuración del video siguiente.
        this.videoSiguiente.setOnMouseClicked(mouseEvent -> {
            if(this.mediaPlayer != null && this.mediaView != null) {
                //Se obtiene el número del elemento que ha sido seleccionado.
                int indexActual =  this.listaReproduccion.getFocusModel().getFocusedIndex();
                this.listaReproduccion.getFocusModel().focus(indexActual + 1);
                this.listaReproduccion.getSelectionModel().select(indexActual + 1);
                if(this.mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING){
                    this.mediaPlayer.pause();
                }
                this.mediaPlayer =
                        new MediaPlayer(new Media(new File(controllerVideo.obtenerURLRelativo(this.IDUsuario,
                                this.listaReproduccion.getSelectionModel().getSelectedItem())).toURI().toString()));
                this.mediaView.setMediaPlayer(mediaPlayer);
                playSwitch = 1;
            }
        });

        //Configuración del video siguiente.
        this.videoAnterior.setOnMouseClicked(mouseEvent -> {
            if(this.mediaPlayer != null && this.mediaView != null) {
                //Se obtiene el número del elemento que ha sido seleccionado.
                int indexActual =  this.listaReproduccion.getFocusModel().getFocusedIndex();
                this.listaReproduccion.getFocusModel().focus(indexActual - 1);
                this.listaReproduccion.getSelectionModel().select(indexActual - 1);
                if(this.mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING){
                    this.mediaPlayer.pause();
                }
                this.mediaPlayer =
                        new MediaPlayer(new Media(new File(controllerVideo.obtenerURLRelativo(this.IDUsuario,
                                this.listaReproduccion.getSelectionModel().getSelectedItem())).toURI().toString()));
                this.mediaView.setMediaPlayer(mediaPlayer);
                playSwitch = 1;
            }
        });

        //Configurción del video anterior.

        //Configuración del list view.
        this.listaReproduccion.setOnMouseClicked(mouseEvent -> {
            if(this.mediaPlayer != null) {
                if (this.mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING) {
                    this.mediaPlayer.pause();
                    playSwitch = 1;
                }
            }

            //Se obtiene el ID del video.
            this.IDVideo =
                    controllerVideo.obtenerIDVideo(this.listaReproduccion.getSelectionModel().getSelectedItem(),
                            this.IDUsuario);

            //Se obtiene el URL del video.
            this.URLVideo = controllerVideo.obtenerURLRelativo(IDUsuario,
                    this.listaReproduccion.getSelectionModel().getSelectedItem());
            //Se ajusta el media file.
            this.media = new Media(new File(this.URLVideo).toURI().toString());
            //Se ajusta el media player.
            this.mediaPlayer = new MediaPlayer(media);
            //Se ajusta en el view del media player.
            this.mediaView.setMediaPlayer(mediaPlayer);

           //Ajuste automático del tamaño del video.
            DoubleProperty mediaViewWidth = mediaView.fitWidthProperty();
            DoubleProperty mediaViewHeight = mediaView.fitHeightProperty();
            try {
                mediaViewWidth.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
                mediaViewHeight.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));
            }catch(Exception ignored){

            }

            //Implementación del volúmen.
            volumeSlider.setValue(mediaPlayer.getVolume() * 100);
            volumeSlider.valueProperty().addListener(new InvalidationListener() {
                @Override
                public void invalidated(Observable observable) {
                    mediaPlayer.setVolume(volumeSlider.getValue() / 100);
                }
            });

            //Se establece el valor por defecto de la barra de progreso.
            progressBar.setValue(0); //Valor por defecto.

            //Se ajusta el movimiento automático de la barra de progreso.
            mediaPlayer.currentTimeProperty().addListener((observableValue, oldValue, newValue) -> {
                progressBar.setMax(mediaPlayer.getTotalDuration().toSeconds());
                progressBar.setValue(mediaPlayer.getCurrentTime().toSeconds());
                tiempo.setText(formatTime(Duration.seconds(mediaPlayer.getCurrentTime().toSeconds()),
                        Duration.seconds(mediaPlayer.getTotalDuration().toSeconds())));
            });

            //Se ajusta el cambio manual en segundos de la barra de progreso.
            progressBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    //Se guardará en la base de datos el último tiempo de reproducción.
                    controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);
                    mediaPlayer.seek(Duration.seconds(progressBar.getValue()));
                }

            });

            //Se obtiene el último tiempo de reproducción.
            this.mediaPlayer.setOnReady(() -> {
                mediaPlayer.seek(Duration.seconds(controllerVideo.obtenerUltimoTiempoReproduccion(IDVideo)));
                playPauseImage.setOnMouseClicked(playImageEvent -> {
                    String imagePath;
                    if (playSwitch == 0) {
                        //Change Image Path
                        imagePath = new File(PAUSE_BTN).getPath();
                        playSwitch = 1;
                        double ultimoTiempo = controllerVideo.obtenerUltimoTiempoReproduccion(IDVideo);
                        progressBar.setValue(ultimoTiempo);
                        mediaPlayer.seek(Duration.seconds(ultimoTiempo));
                        mediaPlayer.play();
                    } else {
                        //Change Image Path
                        imagePath = new File(PLAY_BTN).getPath();
                        playSwitch = 0;
                        mediaPlayer.pause();
                        //Se guardará en la base de datos el último tiempo de reproducción.
                        controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);
                    }
                    File imageFile = new File(imagePath);
                    String imageFileConversion = imageFile.toURI().toString();
                    Image image = new Image(imageFileConversion);
                    playPauseImage.setImage(image);
                });

                this.mediaPlayer.setOnEndOfMedia(() -> {

                });

                //Se recarga el video cuando se presiona el boton reload.
                this.reloadVideo.setOnMouseClicked(reloadVideoEvent -> {
                    if(this.mediaPlayer != null) {
                        mediaPlayer.seek(mediaPlayer.getStartTime());
                        double startTimeValue = mediaPlayer.getStartTime().toSeconds();
                        controllerVideo.actualizarUltimoTiempoVideo(startTimeValue, IDVideo);
                        mediaPlayer.play();
                        String imagePath = new File(PAUSE_BTN).getPath();
                        File imageFile = new File(imagePath);
                        String imageFileConversion = imageFile.toURI().toString();
                        Image image = new Image(imageFileConversion);
                        playPauseImage.setImage(image);
                    }
                });
            });

        });

        //Action Event para minimizar la ventana
        minimize.setOnMouseClicked(mouseEvent -> {
            thisStage = (Stage) minimize.getScene().getWindow();
            thisStage.setIconified(true);
        });

        //Mouse Event para cerra la ventana.
        exit.setOnMouseClicked(mouseEvent ->  {
            thisStage =  (Stage) exit.getScene().getWindow();
            //Se pausa cualquier video
            if(mediaPlayer != null) {
                mediaPlayer.pause();
            }
            //Se guardará en la base de datos el último tiempo de reproducción.
            if(super.validateIsNumber(String.valueOf(progressBar.getValue()))) {
                controllerVideo.actualizarUltimoTiempoVideo(progressBar.getValue(), IDVideo);
            }
            //Se cierra la ventana
            thisStage.hide();
        });
    }

    /**
     * Método que se encarga de actualizar la lista de reproducción, consultando los videos
     * contenidos en ella.
     */
    public void actualizarListaReproduccion(){
        this.listaReproduccion.getItems().clear();

        HashMap<Integer, String> map = controllerListaReproduccion.obtenerNombreVideosEnlazados(this.IDLista);

        for(Map.Entry<Integer, String> entry : map.entrySet()){
            this.listaReproduccion.getItems().add(entry.getValue());
        }

    }


    /**
     * Método que calcula el time line de reproducción de un video.
     *
     */
    private static String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int)Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
                - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int)Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60 -
                    durationMinutes * 60;
            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds,durationMinutes,
                        durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d",elapsedMinutes,
                        elapsedSeconds);
            }
        }
    }
}
