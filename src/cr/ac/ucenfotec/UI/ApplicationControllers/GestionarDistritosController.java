package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class GestionarDistritosController extends Controller implements Initializable {

    @FXML
    private Label notificacionPais;

    @FXML
    private JFXComboBox<String> paisesComboBox;

    @FXML
    private Label notificacionProvincia;

    @FXML
    private JFXComboBox<String> provinciasComboBox;

    @FXML
    private Label notificacionCanton;

    @FXML
    private JFXComboBox<String> cantonesComboBox;

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombre;

    @FXML
    private Label notificacionCodigo;

    @FXML
    private TextField campoCodigo;

    @FXML
    private JFXButton agregarDistritoBtn;

    @FXML
    private ImageView salirBtn;

    @FXML
    private JFXComboBox<String> paisDistritoComboBox;

    @FXML
    private JFXComboBox<String> provinciaDistritoComboBox;

    @FXML
    private JFXComboBox<String> cantonDistritoComboBox;

    @FXML
    private JFXListView<String> listaDistritos;

    @FXML
    private JFXButton listarDatos;

    @FXML
    private JFXButton eliminarDistritoBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.listaDistritos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Paises Izquierda
        this.provinciaDistritoComboBox.setDisable(true);
        this.cantonDistritoComboBox.setDisable(true);
        this.paisDistritoComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisDistritoComboBox.getItems().clear();
            this.paisDistritoComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
            this.provinciaDistritoComboBox.setDisable(false);
        });

        //Paises Derecha
        this.provinciasComboBox.setDisable(true);
        this.cantonesComboBox.setDisable(true);
        this.paisesComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisesComboBox.getItems().clear();
            this.paisesComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
            this.provinciasComboBox.setDisable(false);
        });

        //Provincias Derecha
        this.provinciasComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            this.provinciasComboBox.getItems().clear();
            this.provinciasComboBox.getItems().addAll(super.controllerDireccion.listarProvincias(IDPais));
            this.cantonesComboBox.setDisable(false);
        });

        //Provincias Izquierda
        this.provinciaDistritoComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisDistritoComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            this.provinciaDistritoComboBox.getItems().clear();
            this.provinciaDistritoComboBox.getItems().addAll(super.controllerDireccion.listarProvincias(IDPais));
            this.cantonDistritoComboBox.setDisable(false);
        });

        //Cantones Derecha
        this.cantonesComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();
            String nombreProvincia = this.provinciasComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            int IDProvincia = super.controllerDireccion.solicitarIDProvincia(IDPais, nombreProvincia);
            this.cantonesComboBox.getItems().clear();
            this.cantonesComboBox.getItems().addAll(super.controllerDireccion.listarCantones(IDPais, IDProvincia));
        });

        //Cantones Izquierda
        this.cantonDistritoComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisDistritoComboBox.getSelectionModel().getSelectedItem();
            String nombreProvincia = this.provinciaDistritoComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            int IDProvincia = super.controllerDireccion.solicitarIDProvincia(IDPais, nombreProvincia);
            this.cantonDistritoComboBox.getItems().clear();
            this.cantonDistritoComboBox.getItems().addAll(super.controllerDireccion.listarCantones(IDPais, IDProvincia));
        });

        //Eliminar distritos
        this.eliminarDistritoBtn.setOnAction(actionEvent -> {
            eliminarDistrito();
        });
        this.salirBtn.setOnMouseClicked(mouseEvent -> {
            super.stage = (Stage) this.salirBtn.getScene().getWindow();
            if (super.scene != null) {
                super.stage.hide();
            }
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelDireccionesFXML.fxml"));
                super.setDraggable(root);
                stage.setScene(new Scene(root));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        //Listar Distritos
        this.listarDatos.setOnAction(actionEvent -> {
            String paisSeleccionado = this.paisDistritoComboBox.getSelectionModel().getSelectedItem();
            String provinciaSeleccionada = this.provinciaDistritoComboBox.getSelectionModel().getSelectedItem();
            String cantonSeleccionado = this.cantonDistritoComboBox.getSelectionModel().getSelectedItem();
            if(!(paisSeleccionado == null || paisSeleccionado.isEmpty())) {
                if(!(provinciaSeleccionada == null || provinciaSeleccionada.isEmpty())){
                    if(!(cantonSeleccionado == null || cantonSeleccionado.isEmpty())){
                        listarDistritos();
                    }else{
                        super.errorNotification.setHeaderText("Debe seleccionar un cantón");
                        super.errorNotification.setContentText("Indique el cantón");
                        super.errorNotification.setTitle("Error");
                        super.errorNotification.showAndWait();
                    }
                }else{
                    super.errorNotification.setHeaderText("Debe seleccionar una provincia");
                    super.errorNotification.setContentText("Indique el país");
                    super.errorNotification.setTitle("Error");
                    super.errorNotification.showAndWait();
                }
            }else{
                super.errorNotification.setHeaderText("Debe seleccionar un país");
                super.errorNotification.setContentText("Indique la provincia");
                super.errorNotification.setTitle("Error");
                super.errorNotification.showAndWait();
            }
        });

        //Agregar Distritos
        this.agregarDistritoBtn.setOnMouseClicked(mouseEvent -> {
            String nombreDistrito = this.campoNombre.getText().trim();
            String codigoDistrito = this.campoCodigo.getText().trim();
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();
            String nombreProvincia = this.provinciasComboBox.getSelectionModel().getSelectedItem();
            String nombreCanton = this.cantonesComboBox.getSelectionModel().getSelectedItem();

            boolean resultadoValidacion = this.validarDatos(nombrePais, nombreProvincia, nombreCanton, nombreDistrito
                    , codigoDistrito);

            if(!(resultadoValidacion)){
                registrarDistrito(nombreDistrito, codigoDistrito, nombrePais, nombreProvincia, nombreCanton);
            }
        });


    }

    public void registrarDistrito(String nombreDistrito, String codigoDistrito, String nombrePais,
                                  String nombreProvincia, String nombreCanton) {

        int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
        int IDProvincia = super.controllerDireccion.solicitarIDProvincia(IDPais, nombreProvincia);
        int IDCanton = super.controllerDireccion.solicitarIDCanton(IDPais, IDProvincia, nombreCanton);

        super.controllerDireccion.agregarDistrito(IDPais, IDProvincia, IDCanton, nombreDistrito, codigoDistrito);
        listarDistritos();
    }

    public void listarDistritos(){
        int IDPais =
                super.controllerDireccion.solicitarIDPais(this.paisDistritoComboBox.getSelectionModel().getSelectedItem());
        int IDProvincia =
                super.controllerDireccion.solicitarIDProvincia(IDPais,
                        this.provinciaDistritoComboBox.getSelectionModel().getSelectedItem());
        int IDCanton =
                super.controllerDireccion.solicitarIDCanton(IDPais, IDProvincia,
                        this.cantonDistritoComboBox.getSelectionModel().getSelectedItem());

        this.listaDistritos.getItems().clear();
        String[] distritosRegistrados = super.controllerDireccion.listarDistritos(IDPais, IDProvincia, IDCanton);
        for(String distritoRegistrado: distritosRegistrados){
            this.listaDistritos.getItems().add(distritoRegistrado);
        }
    }

    private boolean validarDatos(String nombrePais, String nombreProvincia, String nombreCanton, String nombreDistrito,
                                 String codigoDistrito) {
        boolean datosInvalidos = false;

        if (super.validarCampoComunCenfotec(nombreProvincia, notificacionProvincia)) {
            datosInvalidos = true;
        }
        if (super.validarCampoComunCenfotec(nombrePais, notificacionPais)) {
            datosInvalidos = true;
        }
        if(super.validarCampoComunCenfotec(nombreCanton, notificacionCanton)){
            datosInvalidos = true;
        }
        if(super.validarCampoComunCenfotec(nombreDistrito, notificacionNombre)){
            datosInvalidos = true;
        }
        if (super.validateIsNumber(codigoDistrito)) {
            this.notificacionCodigo.setVisible(true);
            this.notificacionCodigo.setText("El dato debe ser un número");
            datosInvalidos = true;
        }else{
            this.notificacionCodigo.setVisible(false);
        }

        return datosInvalidos;
    }

    public void eliminarDistrito(){
        String paisSeleccionado = this.paisDistritoComboBox.getSelectionModel().getSelectedItem();
        String provinciaSeleccionada = this.provinciaDistritoComboBox.getSelectionModel().getSelectedItem();
        String cantonSeleccionado = this.cantonDistritoComboBox.getSelectionModel().getSelectedItem();
        String nombreDistrito = this.listaDistritos.getSelectionModel().getSelectedItem();
        try {
            if (!(paisSeleccionado.isEmpty())) {
                if (!(provinciaSeleccionada.isEmpty())) {
                    if(!(cantonSeleccionado.isEmpty())) {
                        if (!(nombreDistrito.isEmpty())) {
                            super.controllerDireccion.eliminarDistrito(paisSeleccionado, provinciaSeleccionada,
                                    cantonSeleccionado, nombreDistrito);
                            listarDistritos();
                        }
                    }
                }
            }
        }catch(NullPointerException ignored){

        }
    }
}
