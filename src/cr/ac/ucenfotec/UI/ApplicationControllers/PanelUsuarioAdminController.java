package cr.ac.ucenfotec.UI.ApplicationControllers;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PanelUsuarioAdminController extends Controller implements Initializable {
    @FXML
    private VBox administrarTemasBtn;

    @FXML
    private VBox administrarCategoriasBtn;

    @FXML
    private VBox administrarDirecciones;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        this.administrarTemasBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarTemas();
        });


        this.administrarCategoriasBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarCategorias();
        });

        this.administrarDirecciones.setOnMouseClicked(mouseEvent -> {
            this.administrarDirecciones();
        });
    }

    public void administrarTemas(){
        super.stage = (Stage) administrarTemasBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarTemasFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void administrarCategorias(){
        super.stage = (Stage) administrarTemasBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarCategoriasFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void administrarDirecciones(){
        super.stage = (Stage) administrarTemasBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelDireccionesFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }



}
