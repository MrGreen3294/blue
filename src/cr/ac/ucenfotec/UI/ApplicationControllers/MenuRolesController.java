package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXRadioButton;
import cr.ac.ucenfotec.TL.ControllerAdministrador;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuRolesController extends Controller implements Initializable {


    @FXML
    private JFXRadioButton administrador;

    @FXML
    private ToggleGroup accountGroup;

    @FXML
    private JFXRadioButton usuarioFinal;

    @FXML
    private Button siguiente;

    //Variable temporal que almacenará el ID del usuario que se está registrando.
    int IDUsuario = Controller.getIDUsuario();

    //Variable temporal que será utilizada para acceder al túnel del controller Administrador.
    ControllerAdministrador controllerAdministrador = new ControllerAdministrador();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.siguiente.setOnAction(actionEvent -> {
            if(this.administrador.isSelected()){
                //Se intenta registrar un usuario con el rol de Administrador.
                registrarUsuarioAdministrador();
            }else if(this.usuarioFinal.isSelected()){
                //Se verifica si ya existe un administrador.
                //En caso de que no sea así el usuario no podrá registrarse como usuario común.
                if(validarExistenciaAdministrador()){
                    super.informationNotification.setTitle("¡TODO LISTO!");
                    super.informationNotification.setHeaderText("Se ha registrado como Usuario Final");
                    super.informationNotification.setContentText("Puede proceder a iniciar sesión");
                    super.informationNotification.showAndWait();
                    //Se cambia la escena al inicio de sesión, se le envía cualquier nodo. Ya que
                    //en realidad todos los usuarios tienen el rol por defecto de usuario final.
                    super.changeSceneToLogin(this.usuarioFinal);
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("NO EXISTE UN ADMINISTRADOR");
                    super.errorNotification.setContentText("No se puede registrar un usuario estándar si no existe un" +
                            " Administrador en el sistema.");
                    super.errorNotification.showAndWait();
                    this.usuarioFinal.setDisable(true);
                    this.administrador.setSelected(true);
                }
            }
        });
    }

    public void registrarUsuarioAdministrador(){
        //Primero se valida si ya existe un administrador.
        boolean adminExiste = validarExistenciaAdministrador();
        if(adminExiste){
            super.errorNotification.setTitle("ERROR");
            super.errorNotification.setHeaderText("YA EXISTE UN ADMINISTRADOR");
            super.errorNotification.setContentText("Ya existe un administrador en el sistema");
            super.errorNotification.showAndWait();
            this.administrador.setDisable(true);
            this.usuarioFinal.setSelected(true);
        }else{
            if(this.IDUsuario < 0){
                super.errorNotification.setTitle("ERROR");
                super.errorNotification.setHeaderText("NO EXISTE UN USUARIO");
                super.errorNotification.setContentText("Esta ha vista ha sido cargada si un usuario, " +
                        "no se puede completar este proceso.");
            }else {
                boolean procesoSatisfactorio = controllerAdministrador.asignarAdministrador(this.IDUsuario);
                if(procesoSatisfactorio){
                    super.informationNotification.setTitle("¡TODO LISTO!");
                    super.informationNotification.setHeaderText("Se ha registrado como Administrador");
                    super.informationNotification.setContentText("Puede proceder a iniciar sesión");
                    super.informationNotification.showAndWait();
                    //Se cambia la escena al inicio de sesión, se le envía cualquier nodo.
                    super.changeSceneToLogin(this.administrador);
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("NO SE PUDO ASIGNAR EL ROL");
                    super.errorNotification.setContentText("Inténtelo más tarde...");
                    super.errorNotification.showAndWait();
                }
            }
        }
    }

    /**
     * Valida que exista un administrador.
     * @return true si existe un administrador.
     */
    public boolean validarExistenciaAdministrador(){
        return controllerAdministrador.validarExistenciaAdministrador();
    }

}
