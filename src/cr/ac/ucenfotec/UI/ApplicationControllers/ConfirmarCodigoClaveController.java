package cr.ac.ucenfotec.UI.ApplicationControllers;

import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConfirmarCodigoClaveController extends Controller implements Initializable {

    @FXML
    private Label correoElectronico;

    @FXML
    private TextField campoCodigo;

    @FXML
    private Label notificacionCodigo;

    @FXML

    private Button confirmarCodigo;

    @FXML
    private Label iniciarSesion;

    //Variable temporal que almacena el código ingresado por el usuario desde la vista en un formato adecuado.
    private String codigo;

    //Variable temporal que almacena el controller del usuario.
    ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Variable temporal que almacena el ID del usuario que está intentando salvar su cuenta.
    int IDUsuario = Controller.getIDUsuario();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Cuando el usuario presione el botón para confirmar el código se llamará el método compararCodigos()
        this.confirmarCodigo.setOnAction(actionEvent -> {
            compararCodigos();
        });
        //Cuando el usuario presione el botón para iniciar sesión se llamará el método del padre para cambiar la
        // escena al login.
        this.iniciarSesion.setOnMouseClicked(mouseEvent -> {
            super.changeSceneToLogin(this.confirmarCodigo);
        });
    }

    /**
     * Se encarga de comparar el código de confirmación enviado para salvar la contraseña.
     */
    public void compararCodigos(){

        //Se almacena el valor del código ingresado en un mejor formato.
        this.codigo = this.campoCodigo.getText().trim();

        //Se valida que ambos códigos sean iguales.
        if(validarCodigoBD()){
            super.informationNotification.setTitle("NOTIFICACION");
            super.informationNotification.setHeaderText("CÓDIGO CORRECTO");
            super.informationNotification.showAndWait();

            //Ahora se cambia la escena para modificar la clave de seguridad.
            modificarClaveSeguridad();
        }else{
            super.errorNotification.setTitle("ERROR");
            super.errorNotification.setHeaderText("EL CÓDIGO INGRESADO ES INCORRECTO");
            super.errorNotification.showAndWait();
        }

    }

    /**
     * Se encarga de validar si el código de confirmación ingresado por el usuario
     * es equivalente al código interno actualizado en la base de datos.
     * @return true si los código son equivalentes.
     */
    public boolean validarCodigoBD(){
        return controllerUsuario.validarCodigoConfirmacion(this.IDUsuario, this.codigo);
    }

    /**
     * Cambia de escena para que el usuario pueda cambiar su clave de seguridad.
     */
    public void modificarClaveSeguridad(){
        //Se valida que la escena no esté nula para esconderla.
        if(super.scene != null){
            super.stage.hide();
        }

        //Se identifica el stage actual
        super.stage = (Stage) this.confirmarCodigo.getScene().getWindow();

        try {

            //Se envía el ID del usuario al controller padre, para que el siguiente controller lo pueda agarrar de él.
            Controller.setIDUsuario(this.IDUsuario);

            //Se carga el FXML de la nueva escena.
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/ModificarClaveSeguridadFXML" +
                    ".fxml"));
            Parent root= fxml.load();
            super.setDraggable(root);
            super.scene = new Scene(root);
            super.stage.setScene(super.scene);
            super.stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
