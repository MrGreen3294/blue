package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.VideoModelTable;
import cr.ac.ucenfotec.Utilities.CodeGenerator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.mp4.MP4Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.metadata.Metadata;
import org.controlsfx.control.Rating;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class PanelVideosController extends Controller implements Initializable {
    @FXML
    private VBox videosSubidos;

    @FXML
    private VBox agregarVideo;

    @FXML
    private VBox eliminarVideo;

    @FXML
    private TextField campoNombreVideo;

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoDescripcionVideo;

    @FXML
    private Label notificacionDescripcion;

    @FXML
    private Button seleccionarLocalizacionBtn;

    @FXML
    private Label notificacionVideo;

    @FXML
    private Rating calificacion;

    @FXML
    private StackPane baulVideos;

    @FXML
    private StackPane subirVideos;


    @FXML
    private StackPane eliminarVideos;


    @FXML
    private Label notificacionEstrellas;

    @FXML
    private Button subirVideoBtn;

    @FXML
    private JFXComboBox<String> categoriaVideo;


    @FXML
    private TableView<VideoModelTable> tablaVideos;

    @FXML
    private TableColumn<VideoModelTable, String> columnaNombre;

    @FXML
    private TableColumn<VideoModelTable, String> columnaDescripcion;

    @FXML
    private TableColumn<VideoModelTable, String> columnaFechaRegistro;

    @FXML
    private TableColumn<VideoModelTable, Double> columnaCalificacion;

    @FXML
    private TableColumn<VideoModelTable, String> columnaCategoria;

    @FXML
    private TableColumn<VideoModelTable, Double> columnaDuracion;

    @FXML
    private JFXButton eliminar;

    @FXML
    private JFXListView<String> listViewVideos;


    //Variable temporal que almacena el ID del usuario.
    private int IDUsuario = Controller.getIDUsuario();

    //Variable temporal que almacena el ID del video que se está operando.
    private int IDVideo = -1;

    //Variable temporal que almacena el ID de una categoria.
    private int IDCategoria = -1;

    //Variable temporal que servirá para seleccionar la localización del video.
    private FileChooser fileChooser = new FileChooser();

    //Variables temporales que se utilizarán para almacenar los datos de los campos gráficos con un formato adecuado.
    private String nombreVideo = "";
    private String descripcionVideo = "";
    private String URLVideo = "";
    private double duracionTotalVideo;
    //Variable temporal que almacenará la extensión de un video seleccionado.
    private String extensionVideo;

    @FXML
    void irMenu(MouseEvent event) {
        if (controllerAdministrador.validarUsuarioAdministrador(this.IDUsuario)) {
            super.changeSceneToMenu(this.eliminar, true);
        } else {
            super.changeSceneToMenu(this.eliminar);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Se inicializan datos importantes.
        this.categoriaVideo.setValue("");

        //Se agrega action event al botón para seleccionar la localización de un video.
        this.seleccionarLocalizacionBtn.setOnAction(actionEvent -> {
            seleccionarLocalizacionVideo();
        });

        //En caso de que el usuario presione el botón para subir un video a la Base de Datos se llamará al método
        // subir video.
        this.subirVideoBtn.setOnAction(actionEvent -> {
            subirVideo();
        });

        //Lista las categorías de los videos cuando se produzca un mouse event en el combo box.
        this.categoriaVideo.setOnMouseClicked(mouseEvent -> {
            this.listarCategorias();
        });

        //En caso de que el usuario presione el botón para ver los videos subidos, se llamará al método
        // stackPaneListarVideos()
        this.videosSubidos.setOnMouseClicked(mouseEvent -> {
            stackPaneListarVideos();
        });

        //En caso de que el usuario presione el botón para subir un video, se llamará al método stackPaneSubirVideos()
        this.agregarVideo.setOnMouseClicked(mouseEvent -> {
            stackPaneSubirVideo();
        });

        //En caso de que el usuario presione el botón para eliminar un video, se llamará al método
        // stackPaneEliminarVideos()
        this.eliminarVideo.setOnMouseClicked(mouseEvent -> {
            stackPaneEliminarVideos();
        });

        //Cuando el usuario presione dos clicks en la tabla, se llamará al método reproducirVideo().
        this.tablaVideos.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                String nombre = this.tablaVideos.getSelectionModel().getSelectedItem().getNombre();
                if (!(nombre.isEmpty())) {
                    reproducirVideo(nombre);
                }
            }
        });

        //Indica el se pueden seleccionar múltiples elementos del list view con SHIFT.
        this.listViewVideos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Cuando el usuario presione el botón de eliminar, se llamará el método eliminarVideos();
        this.eliminar.setOnAction(actionEvent -> {
            eliminarVideos(this.IDUsuario, this.listViewVideos.getSelectionModel().getSelectedItems());
        });

    }

    /**
     * Método que se encarga de reproducir un video seleccionado del TableView.
     *
     * @param nombre: nombre del video que se va a reproducir.
     */
    private void reproducirVideo(String nombre) {

        //Asigna el URL del video al controller para que el Stage generado pueda utilizarlo.
        Controller.setNombreVideo(nombre);

        //Se genera un media player en un nuevo stage.
        generarMediaPlayer();

    }

    /**
     * Se encarga de generar un stage con un media player.
     */
    private void generarMediaPlayer() {
        Stage stage = new Stage();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/MediaPlayerFXML.fxml"));
            //When the root is clicked the y and x variable gets the x and y position of the mouse.
            //Lambda Expressions.
            root.setOnMousePressed(mouseEvent -> {
                xOffset = mouseEvent.getSceneX();
                yOffset = mouseEvent.getSceneY();
            });

            //When the root is dragged set the position to the new x and y position of the mouse.
            root.setOnMouseDragged(mouseEvent -> {
                stage.setX(mouseEvent.getScreenX() - xOffset);
                stage.setY(mouseEvent.getScreenY() - yOffset);
            });
            setDraggable(root);
            scene = new Scene(root);
            scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (mouseEvent.getClickCount() == 2) {
                        stage.setFullScreen(true);
                    } else if (mouseEvent.getClickCount() == 3) {
                        stage.setFullScreen(false);
                    }
                }
            });
            stage.centerOnScreen();
            stage.setScene(scene);
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.show();
            super.stage = stage;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Método que se encarga de transferir el nombre de un video al controller de videos,
     * recibe el URL Relativo y lo retorna.
     *
     * @param nombreVideo: Nombre del video que se enviará.
     * @return URL del video.
     */
    private String obtenerURLRelativoVideo(String nombreVideo) {
        return controllerVideo.obtenerURLRelativo(this.IDUsuario, nombreVideo);
    }

    /**
     * Método que se encarga de mostrar el StackPane para subir los videos.
     */
    public void stackPaneSubirVideo() {

        //Se controla el cambio de color en el VBox
        this.agregarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));
        this.videosSubidos.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.eliminarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));


        //Se controla el nivel visual de los StackPanes.
        this.subirVideos.setVisible(true);
        this.baulVideos.setVisible(false);
        this.eliminarVideos.setVisible(false);
    }

    /**
     * Método que se encarga de mostrar el StackPane para listar los videos.
     */
    private void stackPaneListarVideos() {

        //Se controla el cambio de color en el VBox
        this.agregarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.videosSubidos.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));
        this.eliminarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));

        //Se controla el nivel visual de los StackPanes.
        this.subirVideos.setVisible(false);
        this.baulVideos.setVisible(true);
        this.eliminarVideos.setVisible(false);

        //Se consultan los videos a la base de datos.
        this.listarVideos();
    }

    /**
     * Método que se encarga de mostrar el StackPane para eliminar los videos.
     */
    public void stackPaneEliminarVideos() {

        //Se controla el cambio de color en el VBox
        this.agregarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.videosSubidos.setBackground(new Background(new BackgroundFill(Paint.valueOf("#222222"), null, null)));
        this.eliminarVideo.setBackground(new Background(new BackgroundFill(Paint.valueOf("#380e7f"), null, null)));

        //Se controla el nivel visual de los StackPanes.
        this.subirVideos.setVisible(false);
        this.baulVideos.setVisible(false);
        this.eliminarVideos.setVisible(true);

        /*
          Se borra la memoria del list view
         */
        this.listViewVideos.getItems().clear();

        //Se listan los nombres de los videos
        listarNombresVideos(this.IDUsuario);

    }

    /**
     * Método que se encarga de lista el nombre de los videos.
     */
    private void listarNombresVideos(int IDUsuario) {
        ArrayList<String> videos = controllerVideo.obtenerNombreVideos(IDUsuario);
        this.listViewVideos.getItems().addAll(videos);
    }

    /**
     * Selecciona la localización del video.
     */
    private void seleccionarLocalizacionVideo() {
            this.notificacionNombre.setVisible(false);
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Video Files",
                    "*.mp4",
                    "*.avi",
                    "*.wmv",
                    "*.mov"));

            File archivoVideo = fileChooser.showOpenDialog(this.stage);

            //Si el video seleccionado no es nulo.
            if (archivoVideo != null) {
                String nombreVideo = archivoVideo.getName();

                int puntoExtension = nombreVideo.lastIndexOf(".");

                String extensionArchivo = nombreVideo.substring(puntoExtension);

                //Se guarda la extensión del video seleccionado en una variable global.
                this.extensionVideo = extensionArchivo;

                //Se crea el video con el nombre proporcionado por el usuario y la extensión obtenida.
                //String nombreNuevoArchivo =
                        //(this.campoNombreVideo.getText().replaceAll(" ", "_") + extensionArchivo).trim();

                //El estándar es: imageAB23DEAQ.png <image> <code> <file-extension>
                String nombreNuevoArchivo = ("video" + CodeGenerator.generateAlphaCode(8) + extensionArchivo).trim();

                //Se valida en la base de datos si el nombre del archivo ya está asociado a un video.
                if (controllerVideo.validarExistenciaVideo(this.IDUsuario,
                        (this.campoNombreVideo.getText().replaceAll(" ", "_").trim() + extensionArchivo).trim())){
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("EL NOMBRE DEL VIDEO YA ESTÁ EN USO");
                    super.errorNotification.setContentText(" ");
                    super.errorNotification.showAndWait();
                    return;
                }

                try {
                    File directorioDestino;
                    String rutaDirectorioDestino =
                            ".\\src\\Usuarios\\" + controllerUsuario.obtenerIdentificacionUsuario(IDUsuario) + "\\Videos\\";

                    directorioDestino = new File(rutaDirectorioDestino);

                    //Se intenta crear el directorio en caso de que no haya sido creado anteriormente.
                    directorioDestino.mkdirs();

                    //Ahora se crea la copia del archivo, pero con el nombre que se generó antes y se indica la ruta de
                    // la carpeta destino.
                    File copiaArchivo;

                    copiaArchivo = new File(rutaDirectorioDestino + nombreNuevoArchivo);

                    Controller.copyFileUsingStream(archivoVideo, copiaArchivo);

                    //Este path será guardado en la base de datos.
                    this.URLVideo = copiaArchivo.getPath();
                    this.seleccionarLocalizacionBtn.setStyle("-fx-background-color: #218838");
                    this.notificacionVideo.setVisible(true);
                    this.notificacionVideo.setText("VIDEO SELECCIONADO");
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método que se encarga de realizar todos los procedimientos para subir videos a la base de datos.
     */
    private void subirVideo() {

        //Almacena los datos escritos en variables temporales con un formato adecuado.
        this.nombreVideo = this.campoNombreVideo.getText().replaceAll(" ", "_").trim();
        this.descripcionVideo = this.campoDescripcionVideo.getText().trim();

        //Valida que los datos escritos sean correctos.
        boolean camposViolados = validarDatos();
        //Si los campos son válidos.
        if (!camposViolados) {


            //Se guarda la duración total del video seleccionado.
            this.guardarDuracionVideo();

            HashMap<String, Object> datos_video = new HashMap<>();
            datos_video.put("NombreVideo", (nombreVideo + extensionVideo).trim());
            datos_video.put("DescripcionVideo", descripcionVideo);
            datos_video.put("CalificacionVideo", (int) calificacion.getRating());
            if (duracionTotalVideo > .0) {
                datos_video.put("DuracionTotalVideo", duracionTotalVideo);
            }
            if (!(URLVideo.isEmpty())) {
                datos_video.put("URLVideo", URLVideo);
            }
            if (IDUsuario > -1) {
                datos_video.put("IDUsuario", IDUsuario);
            }

            //Se almacena el ID del video en una variable global.
            this.IDVideo = controllerVideo.registrarVideo(datos_video);

            //Se transfiere el map al controller del video
            if (this.IDVideo > -1) {

                //Si se seleccionó una categoría se consulta su ID en la base de datos.
                if (!(this.categoriaVideo.getSelectionModel().isEmpty())) {
                    String categoriaSeleccionadaVideo = this.categoriaVideo.getValue();
                    this.IDCategoria = this.obtenerIDCategoria(categoriaSeleccionadaVideo);
                    //Si el ID de la categoría es un ID válido, se inserta en la BD.
                    if (IDCategoria > -1) {
                        controllerVideo.actualizarCategoria(this.IDCategoria, this.IDVideo);
                        //Se borra la memoria a su estado inicial.
                        this.IDCategoria = -1;
                    }
                }
                super.informationNotification.setTitle("NOTIFICACION");
                super.informationNotification.setContentText("VIDEO REGISTRADO");
                super.informationNotification.setHeaderText(" ");
                super.informationNotification.showAndWait();
            } else {
                super.errorNotification.setTitle("ERROR");
                super.errorNotification.setHeaderText("EL VIDEO NO PUDO SER REGISTRADO");
                super.errorNotification.setContentText("Inténtelo más tarde...");
                super.errorNotification.showAndWait();
            }

            //Se borra la información de los campos.
            this.seleccionarLocalizacionBtn.setStyle("-fx-background-color: #222222");
            this.notificacionVideo.setVisible(false);
            this.campoNombreVideo.setText("");
            this.campoDescripcionVideo.setText("");
            this.categoriaVideo.setValue("");
            this.calificacion.setRating(0);
        }

    }

    /**
     * Método que se encarga de validar que todos los datos sean correctos.
     *
     * @return true si alguno de los campos ha sido violado.
     */
    private boolean validarDatos() {
        boolean camposViolados = false;
        if (validarCampoComunNumerosCenfotec(nombreVideo, this.notificacionNombre)) {
            camposViolados = true;
        } else if (validateMaxChars(nombreVideo, 90)) {
            this.notificacionNombre.setVisible(true);
            this.notificacionNombre.setText("No puede ingresar más de 90 caracteres.");
            camposViolados = true;
        } else {
            this.notificacionNombre.setVisible(false);
        }
        //Si la descripción no está vacía, porque puede estarlo.
        if (!this.descripcionVideo.isEmpty()) {
            if (this.validarCampoComunCenfotec(descripcionVideo, notificacionDescripcion)) {
                camposViolados = true;
            }
        }
        if (this.URLVideo.isEmpty()) {
            this.notificacionVideo.setVisible(true);
            this.notificacionVideo.setText("Se debe seleccionar una ubicación");
            camposViolados = true;
        }

        if (this.calificacion.getRating() < 1) {
            camposViolados = true;
            this.notificacionEstrellas.setVisible(true);
            this.notificacionEstrellas.setText("Debe calificar el video");
        } else {
            this.notificacionEstrellas.setVisible(false);
        }
        return camposViolados;
    }

    /**
     * Método que se encarga de obtener la duración total de un video.
     * Se utiliza la librería Apache TIKA solamente para obtener el metadato del archivo.
     */
    public void guardarDuracionVideo() {
        if (!(this.URLVideo.isEmpty() || this.URLVideo.equalsIgnoreCase(" "))) {
            BodyContentHandler handler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            try {
                FileInputStream inputStream = new FileInputStream(new File(this.URLVideo));
                ParseContext pContext = new ParseContext();
                MP4Parser MP4Parser = new MP4Parser();
                MP4Parser.parse(inputStream, handler, metadata, pContext);
                String[] metadataNames = metadata.names();
                for (String name : metadataNames) {
                    if (name.equalsIgnoreCase("xmpDM:duration")) {
                        duracionTotalVideo = Double.parseDouble(metadata.get(name));
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (TikaException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            super.errorNotification.setTitle("ALERTA");
            super.errorNotification.setHeaderText("EL URL no ha sido definido");
            super.errorNotification.setContentText(" ");
            super.errorNotification.showAndWait();
        }
    }

    /**
     * Método que se encarga de listar todas las categorías registradas en un ComboBox.
     */
    public void listarCategorias() {
        this.categoriaVideo.getItems().clear();
        ArrayList<String> categorias = controllerCategoria.obtenerCategorias();
        for (String e : categorias) {
            this.categoriaVideo.getItems().add(e);
        }
        this.categoriaVideo.show();
    }

    /**
     * Método que se encarga de obtener el ID de la categoría elegida para el video.
     *
     * @return ID de la categoría o -1 si no pudo ser encontrada.
     */
    public int obtenerIDCategoria(String nombreCategoria) {
        return controllerCategoria.obtenerIDCategoria(nombreCategoria);

    }

    /**
     * Método que se encarga de inicializar el TableView para listar los videos.
     */
    public void listarVideos() {
        //Se indica el valor por defecto cuando no hay videos subidos.
        this.tablaVideos.setPlaceholder(new Label("No hay videos registrados"));

        //Se indican los value factories de las columnas.
        this.columnaNombre.setCellValueFactory(new PropertyValueFactory<VideoModelTable, String>("nombre"));
        this.columnaDescripcion.setCellValueFactory(new PropertyValueFactory<VideoModelTable, String>("descripcion"));
        this.columnaFechaRegistro.setCellValueFactory(new PropertyValueFactory<VideoModelTable, String>(
                "fechaRegistro"));
        this.columnaCalificacion.setCellValueFactory(new PropertyValueFactory<VideoModelTable, Double>("calificacion"));
        this.columnaCategoria.setCellValueFactory(new PropertyValueFactory<VideoModelTable, String>("categoria"));
        this.columnaDuracion.setCellValueFactory(new PropertyValueFactory<VideoModelTable, Double>("duracion"));

        //Se consultan los videos y se agrega el ObservableList retornado.
        this.tablaVideos.setItems(obtenerVideos(this.IDUsuario));
    }

    /**
     * Método que se encarga de obtener los videos del usuario.
     *
     * @param IDUsuario: ID del usuario que inició sesión.
     * @return ObservableList de tipo VideoModelTable
     */
    public ObservableList<VideoModelTable> obtenerVideos(int IDUsuario) {

        //ObservableList de Videos
        ObservableList<VideoModelTable> videos = FXCollections.observableArrayList();
        VideoModelTable vmtObj;
        //ESTO ES LO QUE HAY QUE CAMBIAR AHORA.
        HashMap<Integer, VideoModelTable> map = controllerVideo.obtenerVideos(IDUsuario);
        for (Map.Entry<Integer, VideoModelTable> entry : map.entrySet()) {
            double duracionSegundosVideo = Double.parseDouble(entry.getValue().getDuracion());
            int calificacionVideo = Integer.parseInt(entry.getValue().getCalificacion());
            String nombreVideo = entry.getValue().getNombre();
            String descripcionVideo = entry.getValue().getDescripcion();
            String fechaCreacion = entry.getValue().getFechaRegistro();
            //Se obtiene la categoría con su ID.
            //Si el usuario definio una categoria, se consulta su nombre en la base de datos.
            String tempCategoria;
            int IDTempCategoria = -1;
            //Se consulta el ID temporal del video.
            int IDVideo = controllerVideo.obtenerIDVideo(nombreVideo, this.IDUsuario);
            //Se consuta el ID temporal de la categoría del video tempral.
            if (IDVideo > -1) {
                IDTempCategoria = controllerVideo.obtenerIDCategoria(IDVideo);
            }
            if (IDTempCategoria > -1) {
                tempCategoria = controllerCategoria.obtenerNombreCategoria(IDTempCategoria);
            } else {
                tempCategoria = "INDEFINIDA";
            }
            String calificacionDetalle = "";
            if (calificacionVideo == 1) {
                calificacionDetalle = "MALO";
            } else if (calificacionVideo == 2) {
                calificacionDetalle = "BUENO";
            } else {
                calificacionDetalle = "MUY BUENO";
            }
            long duracionVideo = (long) duracionSegundosVideo;
            String duracionTotalStr;
            if (duracionSegundosVideo >= 60) {
                duracionTotalStr =
                        (TimeUnit.SECONDS.toMinutes(duracionVideo - (TimeUnit.SECONDS.toHours(duracionVideo * 60))) +
                                " min.");
            } else {
                duracionTotalStr = duracionVideo + " seg.";
            }
            vmtObj = new VideoModelTable(
                    nombreVideo,
                    descripcionVideo,
                    fechaCreacion,
                    calificacionDetalle,
                    tempCategoria,
                    duracionTotalStr);

            videos.add(vmtObj);

        }
        return videos;
    }

    private void eliminarVideos(int IDUsuario, ObservableList<String> listaVideos) {
        //Si los elementos seleccionados son mayores a cero
        if (this.listViewVideos.getSelectionModel().getSelectedItems().size() > 0) {

            /*
              Se obtiene el URL del video y se intenta eliminar de la carpeta del usuario.
              Si el video local no se elimina no es problema, ya que su registro en la base de datos sí.
              Y por ende se puede realiza sobreescritura.
             */

            for (String video : listaVideos) {
                String URLVideo = controllerVideo.obtenerURLRelativo(IDUsuario, video);
                File archivoVideo = new File(URLVideo);
                archivoVideo.delete();
            }

            /*
                Se elimina el video de la base de datos.
             */
            for (String video : listaVideos) {
                controllerVideo.eliminarVideo(IDUsuario, video);
            }

            //Se borra la memoria del list view.
            this.listViewVideos.getItems().clear();

            //Se listan nuevamente los items.
            this.listarNombresVideos(this.IDUsuario);

            super.informationNotification.setTitle("NOTIFICACION");
            super.informationNotification.setHeaderText("CONTENIDO ELIMINADO");
            super.informationNotification.setContentText(" ");
            super.informationNotification.showAndWait();

        }
    }

}
