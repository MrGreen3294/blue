package cr.ac.ucenfotec.UI.ApplicationControllers;

import cr.ac.ucenfotec.TL.ControllerUsuario;
import cr.ac.ucenfotec.Utilities.CodeGenerator;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.ResourceBundle;

public class RegistrarUsuarioController extends Controller implements Initializable {


    //Atributos de la vista FXML
    @FXML
    private BorderPane panelCrearCuenta;

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombre;

    @FXML
    private Label notificacionApellido;

    @FXML
    private TextField campoApellido;

    @FXML
    private Label notificacionSegundoApellido;

    @FXML
    private TextField campoSegundoApellido;

    @FXML
    private PasswordField campoClaveSeguridad;

    @FXML
    private Label notificacionClaveSeguridad;

    @FXML
    private TextField campoCorreoElectronico;

    @FXML
    private Label notificacionCorreoElectronico;

    @FXML
    private DatePicker campoFechaNacimiento;

    @FXML
    private Label notificacionFechaNacimiento;

    @FXML
    private ImageView avatar;

    @FXML
    private Label notificacionAvatar;

    @FXML
    private Button seleccionarImagen;

    @FXML
    private Label notificacionNombreUsuario;

    @FXML
    private TextField campoNombreUsuario;

    @FXML
    private TextField campoIdentificacion;

    @FXML
    private Label notificacionIdentificacion;

    @FXML
    private Button registrarse;

    @FXML
    private Label iniciarSesion;

    @FXML
    private BorderPane panelDireccion;

    @FXML
    private Button noAgregarDireccion;

    @FXML
    private Button agregarDireccion;

    //Variables para almacenar los datos de los campos gráficos con un formato adecuado.
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String claveSeguridad;
    private static String correoElectronico;
    private LocalDate fechaNacimiento;
    private String nombreUsuario;
    private String identificacion;
    private String avatarPath;

    /**
     * Retorna el correo electrónico del usuario.
     * @return correo electrónico.
     */
    public static String getCorreoElectronico() {
        return correoElectronico;
    }

    //Variable para almacenar la llave PK del usuario registrado.
    private int primaryKey;

    //Instancia para enviar la información al Controller del usuario general.
    private ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Variable tipo FileChooser para seleccionar la imagen del usuario.
    private FileChooser fileChooser;

    //Variable booleana que se utiliza para saber si se quiere validar los datos o no.
    private final boolean VALIDAR = true;

    //Variable que almacena el código generado para el usuario específico.
    private String codigoGenerado;

    //Variable temporal que almaceanará la ruta de los baules de los usuarios.
    String rutaBaulUsuarios;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Indica un título para las alertas obtenidas de la clase padre.
        super.errorNotification.setTitle("ERROR");
        super.informationNotification.setTitle("INFORMACION");

        //En caso de que el usuario presione el botón para registrarse, se llamará al método registrarUsuarioFinal().
        this.registrarse.setOnAction(actionEvent -> {
            registrarUsuarioFinal();
        });

        //En caso de que el usuario presione el botón de agregar una imagen, se llamará al método seleccionarImagen().
        this.seleccionarImagen.setOnAction(actionEvent ->  {
            seleccionarImagen();
        });

        this.avatar.setOnMouseClicked(mouseEvent -> {
            seleccionarImagen();
        });

        //En caso de que el usuario presione el botón de login
        this.iniciarSesion.setOnMouseClicked(mouseEvent -> {
            //Se cambia la escena, se proporciona cualquier nodo.
            changeSceneToLogin(this.iniciarSesion);
        });

        //Ruta del directorio de archivos de cuenta.
        rutaBaulUsuarios = ".\\src\\Usuarios";

        File baul = new File(rutaBaulUsuarios);

        //Si el baúl no existe se crea
        baul.mkdir();

    }

    public void registrarUsuarioFinal(){

        //Almacenar los datos de los campos gráficos en datos internos.
         nombre = this.campoNombre.getText().trim();
         primerApellido = this.campoApellido.getText().trim();
         segundoApellido = this.campoSegundoApellido.getText().trim();
         claveSeguridad = this.campoClaveSeguridad.getText().trim();
         correoElectronico = this.campoCorreoElectronico.getText().trim();
         fechaNacimiento = this.campoFechaNacimiento.getValue();
         nombreUsuario = this.campoNombreUsuario.getText().trim();
         identificacion = this.campoIdentificacion.getText().trim();
         if(avatarPath == null){
             this.avatarPath = ""; //Valor por defecto.
         }
         boolean camposViolados = false;

         //Validar que todos los campos sean válidos.
        if(VALIDAR)
        {
            camposViolados = validarCampos();
        }

        //Si los campos no fueron violados
        if(!camposViolados){

            boolean usuarioExiste = false;
            //Validar que ese usuario no exista en la base de datos.
            usuarioExiste = validarExistenciaUsuario(identificacion);
            if(usuarioExiste) {
                //Si el nombre de usuario existe se mostrará una alerta
                super.errorNotification.setContentText("Un usuario final con la identificación \"" + identificacion +
                        "\" ya existe en el sistema.");
                super.errorNotification.showAndWait();
                //En caso de que se haya registrado una imagen, se borra de la cuenta.
                if(new File(this.avatarPath).delete()){
                    System.err.println("La imagen fue removida de la cuenta");
                }
                //Se vuelve a habilitar las opciones para que establezca una imagen.
                this.seleccionarImagen.setDisable(false);
                //Se borra la imagen en el Image View
                this.avatar.setImage(null);
                //Se borra el path relativo de la imagen
                this.avatarPath = "";
            }
            //Si el usuario no existe
            else{
                boolean correoExiste = false;
                //Validar que ese usuario no tenga un correo electrónico que ya tiene un usuario en el sistema.
                correoExiste = controllerUsuario.validarExistenciaCorreo(correoElectronico);
                if(correoExiste){
                    super.errorNotification.setContentText("Ya existe una cuenta con el correo \n" + correoElectronico + "\n Inicie sesión o restablezca su clave de seguridad");
                    super.errorNotification.showAndWait();
                }else {

                    //Validar que el usuario no tenga un nombre de usuario que ya existe en el sistema.
                    boolean apodoExiste = controllerUsuario.validarExistenciaApodo(nombreUsuario);
                    if (apodoExiste) {
                        this.notificacionNombreUsuario.setVisible(true);
                        this.notificacionNombreUsuario.setText("El nombre de usuario ya está en uso en el sistema");

                    } else {
                        if (this.notificacionNombreUsuario.isVisible()) {
                            this.notificacionNombreUsuario.setVisible(false);
                        }

                        //Se crea un HashMap para enviar los datos más asegurados.
                        HashMap<String, String> map = new HashMap<>();
                        try {
                            map.put("Nombre", nombre);
                            map.put("PrimerApellido", primerApellido);
                            map.put("SegundoApellido", segundoApellido);
                            map.put("ClaveSeguridad", claveSeguridad);
                            map.put("CorreoElectronico", correoElectronico);
                            map.put("FechaNacimiento", fechaNacimiento.toString());
                            map.put("NombreUsuario", nombreUsuario);
                            map.put("Identificacion", identificacion);
                            map.put("Avatar", avatarPath);
                        } catch (NullPointerException e) {
                            super.errorNotification.setContentText("Los datos están vacíos, inténtelo otra vez.");
                            super.errorNotification.showAndWait();
                            return;
                        }
                        //Se envia la información a la base de datos y se retorna el ID.
                        int IDUsuarioFinal = enviarDatosUsuario(map);
                        if (IDUsuarioFinal != -1) {
                            super.informationNotification.setTitle("CUENTA REGISTRADA");
                            super.informationNotification.setContentText("La cuenta ha sido registrada exitosamente");
                            super.informationNotification.showAndWait();
                            //Guarda la llave en la variable global
                            primaryKey = IDUsuarioFinal;
                            //Se crea el baúl del usuario, en caso de que no haya sido creado al seleccionar su avatar.
                            File baulUsuario =
                                    new File(this.rutaBaulUsuarios + "\\" + this.identificacion);
                            //Se crea el baúl de la imagenes, en caso de que no haya sido creado al seleccionar su
                            // avatar.
                            File baulImagenesUsuario =
                                    new File(this.rutaBaulUsuarios + "\\" + this.identificacion +
                            "\\Imagenes\\");
                            //Se crea el baúl de los playlist o listas de reproducción.
                            File baulPlayListUsuario =
                                    new File(this.rutaBaulUsuarios + "\\" + this.identificacion +
                                    "\\PlayList\\");
                            //Se crea el baúl de los videos
                            File baulVideosUsuario = new File(this.rutaBaulUsuarios + "\\" + this.identificacion +
                                    "\\Videos\\");

                            baulUsuario.mkdir();
                            baulImagenesUsuario.mkdir();
                            baulPlayListUsuario.mkdir();
                            baulVideosUsuario.mkdir();

                            //Deshabilita el botón siempre y cuando la variable VALIDAR sea igual a true.
                            if (VALIDAR) {
                                this.registrarse.setDisable(true);
                            }
                            //Una vez que la cuenta se registra se establece un código de confirmación para ese usuario.
                            boolean codigoGenerado = controllerUsuario.insertarCodigoConfirmacion(IDUsuarioFinal);
                            if (!codigoGenerado) {
                                super.errorNotification.setTitle("ERROR AL GENERAR EL CÓDIGO");
                                super.errorNotification.setHeaderText("EL CÓDIGO NO PUDO SER GENERADO");
                                super.errorNotification.setContentText("No se pudo generar el código de confirmación");
                                super.errorNotification.showAndWait();
                            } else {
                                //Se almacena el código generado en una variable temporal para enviarlo a otro Controller.
                                this.codigoGenerado = controllerUsuario.consultarCodigoConfirmacion(IDUsuarioFinal);
                            }
                        } else {
                            super.errorNotification.setTitle("ERROR A LA HORA DE REGISTRAR");
                            super.errorNotification.setContentText("A ocurrido una complicación desconocida a la hora de " +
                                    "registrar");
                            super.errorNotification.showAndWait();
                            return;
                        }
                        //Si el proceso sale bien, se procedee a cambiar la escena por el BorderPane encima del BorderPane
                        // actual. En esta nuevo contenedor el usuario tendrá la opción de ingresar una dirección o no.
                        //Se deshabilita la visualización del primero.
                        if(super.controllerDireccion.validarExistenciaDireccionCompleta()) {
                            this.panelCrearCuenta.setVisible(false);
                            this.panelDireccion.setVisible(true);

                            //En caso de que el usuario presione el botón de "Sí", se llamará al método agregarDireccion()
                            agregarDireccion.setOnAction(actionEvent -> {
                                agregarDireccion();
                            });
                            //En caso de que el usuario presione el botón "No", se llamará al método confirmarCodigoUsuario()
                            noAgregarDireccion.setOnAction(actionEvent -> {
                                confirmarCodigoGenerado();
                            });
                        }else{
                            confirmarCodigoGenerado();
                        }
                    }
                }
            }
        }
    }



    /**
     * Valida todos los campos que el usuario rellena en la vista.
     * @return false en caso de que todos los campos sean válidos.
     */
    public boolean validarCampos(){
        boolean campoViolado = false;

        //Validar el nombre
        if(super.validarCampoComunCenfotec(nombre, notificacionNombre))
            campoViolado = true;
        //Validar el primer apellido
        if(super.validarCampoComunCenfotec(primerApellido, notificacionApellido))
            campoViolado = true;
        //Validar el segundo apellido
        if(super.validarCampoComunCenfotec(segundoApellido, notificacionSegundoApellido))
            campoViolado = true;
        //Validar el nombre de usuario
        if(super.validarCampoComunCenfotec(nombreUsuario, notificacionNombreUsuario))
            campoViolado = true;
        //Validar la identificacion
        if(super.validarIdentificacionCenfotec(identificacion, notificacionIdentificacion))
            campoViolado = true;
        //Validar correo electrónico
        if(super.validarCorreoCenfotec(correoElectronico, notificacionCorreoElectronico))
            campoViolado = true;
        //Validar la fecha de nacimiento
        if(super.validarFechaNacimientoCenfotec(fechaNacimiento, notificacionFechaNacimiento))
            campoViolado = true;
        //Validar la clave de seguridad
        if(super.validarClaveSeguridadCenfotec(claveSeguridad, notificacionClaveSeguridad))
            campoViolado = true;

        return campoViolado;

    }

    /**
     * Crea una ventana donde se podrá registrar una dirección.
     */
    public void agregarDireccion(){

        //Se obtiene de cualquier nodo la información de la ventana.
         super.stage = (Stage) registrarse.getScene().getWindow();

         //Si ya existe una escena, entonces se esconde.
        if(scene != null){
            stage.hide();
        }
        try {
            //Se carga el FXML de la nueva escena.
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews" +
                    "/AgregarDireccionFXML" +
                    ".fxml"));
            Parent root = fxml.load();

            //Ahora se obtiene su controller.
            AgregarDireccionController fxmlController =
                    (AgregarDireccionController) fxml.getController();

            //Ahora se envían algunos datos al controller padre para que todos los controllers hijos los puedan
            // visualizar.
            //Se envía el primary key del usuario recién registrado.
            //NOTA IMPORTANTE: El controller será cargado nuevamente por el controller de la vista AgregarDireccion
            //Por lo que los datos serán actualizados antes de entrar en la vista de confirmación del código del
            // usuario.
            Controller.setIDUsuario(this.primaryKey);

            //Se envía el correo electrónico del usuario para que otras vistas lo usen después.
            Controller.setCorreoElectronicoUsuario(correoElectronico);

            //Se envía el código generado para el usuario específico al controller padre.
            Controller.setCodigoGenericoUsuario(this.codigoGenerado);

            //Se envía el apodo para el usuario específico al controller padre.
            Controller.setApodoUsuario(this.nombreUsuario);

            //Se envía la clave de seguridad al controller padre.
            Controller.setClaveUsuario(this.claveSeguridad);

            //Se le indica al controller de este fxml que el controller está siendo utilizado para un registro.
            fxmlController.setVentanaRegistro(true);

            //Luego se muestra la escena con el fxml.
            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.setDraggable(root);
            super.stage.show();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Cambia a la escena para confirmar el código que se generó para el usuario registrado.
     */
    public void confirmarCodigoGenerado(){
        //Se obtiene de cualquier nodo la información de la ventana actual.
        super.stage = (Stage) registrarse.getScene().getWindow();

        //Si ya existe una escene, entonces se esconde
        if(scene != null){
            stage.hide();
        }
        try {

            //NOTA IMPORTANTE
            //El controlador se tiene que actualizar primero antes de que la vista lo cargue, ya que en
            //este caso la vista se cambiará directamente y por ende el controller debe ser actualizado antes de su
            // carga. Si al controller se le asignan las variables estáticas después de cargar la vista,
            // esta no lo actualizará, ya que carga el controller luego de actualizar los valores.

            //Se envía el correo electrónico del usuario para que otras vistas lo utilicen.
            Controller.setCorreoElectronicoUsuario(correoElectronico);

            //Se envía el PK del usuario que está utilizando las vistas.
            Controller.setIDUsuario(this.primaryKey);

            //Se envía el código generico del usuario
            Controller.setCodigoGenericoUsuario(this.codigoGenerado);

            //Se envía el apodo para el usuario específico al controller padre.
            Controller.setApodoUsuario(this.nombreUsuario);

            //Se envía la clave de seguridad al controller padre.
            Controller.setClaveUsuario(this.claveSeguridad);

            //Se carga el FXML de la nueva escena
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/ConfirmarCodigoUsuarioFXML.fxml"));

            Parent root = fxml.load();

            super.setDraggable(root);
            //Se muestra la escena con el FXML
            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();

        }catch(IOException | NullPointerException ignored){
            super.changeSceneToLogin(this.agregarDireccion);
        }

    }
    /**
     * Le envía al Controller la identificación para validar si ya existe un usuario con esa identificación en la
     * base de datos.
     * @param identificacion: identificacion del usuario.
     * @return true si el nombre de usuario existe.
     */
    public boolean validarExistenciaUsuario(String identificacion){
        //Aquí va la consulta a la base de datos.
        return controllerUsuario.validarExistenciaUsuario(identificacion);
    }

    /**
     * Le envía al Controller un HashMap Collection con los datos del usuario para registrarlo en la base de datos.
     * @param datos HashMap con los datos del usuario.
     * @return el ID generado del usuario insertado.
     */
    public int enviarDatosUsuario(HashMap<String, String> datos){
        //Se transfiere la información por el Controller.
        return controllerUsuario.registrarUsuario(datos);
    }

    /**
     * Permite que se seleccione una imagen en el registro.
     */
    public void seleccionarImagen() {

        //Se valida la identificación, ya que será necesaria para crear el baúl de datos del usuario.
        if(!(this.campoIdentificacion.getText().isEmpty() || this.campoIdentificacion.getText().equalsIgnoreCase(""))){
            //El correo no cumple con el estandar retorna true y si no lo cumple sale del método.
            if(validarIdentificacionCenfotec(this.campoIdentificacion.getText(), this.notificacionIdentificacion)){
                return;
            }
        }else{
            //Si el video está vacío.
            super.errorNotification.setContentText("La identificación es requerida para ejecutar esta acción");
            super.errorNotification.showAndWait();
            return;
        }

        //Se valida que la identificacion ingresada no sea la de un usuario registrado.
        if(controllerUsuario.validarExistenciaUsuario(this.campoIdentificacion.getText())){
            super.errorNotification.setTitle("ERROR");
            super.errorNotification.setContentText("Ya existe un usuario con esta identificación");
            super.errorNotification.showAndWait();
            return;
        }

        fileChooser = new FileChooser();

        //Extension Filters Configuration: Filters the file extensions available
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Images Files",
                        "*.png",
                        "*.jpg",
                        "*.gif",
                        "*.jpeg")
        );

        //Abre una ventana para seleccionar el archivo de la imagen.
        File archivo = fileChooser.showOpenDialog(null);

        //Si se seleccionó un archivo válido entra en el condicional.
        if (archivo != null) {

            // Paso 1: Encontrar la extensión del archivo

            //Obtiene el nombre del archivo, por ejemplo: icons8-lock-192.png
            String imageName = archivo.getName();

            //Con el nombre del archivo busca donde haya un punto para obtener la extensión.
            int imageDotIndex = imageName.lastIndexOf(".");

            //Con la posición del punto parte el String para obtener solamente la extensión del archivo.
            String imageFileExtension = imageName.substring(imageDotIndex);

            //Step 2: Crea un nuevo nombre para la copia que se creará posteriormente, usa la extensión del archivo.

            //El estándar es: imageAB23DEAQ.png <image> <code> <file-extension>
            String standardImageName = "image" + CodeGenerator.generateAlphaCode(8) + imageFileExtension;

            //Paso 3: Crea una copia del archivo, pero a este le asigna el nombre generado anteriormente.
            //El código siguiente asegura que el nombre del archivo no esté repetido.
            try {

                //Intenta crear un nuevo directorio con el nombre "UserAvatars" en el caso que no haya sido creado.
                boolean nombreRepetido = true;
                File directorioDestino;

                String rutaDirectorioDestino = ".\\src\\Usuarios\\" + this.campoIdentificacion.getText() +
                        "\\Imagenes\\";

                directorioDestino = new File(rutaDirectorioDestino);

                directorioDestino.mkdirs();

                //Ahora se crea la copia del archivo, pero con el nombre que se generó antes y se indica la ruta de
                // la carpeta destino.
                File copiaArchivo;
                do {
                    copiaArchivo = new File(rutaDirectorioDestino + standardImageName);
                    //Valida que el nombre no esté repetido.
                    nombreRepetido = copyFileUsingStream(archivo, copiaArchivo);
                } while (nombreRepetido);

                //Finalmente se asigna la imagen al image view.
                if (avatar != null) {

                    //Este path será guardado luego en la base de datos.
                    String path = copiaArchivo.getPath();
                    File file = new File(path);
                    this.avatarPath = file.toString();

                    //Convierte el path relativo a un path absoluto dependiendo del árbol del sistema de ficheros de
                    // cada equipo.
                    String conversion = file.toURI().toString();
                    Image imagenSeleccionada = new Image(conversion);

                    avatar.setImage(imagenSeleccionada);

                    //Deshabilita los botones de seleccion siempre y cuando la variable VALIDAR esté activa
                    if(VALIDAR) {
                        avatar.setDisable(true);
                        seleccionarImagen.setDisable(true);
                    }
                }
            } catch (IOException e) {
                System.err.println("Ocurrió una excepción a la hora de asignar la imagen");
                System.err.println(e.getMessage());
            }
        }
    }
}
