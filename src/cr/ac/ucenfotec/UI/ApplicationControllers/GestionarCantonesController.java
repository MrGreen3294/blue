package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class GestionarCantonesController extends Controller implements Initializable {


    @FXML
    private Label notificacionPais;

    @FXML
    private JFXComboBox<String> paisesComboBox;

    @FXML
    private Label notificacionProvincia;

    @FXML
    private JFXComboBox<String> provinciasComboBox;

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombreCanton;

    @FXML
    private Label notificacionCodigo;

    @FXML
    private TextField campoCodigo;

    @FXML
    private JFXButton agregarCantonBtn;

    @FXML
    private ImageView salirBtn;

    @FXML
    private JFXComboBox<String> paisCantonComboBox;

    @FXML
    private JFXComboBox<String> provinciaCantonComboBox;

    @FXML
    private JFXListView<String> listaCantones;

    @FXML
    private JFXButton listarDatos;

    @FXML
    private JFXButton eliminarCantonBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Paises Izquierda
        this.provinciaCantonComboBox.setDisable(true);
        this.paisCantonComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisCantonComboBox.getItems().clear();
            this.paisCantonComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
            this.provinciaCantonComboBox.setDisable(false);
        });

        //Paises Derecha
        this.provinciasComboBox.setDisable(true);
        this.paisesComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisesComboBox.getItems().clear();
            this.paisesComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
            this.provinciasComboBox.setDisable(false);
        });

        //Provincias Derecha
        this.provinciasComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            this.provinciasComboBox.getItems().clear();
            this.provinciasComboBox.getItems().addAll(super.controllerDireccion.listarProvincias(IDPais));
        });

        //Provincias Izquierda
        this.provinciaCantonComboBox.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.paisCantonComboBox.getSelectionModel().getSelectedItem();
            int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
            this.provinciaCantonComboBox.getItems().clear();
            this.provinciaCantonComboBox.getItems().addAll(super.controllerDireccion.listarProvincias(IDPais));
        });

        //Agregar Canton
        this.agregarCantonBtn.setOnAction(actionEvent -> {
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();
            String nombreProvincia = this.provinciasComboBox.getSelectionModel().getSelectedItem();
            String nombreCanton = this.campoNombreCanton.getText().trim();
            String codigoCanton = this.campoCodigo.getText().trim();

            boolean resultadoValidacion = this.validarDatos(nombrePais, nombreProvincia, nombreCanton, codigoCanton);

            if(!(resultadoValidacion)){
                registrarCanton(nombrePais, nombreProvincia, nombreCanton, codigoCanton);
            }
        });

        //Listar Cantones
        this.listarDatos.setOnAction(actionEvent -> {
            String paisSeleccionado = this.paisCantonComboBox.getSelectionModel().getSelectedItem();
            String provinciaSeleccionada = this.provinciaCantonComboBox.getSelectionModel().getSelectedItem();
            if(!(paisSeleccionado == null || paisSeleccionado.isEmpty())) {
                if(!(provinciaSeleccionada == null || provinciaSeleccionada.isEmpty())){
                    listarCantones();
                }else{
                    super.errorNotification.setHeaderText("Debe seleccionar una provincia");
                    super.errorNotification.setContentText("Indique el país");
                    super.errorNotification.setTitle("Error");
                    super.errorNotification.showAndWait();
                }
            }else{
                super.errorNotification.setHeaderText("Debe seleccionar un país");
                super.errorNotification.setContentText("Indique la provincia");
                super.errorNotification.setTitle("Error");
                super.errorNotification.showAndWait();
            }
        });

        //Eliminar Cantones
        this.eliminarCantonBtn.setOnAction(actionEvent -> {
            eliminarCanton();
        });

        //Salir al menú
        this.salirBtn.setOnMouseClicked(mouseEvent -> {
            super.stage = (Stage) this.salirBtn.getScene().getWindow();
            if (super.scene != null) {
                super.stage.hide();
            }
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelDireccionesFXML.fxml"));
                super.setDraggable(root);
                stage.setScene(new Scene(root));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void registrarCanton(String nombrePais, String nombreProvincia, String nombreCanton, String codigoCanton) {
        int IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
        int IDProvincia = super.controllerDireccion.solicitarIDProvincia(IDPais, nombreProvincia);
        super.controllerDireccion.agregarCanton(IDPais, IDProvincia, nombreCanton, codigoCanton);
        listarCantones();
    }

    private boolean validarDatos(String nombrePais, String nombreProvincia, String nombreCanton, String codigoCanton) {
        boolean datosInvalidos = false;

        if (super.validarCampoComunCenfotec(nombreProvincia, notificacionProvincia)) {
            datosInvalidos = true;
        }
        if (super.validarCampoComunCenfotec(nombrePais, notificacionPais)) {
            datosInvalidos = true;
        }
        if(super.validarCampoComunCenfotec(nombreCanton, notificacionNombre)){
            datosInvalidos = true;
        }
        if (super.validateIsNumber(codigoCanton)) {
            this.notificacionCodigo.setVisible(true);
            this.notificacionCodigo.setText("El dato debe ser un número");
            datosInvalidos = true;
        }else{
            this.notificacionCodigo.setVisible(false);
        }

        return datosInvalidos;
    }

    public void listarCantones() {
        int IDPais =
                super.controllerDireccion.solicitarIDPais(this.paisCantonComboBox.getSelectionModel().getSelectedItem());
        int IDProvincia =
                super.controllerDireccion.solicitarIDProvincia(IDPais,
                        this.provinciaCantonComboBox.getSelectionModel().getSelectedItem());
        this.listaCantones.getItems().clear();
        String[] cantonesRegistrados = super.controllerDireccion.listarCantones(IDPais, IDProvincia);
        for (String cantonRegistrado : cantonesRegistrados) {
            this.listaCantones.getItems().add(cantonRegistrado);
        }
    }

    public void eliminarCanton(){
        String paisSeleccionado = this.paisCantonComboBox.getSelectionModel().getSelectedItem();
        String provinciaSeleccionada = this.provinciaCantonComboBox.getSelectionModel().getSelectedItem();
        String nombreCanton = this.listaCantones.getSelectionModel().getSelectedItem();
        try {
            if (!(paisSeleccionado.isEmpty())) {
                if (!(provinciaSeleccionada.isEmpty())) {
                    if (!(nombreCanton.isEmpty())) {
                        super.controllerDireccion.eliminarCanton(paisSeleccionado, provinciaSeleccionada, nombreCanton);
                        listarCantones();
                    }
                }
            }
        }catch(NullPointerException ignored){

        }
    }

}
