package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class TitleBarController implements Initializable {
    @FXML
    private ImageView minimize;

    @FXML
    private ImageView fullscreen;

    @FXML
    private ImageView exit;

    private Stage currentStage;

    private boolean switcher = false;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Minimize current stage
        minimize.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                currentStage = (Stage) minimize.getScene().getWindow();
                currentStage.setIconified(true);
            }
        });

        //FullScreen-Switcher current stage
        fullscreen.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                currentStage = (Stage) fullscreen.getScene().getWindow();
                if(switcher){
                    currentStage.setFullScreen(true);
                    currentStage.setResizable(false);
                    switcher = false;
                }else{
                    currentStage.setFullScreen(false);

                    switcher = true;
                }
            }
        });

        //Exit
        exit.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Platform.exit();
            }
        });

    }
}
