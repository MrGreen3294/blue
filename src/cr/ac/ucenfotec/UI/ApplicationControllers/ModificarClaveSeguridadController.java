package cr.ac.ucenfotec.UI.ApplicationControllers;

import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ModificarClaveSeguridadController extends Controller implements Initializable {


    @FXML
    private TextField campoClave;

    @FXML
    private TextField campoClaveConfirmacion;

    @FXML
    private Label notificacionClaves;

    @FXML
    private Button cambiarClave;

    @FXML
    private Label iniciarSesion;


    //Variable temporal que almacenará el ID del usuario que está cambiando su clave de seguridad.
    int IDUsuario = Controller.getIDUsuario();

    //Variable temporal que será utilizada para establecer la comunicación con el controller del usuario.
    ControllerUsuario controllerUsuario = new ControllerUsuario();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Cuando el botón para cambiar la clave sea presionado, se llamará al método cambiar clave.
        this.cambiarClave.setOnAction(actionEvent -> {
            this.cambiarClaveSeguridad();
        });
        //Cuando el usuario presione el botón para iniciar sesión se cambiará la escena.
        this.iniciarSesion.setOnMouseClicked(mouseEvent -> {
            super.changeSceneToLogin(this.cambiarClave);
        });
    }

    /**
     * Se encarga de realizar todos los procedimientos para cambiar la clave del usuario.
     */
    private void cambiarClaveSeguridad(){

        //Se almacena en variables temporales los datos ingresados por el usuario en un formato adecuado.
        String claveIngresada = this.campoClave.getText().trim();
        String claveConfirmada = this.campoClaveConfirmacion.getText().trim();

        //Primero se valida que ambos campos tengan el mismo contenido.
        if(!validarCampos(claveIngresada, claveConfirmada)){
            this.notificacionClaves.setVisible(true);
            this.notificacionClaves.setText("Las claves no tienen el mismo contenido");
        }else{
            //En caso de que el usuario ya haya fallado, entonces el campo se notificacionClaves se vuelve invisible.
            this.notificacionClaves.setVisible(false);

            //Ahora se valida que las claves cumplan con el estándar, como en este paso ambas
            //son idénticas, solo hace falta validar una de ellas.
            boolean estandarViolado = this.validarEstandarClave(claveIngresada, this.notificacionClaves);
            if(!(estandarViolado)){
                //Se valida que el ID del usuario sea válido
                if(this.IDUsuario > -1) {
                    //Si todos los datos son válidos, se actualiza la clave de seguridad.
                    boolean procesoCompletado = this.actualizarClaveBD(claveIngresada, this.IDUsuario);
                    if (procesoCompletado) {
                        super.informationNotification.setTitle("NOTIFICACION");
                        super.informationNotification.setHeaderText("CLAVE ACTUALIZADA");
                        super.informationNotification.setContentText("Su clave de seguridad ha sido actualizada " +
                                "satisfactoriamente" +
                                ".");
                        super.informationNotification.showAndWait();
                        //Finalmente se cambia a la escene de login.
                        super.changeSceneToLogin(this.cambiarClave);
                    } else {
                        super.errorNotification.setTitle("ERROR");
                        super.errorNotification.setContentText("Ha ocurrido un error a la hora de actualizar su clave. \n" +
                                "Inténtelo más tarde...");
                        super.errorNotification.showAndWait();
                        super.changeSceneToLogin(this.cambiarClave);
                    }
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("FALTAN DATOS");
                    super.errorNotification.setContentText("No se puede completar la acción");
                    super.errorNotification.showAndWait();
                    super.changeSceneToLogin(this.cambiarClave);
                }
            }
        }
    }

    /**
     * Método que se encarga de validar los dos campos de las claves.
     * Retornará true si ambos campos contienen los mismos datos.
     * @param clave: contenido del primer campo.
     * @param claveConfirmada: contenido del segundo campo, en este caso del campo de confirmación.
     * @return true si ambos campos contienen los mismos valores.
     */
    public boolean validarCampos(String clave, String claveConfirmada){
        return clave.equals(claveConfirmada);
    }

    /**
     * Método que valida que las claves cumplan con el estándar, como en este paso ambas
     * son idénticas, solo hace falta validar una de ellas.
     * @param clave: contenido de la clave de seguridad.
     * @param notificacionClave: label que se manipulará en la validación.
     * @return true si la clave no cumple con el estanar.
     */
    public boolean validarEstandarClave(String clave, Label notificacionClave){
        boolean estandarViolado = false;
        if(super.validarClaveSeguridadCenfotec(clave, notificacionClave)){
            estandarViolado = true;
        }
        return estandarViolado;
    }

    /**
     * Método que se encarga de actualizar la clave de seguridad del usuario.
     * @param claveSeguridad: nueva clave de seguridad.
     * @param IDUsuario: ID del usuario que está cambiando su clave.
     * @return true si la clave pudo ser actualizada.
     */
    public boolean actualizarClaveBD(String claveSeguridad, int IDUsuario){
        return this.controllerUsuario.actualizarClaveSeguridad(claveSeguridad, IDUsuario);
    }
}
