package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PanelDireccionesController extends Controller implements Initializable {

    @FXML
    private ImageView regresarBtn;

    @FXML
    private VBox paisesBtn;

    @FXML
    private VBox provinciasBtn;

    @FXML
    private VBox distritosBtn;

    @FXML
    private VBox cantonesBtn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.regresarBtn.setOnMouseClicked(mouseEvent -> {
            super.changeSceneToMenu(this.regresarBtn, true);
        });

        this.paisesBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarPaises();
        });

        this.provinciasBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarProvincias();
        });

        this.cantonesBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarCantones();
        });

        this.distritosBtn.setOnMouseClicked(mouseEvent -> {
            this.administrarDistritos();
        });
    }


    private void administrarPaises(){
        super.stage = (Stage) this.paisesBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarPaisesFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void administrarProvincias(){
        super.stage = (Stage) this.paisesBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarProvinciasFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void administrarCantones(){
        super.stage = (Stage) this.paisesBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarCantonesFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void administrarDistritos(){
        super.stage = (Stage) this.paisesBtn.getScene().getWindow();
        if(super.scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/GestionarDistritosFXML.fxml"));
            super.setDraggable(root);
            stage.setScene(new Scene(root));
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
