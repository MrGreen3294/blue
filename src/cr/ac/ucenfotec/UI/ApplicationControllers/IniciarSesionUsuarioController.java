package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXCheckBox;
import cr.ac.ucenfotec.TL.ControllerAdministrador;
import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class IniciarSesionUsuarioController extends Controller implements Initializable {

    @FXML
    private TextField campoNombreUsuario;

    @FXML
    private Label notificacionNombreUsuario;

    @FXML
    private PasswordField campoClave;

    @FXML
    private Label notificacionClave;

    @FXML
    private Label olvidoClave;

    @FXML
    private Button ingresarDatos;

    @FXML
    private Label registroCuenta;

    @FXML
    private JFXCheckBox recordarme;



    //Variables temporales para almacenar en memoria los datos escritos por el usuario.
    private String nombreUsuario;
    private String claveSeguridad;

    //Variable temporal que almacenará el ID del usuario que está iniciando sesión.
    private int IDUsuario;

    //Variable temporal para poder enviar información al túnel o controller del usuario.
    ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Variable temporal para poder enviar información al controller del usuario Administrador.
    ControllerAdministrador controllerAdministrador = new ControllerAdministrador();

    File file;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Cuando el usuario presione el botón para iniciar sesion se llama al método iniciarSesion();
        this.ingresarDatos.setOnAction(actionEvent -> {
            iniciarSesion();
            if(this.recordarme.isSelected()){
                RECORDARME();
            }else{
                OLVIDARME();
            }
        });
        //Cuando el usuario presione el botón registrar cuenta, se cambia a la escena Registrar Usuario FXML.
        this.registroCuenta.setOnMouseClicked(mouseEvent -> {
            changeSceneToSignUp();
        });
        //Cuando el usuario presione el botón para recuperar la contraseña, se llama el método
        // recuperarClaveSeguridad()
        this.olvidoClave.setOnMouseClicked(mouseEvent -> {
            recuperarClaveSeguridad();
        });

        //Se analiza su baúl
        String rutaArchivoLogin =
                ".\\src\\Recordarme\\";

        File directorioDestino = new File(rutaArchivoLogin);

        directorioDestino.mkdirs();

        file = new File(rutaArchivoLogin + "Datos.txt");

        ACTUALIZAR();

    }

    public void changeSceneToSignUp(){
        //Se cambia la escena al registro de un usuario.
        super.changeSceneToSignUp(ingresarDatos);
    }

    public void iniciarSesion(){

        nombreUsuario = this.campoNombreUsuario.getText().trim();
        claveSeguridad = this.campoClave.getText().trim();

        //Primero se valida que ambos datos sean válidos.
        boolean camposViolados = validarDatos();

        if(!(camposViolados)){
            //Se valida a nivel de la base de datos.
            boolean credencialesValidos = this.controllerUsuario.validarCredencialesUsuario(this.nombreUsuario,
                    this.claveSeguridad);

            if(credencialesValidos){
//                informationNotification.setTitle("CREDENCIALES VÁLIDOS");
//                informationNotification.setHeaderText("CREDENCIALES VÁLIDOS");
//                informationNotification.setContentText("HA INICIADO SESIÓN");
//                informationNotification.showAndWait();

                //Se consulta el ID del usuario que está iniciando sesión.
                this.IDUsuario = consultarIDUsuario(nombreUsuario, claveSeguridad);
                //Antes de cambiar la escena, se establece en el Controller padre el ID del usuario para que otros
                // controllers lo puedan manejar en el core del apalicación.
                Controller.setIDUsuario(this.IDUsuario);
                //Se selecciona el tipo de escena según el rol del usuario.
                inicializarMenu();
            }else{
                errorNotification.setTitle("DATOS INVÁLIDOS");
                errorNotification.setHeaderText("DATOS INVÁLIDOS");
                errorNotification.setContentText("LOS CREDENCIALES INGRESADOS SON INCORRECTOS");
                errorNotification.showAndWait();
            }
        }

    }

    public boolean validarDatos(){
        boolean camposViolados = false;
        if(validarCampoComunCenfotec(nombreUsuario, notificacionNombreUsuario)){
            camposViolados = true;
        }
        if(validarClaveSeguridadCenfotec(claveSeguridad, notificacionClave)){
            camposViolados = true;
        }
        return camposViolados;
    }

    public void recuperarClaveSeguridad(){

        super.stage = (Stage) this.ingresarDatos.getScene().getWindow();

        if(super.scene != null){
            super.stage.hide();
        }
        try {
            //Se carga el FXML
            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/SalvarClaveSeguridadFXML.fxml"));
            Parent root = fxml.load();
            super.setDraggable(root);

            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void inicializarMenu(){
        //Primero se consulta si el usuario es un administrador, ya que en este caso se le asigna una escena diferente.
        boolean usuarioAdmin = consultarRolAdmin(this.IDUsuario);
        //Si el usuario NO es un Administrador, se cargará un menú estándar.
        if(!usuarioAdmin)
            cargarMenuEstandar();
        else
            cargarMenuAdmin();
    }

    /**
     * Método que retorna si el usuario que está iniciando sesión es un Administrador.
     * @param IDUsuario: ID del usuario.
     * @return true si el usuario es Administrador.
     */
    public boolean consultarRolAdmin(int IDUsuario){
        return controllerAdministrador.validarUsuarioAdministrador(IDUsuario);
    }

    /**
     * Método que retorna el ID del usuario que está iniciando sesión.
     * @param nombreUsuario: Apodo del usuario que está iniciando sesión.
     * @param claveSeguridad: Clave del usuario que está iniciando sesión.
     * @return ID del usuario o -1 si el usuario no pudo ser encontrado.
     */
    private int consultarIDUsuario(String nombreUsuario, String claveSeguridad){
        return controllerUsuario.obtenerPKUsuario(nombreUsuario, claveSeguridad);
    }

    /**
     * Método que se encarga de cambiar la escena a un menú estandar.
     */
    public void cargarMenuEstandar(){

        super.stage = (Stage) this.ingresarDatos.getScene().getWindow();

        if(scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelUsuarioFXML.fxml"));
            super.setDraggable(root);
            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();

        }catch(Exception ignored){

        }
    }

    /**
     * Método que se encarga de cambiar la escena al menú de un administrador..
     */
    public void cargarMenuAdmin(){

        super.stage = (Stage) this.ingresarDatos.getScene().getWindow();

        if(scene != null){
            super.stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelUsuarioAdminFXML.fxml"));
            super.setDraggable(root);
            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();

        }catch(Exception ignored){

        }
    }

    /**
     * Guarda el nombre de un usuario y la clave de seguridad en un archivo.
     */
    public void RECORDARME(){ //Guarda el nombre de usuario y la clave de seguridad.
        try {
            if (!(file.exists())) { //Si el archivo no existe.
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
                bufferedWriter.write(this.nombreUsuario);
                bufferedWriter.newLine();
                bufferedWriter.write(this.claveSeguridad);
                bufferedWriter.close();
            }else{
                //Sobre escribe los datos.
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file.getAbsolutePath(), false));
                bufferedWriter.write(this.nombreUsuario);
                bufferedWriter.newLine();
                bufferedWriter.write(this.claveSeguridad);
                bufferedWriter.close();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Se encarga de eliminar el archivo en caso de que el usuario ya no quiera ser recordaro.
     */
    public void OLVIDARME(){
        try{
            if(file.exists() && !(this.recordarme.isSelected())){
                //Se intenta eliminar el archivo
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lee el archivo de los datos del usuario en caso de que haya solicitado que el sistema se los recorada.
     */
    public void ACTUALIZAR(){
        try{
            if(file.exists()){ //Si el archivo existe...
                this.recordarme.setSelected(true);
                Scanner scanner = new Scanner(file); //Se lee el archivo
                this.campoNombreUsuario.setText(scanner.nextLine());
                this.campoClave.setText(scanner.nextLine());
                scanner.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
