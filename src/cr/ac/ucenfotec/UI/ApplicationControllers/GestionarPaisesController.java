package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class GestionarPaisesController extends Controller implements Initializable {

    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombre;

    @FXML
    private Label notificacionCodigoPais;

    @FXML
    private TextField campoCodigo;

    @FXML
    private JFXButton agregarPaisBtn;

    @FXML
    private JFXButton eliminarPaisBtn;

    @FXML
    private JFXListView<String> listaPaises;

    @FXML
    private ImageView salirBtn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listarPaisesRegistrados();
        this.listaPaises.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        this.eliminarPaisBtn.setOnMouseClicked(mouseEvent -> {
            this.eliminarPaises(this.listaPaises.getSelectionModel().getSelectedItems());
        });

        this.agregarPaisBtn.setOnMouseClicked(mouseEvent -> {
            String nombrePais = this.campoNombre.getText().trim();
            String codigoPais = this.campoCodigo.getText().trim();

            boolean resultadoValidacion = validarDatosPais(nombrePais, codigoPais);

            if(!(resultadoValidacion)) {
                this.agregarPais(nombrePais, codigoPais);
                this.listarPaisesRegistrados();
            }
        });

        this.salirBtn.setOnMouseClicked(mouseEvent -> {
            super.stage = (Stage) this.salirBtn.getScene().getWindow();
            if(super.scene != null){
                super.stage.hide();
            }
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelDireccionesFXML.fxml"));
                super.setDraggable(root);
                stage.setScene(new Scene(root));
                stage.show();
            }catch(Exception e){
                e.printStackTrace();
            }
        });
    }

    public void listarPaisesRegistrados(){
        this.listaPaises.getItems().clear();
        String[] paisesRegistrados = controllerDireccion.listarPaises();
        for(String pais: paisesRegistrados){
            this.listaPaises.getItems().add(pais);
        }
    }

    public boolean validarDatosPais(String nombrePais, String codigoPais){
        boolean datosIncorrectos = false;
        if(validarCampoComunCenfotec(nombrePais, this.notificacionNombre)){
            datosIncorrectos = true;
        }
        if(validarCampoComunNumerosCenfotec(codigoPais, this.notificacionCodigoPais)){
            datosIncorrectos = true;
        }
        return datosIncorrectos;
    }

    public void agregarPais(String nombrePais, String codigoPais){
        super.controllerDireccion.agregarPais(nombrePais, codigoPais);
    }


    public void eliminarPaises(ObservableList<String> selectedItems){
        if(selectedItems.size() > 0){
            //Consult in the database.
            boolean successProcesing = true;
            boolean deletingResult;
            for(String e : selectedItems) {
                 deletingResult = super.controllerDireccion.eliminarPais(e);
                if(!(deletingResult)){
                    successProcesing = false;
                }
            }
            if(successProcesing){
                super.informationNotification.setTitle("Contenido Eliminado");
                super.informationNotification.setHeaderText("CONTENIDO ELIMINADO.");
                super.informationNotification.setContentText(" ");
                super.informationNotification.show();
                this.listarPaisesRegistrados();
            }else{
                super.errorNotification.setTitle("Fallo a la hora de eliminar");
                super.errorNotification.setHeaderText("HUBO UN FALLO EN EL PROCESO.");
                super.errorNotification.setContentText("Por favor, contacte al diseñador");
                super.errorNotification.show();
            }
        }
    }




}
