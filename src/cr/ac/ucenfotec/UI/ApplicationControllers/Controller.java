package cr.ac.ucenfotec.UI.ApplicationControllers;

import cr.ac.ucenfotec.TL.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {

    Matcher matcher;
    Pattern pattern;

    //User Types
    public static final int ADMIN = 0;
    public static final int FINAL_USER = 1;

    //REGEX (REGULAR EXPRESSIONS)
    public static final String SPECIAL_CHAR_PATTERN = "[!@#$%^&*(),.?\":{}|<>]";
    public static final String LOWERCASE_LETTER_PATTERN = "[a-z]";
    public static final String UPPERCASE_LETTER_PATTERN = "[A-Z]";
    public static final String NUMBER_PATTERN = "[0-9]";
    public static final String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\" +
            ".[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f" +
            "]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";


    //Alertas
    protected Alert errorNotification = new Alert(Alert.AlertType.ERROR);
    protected Alert informationNotification = new Alert(Alert.AlertType.INFORMATION);
    protected Alert confirmationNotification = new Alert(Alert.AlertType.CONFIRMATION);
    //Variable que almacena la llave PK del usuario que está utilizando esta vista.
    private static int IDUsuario = -1;

    //Variable que almacena el correo electrónico del usuario que está utilizando las vistas.
    private static String correoElectronicoUsuario;

    //Variable que almacena el nombre de usuario o apodo del usuario que está utilizando las vistas.
    private static String apodoUsuario;

    //Variable que almacena la clave de seguridad del usuario que está utilizando las vistas.
    private static String claveUsuario;

    //Variable que almacena el código genérico del usuario que está utilizando las vistas.
    private static String codigoGenericoUsuario;

    //Variables temporales que almacenan los datos necesarios para enviar al correo electrónico.
    protected static  String fromEmailAccount, emailPassword, toEmailAccount, genericCode, messageToSend;

    //Variable temporal que almacena el URL de un video.
    protected static String URLVideo;

    //Variable temporal que almacena el nombre de un video.
    protected static String nombreVideo;

    //Variable temporal que almacena el nombre de una lista.
    protected static String nombreLista;

    //Variable temporal que almacena el ID de una lista de reproducción.
    protected static int IDLista;

    //Instancia para transferir y recibir información del controller las categorías.
    protected ControllerCategoria controllerCategoria = new ControllerCategoria();

    //Instancia para transferir y recibir información del controller de los temas.
    protected ControllerTema controllerTema = new ControllerTema();

    //Instancia para transferir y recibir información del controller de usuarios.
    protected ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Instancia para transferir y recibir información del controller de los videos.
    protected ControllerVideo controllerVideo = new ControllerVideo();

    //Instancia para transferir y recibir información del controller de las listas de reproducción.
    protected ControllerListaReproduccion controllerListaReproduccion = new ControllerListaReproduccion();

    //Instancia para transferir y recibir información del controller de roles.
    protected ControllerRol controllerRol = new ControllerRol();

    //Instancia para transferir y recibir información del controller administrador.
    protected ControllerAdministrador controllerAdministrador = new ControllerAdministrador();

    //Instancia para transferir y recibir información del controller de direcciones.
    protected ControllerDireccion controllerDireccion = new ControllerDireccion();

    /**
     * Setter que establece el valor de la llave PK del usuario que está utilizando el controller del view.
     * @param IDUsuario llave primaria del usuario.
     */
    public static void setIDUsuario(int IDUsuario){
        Controller.IDUsuario = IDUsuario;
    }

    /**
     * Setter que establece el valor del correo electrónico del usuario que está utilizando los views.
     * @param correoElectronicoUsuario: correo electrónico del usuario.
     */
    public static void setCorreoElectronicoUsuario(String correoElectronicoUsuario) {
        Controller.correoElectronicoUsuario = correoElectronicoUsuario;
    }

    /**
     * Setter que establece el valor del código genérico del usuario que está utilizando los views.
     * @param codigoGenericoUsuario código genérico.
     */
    public static void setCodigoGenericoUsuario(String codigoGenericoUsuario) {
        Controller.codigoGenericoUsuario = codigoGenericoUsuario;
    }

    /**
     * Setter que establece el valor del apodo del usuario que está utilizando los views.
     * @param apodoUsuario apodo del usuario.
     */
    public static void setApodoUsuario(String apodoUsuario) {
        Controller.apodoUsuario = apodoUsuario;
    }

    /**
     * Setter que establece el valor de la clave del usuario que está utilizando los views.
     * @param claveUsuario clave del usuario.
     */
    public static void setClaveUsuario(String claveUsuario) {
        Controller.claveUsuario = claveUsuario;
    }

    /**
     * Setter que asgina el URL de un video para que todos los controllers puedan administrar el dato.
     * @param URLVideo URL del video.
     */
    public static void setURLVideo(String URLVideo) {
        Controller.URLVideo = URLVideo;
    }

    /**
     * Setter que asigna el nombre de un video para que todos los controller puedan administrar el dato.
     * @param nombreVideo
     */
    public static void setNombreVideo(String nombreVideo) {
        Controller.nombreVideo = nombreVideo;
    }

    /**
     * Setter que asigna el ID de una lista de reproducción para que todos los controller puedan administrar el dato.
     * @param IDLista
     */

    public static void setIDLista(int IDLista){Controller.IDLista = IDLista;}
    /**
     * Getter que retorna el valor de la llave PK del usuario que está utilizando el controller del view
     * @return primaryKey llave primaria del usuario.
     */
    public static int getIDUsuario() {
        return Controller.IDUsuario;
    }

    /**
     * Getter que retorna el valor del correo electrónico del usuario que está utilizando los views.
     * @return correo electrónico.
     */
    public static String getCorreoElectronicoUsuario() {
        return correoElectronicoUsuario;
    }

    /**
     * Getter que retorna el valor del código genérico del usuario que está utilizando los views.
     * @return código genérico.
     */
    public static String getCodigoGenericoUsuario() {
        return codigoGenericoUsuario;
    }

    /**
     * Getter que retorna el valor del apodo del usuario que está utilizando los views.
     * @return apodo del usuario.
     */
    public static String getApodoUsuario() {
        return apodoUsuario;
    }

    /**
     * Getter que retorna el valor de la clave del usuario que está utilizando los views.
     * @return clave del usuario.
     */
    public static String getClaveUsuario() {
        return claveUsuario;
    }

    /**
     * Getter que retorna el valor del URL de un video para que todos los controllers puedan manipular el dato.
     * @return URL del video.
     */
    public static String getURLVideo() {
        return URLVideo;
    }

    /**
     * Getter que retorna el valor del nombre de un video para que todos los controller puedan manipular el dato.
     * @return nombre del video.
     */
    public static String getNombreVideo() {
        return nombreVideo;
    }

    /**
     * Getter que retorna el valor del ID de una lista de reproducción.
     */
    public static int getIDLista() {
        return IDLista;
    }

    public static String getNombreLista() {
        return nombreLista;
    }

    public static void setNombreLista(String nombreLista) {
        Controller.nombreLista = nombreLista;
    }

    Stage stage = new Stage();
    Scene scene;


    double  xOffset = .0;
    double  yOffset = .0;

    //Default Constructor
    public Controller(){

    }


    /**
     * Cambia la escena actual a la escena de registro.
     * @param applicationNode: Cualquier nodo contenido en el controller de la escena actual.  Esto
     * se utiliza para obtener los datos del stage actual y así poder manipular sus valores.
     */
    public void changeSceneToSignUp(Node applicationNode){
        stage = (Stage) applicationNode.getScene().getWindow();

        if(this.scene != null){
            stage.hide();
        }

        try{
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/RegistrarUsuarioFXML.fxml"));
            setDraggable(root);
            scene = new Scene(root);
            stage.centerOnScreen();
            stage.setScene(scene);
            stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Cambia la escena a un menú de usuario final.
     * @param applicationNode: Nodo de JAVAFX, se utiliza para obtener la información del stage actual.
     * @param adminMenu: true si el menú que se quiere ejecutar es el de un administrador y no el de un usuario final.
     */
    public void changeSceneToMenu(Node applicationNode, boolean... adminMenu){
        stage = (Stage) applicationNode.getScene().getWindow();

        if (this.scene != null) {
            this.stage.hide();
        }

        try {
            Parent root;
            if(adminMenu != null && adminMenu.length > 0) {
                if (adminMenu[0]) {
                    root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelUsuarioAdminFXML.fxml"));
                } else {
                    root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelUsuarioFXML.fxml"));
                }
            }else{
                root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelUsuarioFXML.fxml"));
            }
            this.setDraggable(root);
            this.scene = new Scene(root);
            this.stage.setScene(scene);
            this.stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cambia la escena actual a la escena de inicio de sesión.
     * @param applicationNode: Cualquier nodo contenido en el controller. Esto
     * se utiliza para obtener los datos del stage actual y así poder manipular sus valores.
     */
    public void changeSceneToLogin(Node applicationNode){
        stage = (Stage) applicationNode.getScene().getWindow();

        if(scene != null){
            stage.hide();
        }
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/IniciarSesionUsuarioFXML.fxml"));
            setDraggable(root);
            scene = new Scene(root);
            stage.centerOnScreen();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Valida que el valor proporcionado no esté vacío.
     * @param textContent: Texto que se va a validar.
     * @return true si el texto no es un número.
     */
    public boolean validateEmpty(String textContent){
        boolean errorFound = false;

        if(textContent == null){
            errorFound = true;
        }
        //Validate if the required field is empty.
        else if(textContent.isEmpty()){
            errorFound = true;
        }
        //Validate if the required field is blank.
        else if(textContent.isBlank()){
            errorFound = true;
        }
        else{
            errorFound = false;
        }
        return errorFound;
    }

    /**
     * Valida que el valor proporcionado sea un número.
     * @param numberContent: String con el número que se va a validar.
     * @return true si el valor ingresado puede ser convertido a un número.
     */
    public boolean validateIsNumber(String numberContent){
        boolean errorExpected= true;
        try{
            Double.parseDouble(numberContent);
            errorExpected = false;
        }catch(NumberFormatException ignored){
            errorExpected = true;
        }
        return errorExpected;
    }

    /**
     * Valida si el contenido proporcionado es no contiene números.
     * @param textContent: Contenido textual que se va a validar.
     * @return true si el contenido textual es un número.
     */
    public boolean validateIsOnlyString(String textContent){
        boolean errorExpected = true;
        for(int index = 0; index < textContent.length(); index++){
            try{
                Double.parseDouble(String.valueOf(textContent.charAt(index)));
                errorExpected = true;
                return errorExpected;
            }catch(NumberFormatException formatException){
                errorExpected = false;
            }
        }
        return errorExpected;
    }

    /**
     * Intenta transformar un valor textual en un número de punto flotante.
     * @param numberContent: Contenido del número.
     * @return Contenido numérico de tipo Double.
     */
    public Double convertToDouble(String numberContent){
        //First Validate if there is a problem with the input information
        if(validateEmpty(numberContent)){
            return -1.;
        }
        //Second Validate if there is a problem with the data type
        else if(validateIsNumber(numberContent)){
            return -1.;
        }
        //If all is okay, return the real number
        else{
           try {
               Double.parseDouble(numberContent);
           }catch(NumberFormatException numberFormatE){
               return -1.;
           }
        }
        return -1.;
    }

    public boolean validateMinChars(String dataContent, int charsAmount){
        boolean errorExpected = true;
        if(!(dataContent.length() < charsAmount)){
            errorExpected = false;
        }
        return errorExpected;
    }

    public boolean validateMaxChars(String dataContent, int charsAmount){
        boolean errorExpected = true;
        if(!(dataContent.length() > charsAmount)){
            errorExpected = false;
        }
        return errorExpected;
    }

    public boolean validateUppercaseChar(String dataContent){
        pattern = Pattern.compile(UPPERCASE_LETTER_PATTERN);
        boolean errorExpected = true;
        for(int index = 0; index < dataContent.length(); index++){
            matcher = pattern.matcher(String.valueOf(dataContent.charAt(index)));
            if(matcher.matches()){
                errorExpected = false;
                return errorExpected;
            }
        }
        return errorExpected;
    }

    public boolean validateLowercaseChar(String dataContent){
        pattern = Pattern.compile(LOWERCASE_LETTER_PATTERN);
        boolean errorExpected = true;
        for(int index = 0; index < dataContent.length(); index++){
            matcher = pattern.matcher(String.valueOf(dataContent.charAt(index)));
            if(matcher.matches()){
                errorExpected = false;
                return errorExpected;
            }
        }
        return errorExpected;
    }

    public boolean validateSpecialChar(String dataContent){
        pattern = Pattern.compile(SPECIAL_CHAR_PATTERN);
        boolean errorExpected = true;
        for(int index = 0; index < dataContent.length(); index++){
            matcher = pattern.matcher(String.valueOf(dataContent.charAt(index)));
            if(matcher.matches()){
                errorExpected = false;
            }
        }
        return errorExpected;
    }

    public boolean validateHaveNumbersQuantity(String dataContent, int numbersQuantity){
        pattern = Pattern.compile(NUMBER_PATTERN);
        int numbersCounter = 0;
        boolean errorExpected = true;
        for(int index = 0; index < dataContent.length(); index++){
            matcher = pattern.matcher(String.valueOf(dataContent.charAt(index)));
            if(matcher.matches()){
                numbersCounter++;
            }
        }
        if(numbersCounter >= numbersQuantity){
            errorExpected = false;
        }
        return errorExpected;
    }

    public boolean validateEmailFormat(String dataContent){
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(dataContent);
        boolean errorExpected = true;
        if(matcher.matches()){
            errorExpected = false;
        }
        return errorExpected;
    }

    public static boolean copyFileUsingStream(File source, File destination) throws IOException{
        boolean errorExpected = true;
        InputStream inputStream = new FileInputStream(source);
        OutputStream outputStream = new FileOutputStream(destination);
        try{
            byte[] buffer = new byte[1024];
            int length;
            while((length = inputStream.read(buffer)) > 0){
                outputStream.write(buffer, 0, length);
            }
            errorExpected = false;
        }catch(Exception ignored){

        }
        finally{
            inputStream.close();
            outputStream.close();
        }
        return errorExpected;
    }


    /**
     * Valida todos los campos comunes siguiendo las siguientes reglas de negocio.
     * 1. El campo común no puede estar vacío.
     * 2. El campo común no puede ser un número.
     * 3. El campo común no puede contener ningún número.
     * @param dataContent campo común que se va a validar.
     * @param label label del campo común donde se mostrarán los errores en caso de que hayan.
     * @return false si el campo común es válido.
     */
    public boolean validarCampoComunCenfotec(String dataContent, Label label){
        boolean violatedField = false;
        if(label != null) {
            label.setVisible(true);
        }
        //Validate Name
        if(validateEmpty(dataContent)){
            //System.err.println("El dato no puede estar vacío.");
            if(label != null) {
                label.setText("El dato no puede estar vacío.");
            }
            violatedField = true;
        }
        //Validate If The Field is a Number
        //If the field is a number there is an error
        else if(!validateIsNumber(dataContent)){
            //System.err.println("El dato no puede ser un número.");
            if(label != null) {
                label.setText("El dato no puede ser un número.");
            }
            violatedField = true;
        }
        //Validate If The Field Have only chars
        else if(validateIsOnlyString(dataContent)){
            if(label != null) {
                label.setText("El dato no debe contener numeros.");
            }
            violatedField = true;
        }else{
            if(label != null) {
                label.setVisible(false);
            }
        }
        //If everything is okay
        return violatedField;
    }

    /**
     * Valida que un campo común tenga las siguientes reglas de negocio.
     * 1. El campo no puede estar vacío.
     * @param dataContent campo común o numérico que se va a validar.
     * @param label: label del campo común que se va a utilizar para mostrar notificaciones en caso de que hayan
     * errores.
     * @return false si el campo es válido.
     */
    public boolean validarCampoComunNumerosCenfotec(String dataContent, Label label){
        boolean violatedField = false;
        if(label != null){
            label.setVisible(true);
        }
        if(dataContent.isEmpty() || dataContent.isBlank()){
            violatedField = true;
            if(label != null){
                label.setText("El dato no puede estar vacío.");
            }
        }else{
            if(label != null){
                label.setVisible(false);
            }
        }
        return violatedField;
    }

    /**
     * Valida una contraseña siguiendo las siguientes reglas de negocio:
     * 1. La contraseña no puede estar vacía.
     * 2. La contraseña debe tener al menos un total de 6 caracteres.
     * 3. La contraseña debe tener un máximo de 8 caracteres.
     * 4. La contraseña debe tener al menos una letra mayúscula.
     * 5. La contraseña debe tener al menos una letra minúscula.
     * 6. La contraseña debe tener al menos un caracter especial.
     * 7. La contraseña debe tener al menos un número.
     * @param dataContent clave que se va a validar.
     * @param label label de la contraseña donde se mostrarán los errores en caso de que hayan.
     * @return false si la contrseña es válida.
     */
    public boolean validarClaveSeguridadCenfotec(String dataContent, Label label){
        //Siempre que haya un label, este se ajustará como un campo visible para que cualquier notificación puede ser
        // mostrada.
        label.setVisible(true);
        boolean violatedField = false;
        if(validateEmpty(dataContent)){
            //System.err.println("El dato no puede estar vacío.");
            label.setText("El dato no puede estar vacío.");
            violatedField = true;
        }
        //Validate If The Field Have at least 6 characters
        else if(validateMinChars(dataContent, 6)){
            //System.err.println("El dato debe contener al menos 6 caracteres.");
            label.setText("El dato debe contener al menos 6 caracteres.");
            violatedField = true;
        }
        //Validate If The Field Have a Max of 8 characters
        else if(validateMaxChars(dataContent, 8)){
            //System.err.println("El dato no puede contener más de 8 caracteres.");
            label.setText("El dato no puede contener más de 8 caracteres.");
            violatedField = true;
        }
        //Validate If The Field Have at Least One Uppercase Value
        else if(validateUppercaseChar(dataContent)){
            //System.err.println("El dato debe contener al menos una letra mayúscula.");
            label.setText("El dato debe contener al menos una letra mayúscula.");
            violatedField = true;
        }
        //Validate If The Field Have at Least One Lowercase Value
        else if(validateLowercaseChar(dataContent)){
            //System.err.println("El dato debe contener al menos una letra minúscula.");
            label.setText("El dato debe contener al menos una letra minúscula.");
            violatedField = true;
        }
        //Validate If the Field Have at Least One Special Character
        else if(validateSpecialChar(dataContent)){
            //System.err.println("El dato debe contener al menos un carácter especial.");
            label.setText("El dato debe contener al menos un carácter especial.");
            violatedField = true;
        }
        //Validate If the Field Have at Least One Number
        else if(validateHaveNumbersQuantity(dataContent, 1)){
            //System.err.println("El dato debe contener al menos un número.");
            label.setText("El dato debe contener al menos un número.");
            violatedField = true;
        }else{
            //Luego de que las validaciones resulten bien, el label será invisible.
            label.setVisible(false);
        }
        //If everything is okay then
        return violatedField;
    }

    /**
     * Valida un correo electrónico siguiendo las siguientes reglas de negocio:
     * 1. El correo electrónico no puede estar vacío.
     * 2. El correo electrónico debe seguir el estándar oficial REGEX RFC 5322.
     * @param dataContent correo electrónico a validar.
     * @param label label del correo electrónico donde se mostrarán los errores en caso de que hayan.
     * @return false si el correo electrónico es válido.
     */
    public boolean validarCorreoCenfotec(String dataContent, Label label){
        boolean violatedField = false;
        if(label != null)
            label.setVisible(true);
        if(validateEmpty(dataContent)){
            //System.err.println("El correo electrónico no puede estar vacío")
            label.setText("El correo electrónico no puede estar vacío.");
            violatedField = true;
        }
        else if(validateEmailFormat(dataContent)){
            //System.err.println("El correo electrónico no tiene un formato válido");
            label.setText("El correo electrónico es inválido.");
            violatedField = true;
        }else{
            if(label != null)
                label.setVisible(false);
        }
        //If everything is okay then
        return violatedField;
    }

    /**
     * Valida una identificación siguiendo las siguientes reglas de negocio:
     * 1. La identificación no puede estár vacía.
     * @param identificacion identificación a validar.
     * @param etiqueta label de la identificación donde se mostrarán los errores en caso de que hayan.
     * @return false si la identificación es válida.
     */
    public boolean validarIdentificacionCenfotec(String identificacion, Label etiqueta){
        boolean campoViolado = false;
        if(etiqueta != null)
            etiqueta.setVisible(true);
        if(validateEmpty(identificacion)){
            if(etiqueta != null)
                etiqueta.setText("La identificación no puede estar vacía.");
                campoViolado = true;
        }
        else{
           if(etiqueta != null)
               etiqueta.setVisible(false);
        }
        return campoViolado;
    }

    /**
     * Valida una fecha de nacimiento siguiendo las siguientes reglas de negocio:
     * 1. La fecha de nacimiento introducida no puede estar vacía.
     * 2. La fecha de nacimiento no puede ser menor o igual a 0 convirtiendola en años.
     * 3. La fecha de nacimiento debe dar como resultado la edad de un mayor de edad.
     * @param fechaNacimiento fecha de nacimiento a validar.
     * @param etiqueta label de la fecha de nacimiento donde se mostrarán los errores en caso de que existan.
     * @return false si la fecha de nacimiento es válida.
     */
    public boolean validarFechaNacimientoCenfotec(LocalDate fechaNacimiento, Label etiqueta){
        boolean campoViolado = false;
        if(etiqueta != null)
            etiqueta.setVisible(true);

        //Valida que la fecha de nacimiento no esté vacía.
        if(fechaNacimiento != null) {
            if (validateEmpty(fechaNacimiento.toString())) {
                if (etiqueta != null)
                    etiqueta.setText("La fecha de nacimiento no puede estar vacía.");
                campoViolado = true;
            }

            LocalDate fechaActual = LocalDate.now();
            Period period = Period.between(fechaNacimiento, fechaActual);
            //Valida que los años no tengan un valor erroneo.
            if (period.getYears() <= 0) {
                if (etiqueta != null)
                    etiqueta.setText("La fecha de nacimiento no puede ser menor o igual a cero.");
                campoViolado = true;
            }

            //Valida que los años sean de un mayor de edad.
            if (period.getYears() < 18) {
                if (etiqueta != null)
                    etiqueta.setText("No se permiten menores de edad.");
                campoViolado = true;
            }

            else{
                if(etiqueta != null)
                    etiqueta.setVisible(false);
            }
        }
        return campoViolado;
    }

    protected void setDraggable(Parent root){
        //Lambda Expressions.
        root.setOnMousePressed(mouseEvent -> {
            xOffset = mouseEvent.getSceneX();
            yOffset = mouseEvent.getSceneY();
        });

        //When the root is dragged set the position to the new x and y position of the mouse.
        root.setOnMouseDragged(mouseEvent -> {
            assert stage != null;
            stage.setX(mouseEvent.getScreenX() - xOffset);
            stage.setY(mouseEvent.getScreenY() - yOffset);
        });
    }
}
