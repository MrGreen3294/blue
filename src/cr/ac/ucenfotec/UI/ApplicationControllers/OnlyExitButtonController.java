package cr.ac.ucenfotec.UI.ApplicationControllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class OnlyExitButtonController implements Initializable {

    @FXML
    ImageView exit;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.exit.setOnMouseClicked(mouseEvent -> {
            System.exit(0);
            Platform.exit();
        });
    }
}
