package cr.ac.ucenfotec.UI.ApplicationControllers;


import cr.ac.ucenfotec.TL.ControllerUsuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class SalvarClaveSeguridadController extends Controller implements Initializable {

    @FXML
    private TextField campoCorreo;

    @FXML
    private Button salvarCuenta;

    @FXML
    private Label notificacionCorreo;

    @FXML
    private Button cancelarBtn;


    //Variable temporal que almacena el correo electrónico
    String correoElectronico;

    //Variable temporal que almacena el ID del usuario que está intentando salvar su cuenta.
    int IDUsuario;

    //Variable temporal que será utilizada para validar enviar y recibir información del Controller del usuario.
    ControllerUsuario controllerUsuario = new ControllerUsuario();

    //Variable temporal que almacena localmente el código genérico del usuario que se está registrando.
    private String codigoGenerico = "";

    //Variable temporal para almacenar el mensaje definido.
    private String mensaje = "";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        if(Controller.fromEmailAccount == null || Controller.toEmailAccount == null){
            //Inicializa las variables temporales para el envío del correo electrónico.
            Controller.fromEmailAccount = "blueartsapplication@gmail.com";
            Controller.emailPassword = "Qs7Cq85CQfDq";
        }

        //En caso de que el usuario presione el botón para salvar la cuenta se llamará al método salvarCuenta()
        this.salvarCuenta.setOnAction(actionEvent -> {
            salvarCuenta();
        });

        //En caso de que el usuario presione el botón para cancelar la cuenta se llamará al método changeToLogin()
        this.cancelarBtn.setOnAction(actionEvent -> {
            changeSceneToLogin(this.cancelarBtn);
        });

    }

    public void salvarCuenta(){
        //Se transforman los datos en variables temporales
        this.correoElectronico = this.campoCorreo.getText().trim().toLowerCase();

        //Primero se validan los datos
        boolean datosViolados = validarDatos();

        if(!datosViolados){
            //Se valida que exista un usuario ACTIVO con el correo electrónico proporcionado.
            boolean usuarioExiste = this.validarExistenciaUsuarioCorreo();
            if(!(usuarioExiste)){
                super.errorNotification.setTitle("ERROR");
                super.errorNotification.setHeaderText("NO EXISTE");
                super.errorNotification.setContentText("No existe un usuario con el correo " + this.correoElectronico + " en el sistema.");
                super.errorNotification.showAndWait();
            }else{
                //Se consulta el ID del usuario que tiene el correo electrónico.
                this.IDUsuario = obtenerPrimaryKeyUsuario(this.correoElectronico);
                if(IDUsuario > -1){
                    //Se genera y se actualiza el código alfanumérico.
                    controllerUsuario.actualizarCodigoConfirmacion(this.IDUsuario);
                    //Luego se obtiene para establecerlo en una variable temporal.
                    this.codigoGenerico = controllerUsuario.consultarCodigoConfirmacion(this.IDUsuario);
                    //Ahora se define el mensaje para prepararlo en HTML5.
                    this.mensaje = this.definirMensaje();
                    //Finalmente se envía el correo.
                    try {
                        this.enviarMail();
                        super.informationNotification.setTitle("NOTIFICACION");
                        super.informationNotification.setHeaderText("CORREO ENVIADO");
                        super.informationNotification.setContentText("Se ha enviado un código genérico a su correo de" +
                                " restauración.");
                        super.informationNotification.showAndWait();
                    } catch (Exception e) {
                        super.errorNotification.setTitle("ERROR AL ENVIAR EL MENSAJE");
                        super.errorNotification.setHeaderText("EL MENSAJE DE RECUPERACIÓN NO PUDO SER ENVIADO");
                        super.errorNotification.setContentText("Es posible que no se haya podido establecer, " +
                                "una conexión a internet.\n Inténtelo más tarde...");
                        super.errorNotification.showAndWait();
                        super.changeSceneToLogin(this.salvarCuenta);
                    }
                    //Se cambia a la escena para confirmar el código enviado.
                    this.confirmarCodigoClave();
                }else{
                    super.errorNotification.setTitle("ERROR");
                    super.errorNotification.setHeaderText("DATOS ENCONTRADOS");
                    super.errorNotification.setContentText("Ha ocurrido un error desconocido, inténtelo más tarde.");
                    super.errorNotification.showAndWait();
                    super.changeSceneToLogin(this.salvarCuenta);
                }
            }
        }
    }

    /**
     * Se encarga de validar que los datos ingresados tengan un formato válido.
     * @return false si los datos son válidos.
     */
    public boolean validarDatos(){
        boolean camposViolados = false;
        if(super.validarCorreoCenfotec(this.correoElectronico, this.notificacionCorreo)){
            camposViolados = true;
        }
        return camposViolados;
    }


    /**
     * Se encarga de validar que un usuario exista con el correo electrónico escrito.
     * El usuario debe estár activo, si su cuenta está inactivo el resultado será false.
     * @return true si el usuario existe.
     */
    public boolean validarExistenciaUsuarioCorreo(){
        return controllerUsuario.validarExistenciaCorreo(this.correoElectronico);
    }

    /**
     * Se encarga de obtener el ID del usuario mediante la realización de una consulta con
     * el correo electrónico.
     * @param correoElectronico: Correo electrónico del usuario.
     * @return ID del usuario consultado o -1 si el usuario no existe.
     */
    public int obtenerPrimaryKeyUsuario(String correoElectronico){
        return controllerUsuario.obtenerPKUsuarioCorreo(this.correoElectronico);
    }

    /**
     * Se encarga de enviar un correo electrónico con el mensaje generado.
     * @throws MessagingException excepción que es lanzada en caso de que haya ocurrido un error.
     */
    private void enviarMail() throws MessagingException {
        //Properties es una estructura Key: Value
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        //Se crea una nueva sesión con los datos.
        Session session = Session.getInstance(properties, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(Controller.fromEmailAccount, Controller.emailPassword);
            }
        });

        Message message = generarMensaje(session, Controller.fromEmailAccount, this.correoElectronico,
                this.mensaje);
        Transport.send(message);

    }

    private Message generarMensaje(Session session, String fromEmailAccount, String recipientEmailAccount,
                                   String messageToSend) {
        Message tempMessage = new MimeMessage(session);
        try {
            tempMessage.setFrom(new InternetAddress(fromEmailAccount));
            tempMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmailAccount));
            tempMessage.setSubject("RECUPERA TU CUENTA - BLUE Arts");
            tempMessage.setContent(messageToSend, "text/html; charset=UTF-8");
            return tempMessage;
        } catch (Exception ignored) {

        }
        return null;
    }

    private String definirMensaje(){
        StringBuilder builder = new StringBuilder();
        builder.append("<h1>Recuperación de la cuenta BLUE Arts</h1>").append("\n");
        builder.append("<p>").append("Hemos detectado que usted intenta recuperar su contraseña.").append("</p>");
        builder.append("<p>").append("Para que estemos seguros que la intención es suya, le hemos enviado este correo" +
                " de confirmación.").append("</p>");
        builder.append("<p>").append("Por favor, ingrese el siguiente código genérico en su aplicación BLUE").append(
                "</p>");
        builder.append("<h2>Código Generico</h2>");
        builder.append("<h1>").append(this.codigoGenerico).append("</h1>").append("\n");
        builder.append("<p>").append("Gracias por preferirnos, nuestro Software ha sido diseñado con cariño y " +
                "así serán tratados nuestros clientes. Atentamente, equipo <strong>BLUE Arts</strong>").append("</p>");
        return builder.toString();
    }

    public void confirmarCodigoClave(){
        super.stage = (Stage) this.salvarCuenta.getScene().getWindow();

        if(super.scene != null){
            super.stage.hide();
        }

        try{

            //Actualizar información del controller.
            Controller.setIDUsuario(this.IDUsuario);

            FXMLLoader fxml = new FXMLLoader(getClass().getResource("../ApplicationViews/ConfirmarCodigoClaveFXML" +
                    ".fxml"));
            Parent root = fxml.load();
            super.setDraggable(root);


            super.scene = new Scene(root);
            super.stage.setScene(scene);
            super.stage.show();
        }catch(Exception e){
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }
}
