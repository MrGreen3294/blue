package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXListView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class GestionarProvinciasController extends Controller implements Initializable {

    @FXML
    private JFXComboBox<String> paisesComboBox;

    @FXML
    private JFXComboBox<String> paisProvinciaComboBox;

    @FXML
    private Label notificacionNombre;

    @FXML
    private Label notificacionPais;

    @FXML
    private TextField campoNombre;

    @FXML
    private Label notificacionCodigo;

    @FXML
    private TextField campoCodigo;

    @FXML
    private JFXButton eliminarProvinciaBtn;

    @FXML
    private JFXButton listarDatos;

    @FXML
    private JFXButton agregarProvinciaBtn;

    @FXML
    private JFXListView<String> listaProvincias;

    @FXML
    private ImageView salirBtn;

    int IDPais;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //Seleccionar múltiples elementos.
        this.listaProvincias.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //Listar paises de la derecha.
        this.paisesComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisesComboBox.getItems().clear();
            this.paisesComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
        });

        //Listar paises de la izquierda.
        this.paisProvinciaComboBox.setOnMouseClicked(mouseEvent -> {
            this.paisProvinciaComboBox.getItems().clear();
            this.paisProvinciaComboBox.getItems().addAll(super.controllerDireccion.listarPaises());
        });

        this.salirBtn.setOnMouseClicked(mouseEvent -> {
            super.stage = (Stage) this.salirBtn.getScene().getWindow();
            if (super.scene != null) {
                super.stage.hide();
            }
            try {
                Parent root = FXMLLoader.load(getClass().getResource("../ApplicationViews/PanelDireccionesFXML.fxml"));
                super.setDraggable(root);
                stage.setScene(new Scene(root));
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        this.agregarProvinciaBtn.setOnMouseClicked(mouseEvent -> {
            String nombreProvincia = this.campoNombre.getText().trim();
            String codigoProvincia = this.campoCodigo.getText().trim();
            String nombrePais = this.paisesComboBox.getSelectionModel().getSelectedItem();

            boolean resultadoValidacion = this.validarDatos(nombreProvincia, nombrePais, codigoProvincia);
            if (!(resultadoValidacion)) {
                registrarProvincia(nombreProvincia, codigoProvincia, nombrePais);
            }
        });

        this.eliminarProvinciaBtn.setOnAction(actionEvent -> {
            eliminarProvincia();
        });

        this.listarDatos.setOnAction(actionEvent -> {
            String paisSeleccionado = this.paisProvinciaComboBox.getSelectionModel().getSelectedItem();
            if(!(paisSeleccionado == null || paisSeleccionado.isEmpty())) {
                listarProvincias();
            }else{
                super.errorNotification.setHeaderText("Debe seleccionar un país");
                super.errorNotification.setContentText("Indique el país");
                super.errorNotification.setTitle("Error");
                super.errorNotification.showAndWait();
            }
        });

    }

    private boolean validarDatos(String nombreProvincia, String nombrePais, String codigoProvincia) {
        boolean datosInvalidos = false;

        if (super.validarCampoComunCenfotec(nombreProvincia, notificacionNombre)) {
            datosInvalidos = true;
        }
        if (super.validarCampoComunCenfotec(nombrePais, notificacionPais)) {
            datosInvalidos = true;
        }
        if (super.validateIsNumber(codigoProvincia)) {
            this.notificacionCodigo.setVisible(true);
            this.notificacionCodigo.setText("El dato debe ser un número");
            datosInvalidos = true;
        }else{
            this.notificacionCodigo.setVisible(false);
        }

        return datosInvalidos;
    }

    public void registrarProvincia(String nombreProvincia, String codigoProvincia, String nombrePais) {
        this.IDPais = super.controllerDireccion.solicitarIDPais(nombrePais);
        super.controllerDireccion.agregarProvincia(nombreProvincia, codigoProvincia, IDPais);
        listarProvincias();

    }

    public void eliminarProvincia(){
        try {
            String nombrePais = this.paisProvinciaComboBox.getSelectionModel().getSelectedItem().trim();
            String nombreProvincia = this.listaProvincias.getSelectionModel().getSelectedItem();
            if(!(nombrePais.isEmpty())){
                if(!(nombreProvincia.isEmpty())){
                    super.controllerDireccion.eliminarProvincia(nombrePais, nombreProvincia);
                    listarProvincias();
                }
            }
        }catch(NullPointerException ignored){
        }
    }

    public void listarProvincias() {
        this.IDPais = super.controllerDireccion.solicitarIDPais(this.paisProvinciaComboBox.getSelectionModel().getSelectedItem());
            this.listaProvincias.getItems().clear();
            String[] provinciasRegistradas = super.controllerDireccion.listarProvincias(this.IDPais);
        for (String provinciasRegistrada : provinciasRegistradas) {
            this.listaProvincias.getItems().add(provinciasRegistrada);
        }
    }
}
