package cr.ac.ucenfotec.UI.ApplicationControllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import cr.ac.ucenfotec.BL.TemaLista.TemaLista;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class GestionarTemasController extends Controller implements Initializable {
    @FXML
    private Label notificacionNombre;

    @FXML
    private TextField campoNombre;

    @FXML
    private JFXButton agregarTemaBtn;

    @FXML
    private JFXButton eliminarTemaBtn;

    @FXML
    private Label notificacionDescripcion;

    @FXML
    private TextField campoDescripcion;

    @FXML
    private JFXListView<String> listaTemas;

    @FXML
    private ImageView irMenu;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listarTemasRegistrados();

        this.agregarTemaBtn.setOnAction(actionEvent -> {
            this.agregarTema();
        });

        this.eliminarTemaBtn.setOnAction(actionEvent -> {
            this.eliminarTema();
        });

        this.irMenu.setOnMouseClicked(mouseEvent -> {
            super.changeSceneToMenu(this.irMenu, true);
        });


    }

    public void listarTemasRegistrados(){
        this.listaTemas.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.listaTemas.getItems().clear();

        ArrayList<String> temas = controllerTema.obtenerTemas();

        this.listaTemas.getItems().addAll(temas);
    }

    private String nombre;
    private String descripcion;

    public void agregarTema(){
        nombre = this.campoNombre.getText().trim();
        descripcion = this.campoDescripcion.getText();

        boolean datosIncorrectos = validarDatosTema();
        if(!datosIncorrectos){
            if(controllerTema.agregarTema(nombre, descripcion)){
                super.informationNotification.setTitle("Tema Registrado");
                super.informationNotification.setContentText("El tema ha sido registrado.");
                super.informationNotification.setHeaderText("Tema " + nombre + " Registrado.");
                super.informationNotification.showAndWait();

                this.listarTemasRegistrados();
            }
        }
    }

    public void eliminarTema(){
        boolean error = false;
        if(this.listaTemas.getSelectionModel().getSelectedItems().size() > 0){
            for(String nombreTema: this.listaTemas.getSelectionModel().getSelectedItems()){
                boolean temaEliminado = controllerTema.eliminarTema(nombreTema);
                if(!(temaEliminado)){
                    error = true;
                }
            }
            if(!(error)){
                super.informationNotification.setTitle("Tema(s) Eliminado(s)");
                super.informationNotification.setHeaderText("El contenido ha sido eliminado.");
                super.informationNotification.showAndWait();

                this.listarTemasRegistrados();
            }
        }
    }

    public boolean validarDatosTema(){
        boolean campoIncorrecto = false;

        if(super.validarCampoComunCenfotec(nombre, this.notificacionNombre)){
            campoIncorrecto = true;
        }
        if(super.validarCampoComunCenfotec(descripcion, this.notificacionDescripcion)){
            campoIncorrecto = true;
        }
        else{
            this.notificacionDescripcion.setVisible(false);
            this.notificacionNombre.setVisible(false);
        }

        return campoIncorrecto;
    }
}
