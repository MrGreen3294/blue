package cr.ac.ucenfotec.BL.CategoriaVideo;

import cr.ac.ucenfotec.Connector;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interface que se encargará que de todos sus hijos definan obligatoriamente
 * todos los métodos necesarios para la manipulación de las categorías de los videos en
 * la base de datos.
 * @author Pablo Fonseca
 * @version 1.0
 */
public interface ICategoriaVideo {
    /**
     * Método que retorna todas las categorías definidas en la base de datos, la información se retorna como objetos.
     * @return HashMap con la llave numérica y con objetos de tipo CategoriaVideo.
     * o null si ocurrió algún fallo en el proceso.
     */
    public HashMap<Integer, CategoriaVideo> obtenerCategorias();

    /**
     * Método que retorna el ID de la categoría seleccionada o -1 en caso de que la categoría no pudiera ser
     * encontrada.
     * @param descripcionCategoria: Nombre de la categoría que será consultada.
     * @return el ID de la categoría o -1 si no pudo ser encontrada.
     */
    public int obtenerIDCategoria(String descripcionCategoria);

    /**
     * Consulta el nombre de la categoría de un video.
     * @param IDCategoria: ID de la categoría.
     * @return el nombre de la categoría.
     */
    public String obtenerNombreCategoria(int IDCategoria);

    /**
     * Registra una categoría en la base de datos.
     * @param nombre: Nombre de la categoría que será registrada.
     * @param descripcion: Descripción de la categoría que será registrada.
     * @return true si la categoría pudo ser registrada.
     */
    public boolean registrarCategoria(String nombre, String descripcion);

    /**
     * Elimina una categoría en la base de datos.
     * @param nombreCategoria: Nombre de la categoría que se va a eliminar.
     * @return true si la categoría pudo ser eliminada.
     */
    public boolean eliminarCategoria(String nombreCategoria);
}
