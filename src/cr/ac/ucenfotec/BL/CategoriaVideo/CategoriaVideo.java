package cr.ac.ucenfotec.BL.CategoriaVideo;

import cr.ac.ucenfotec.BL.Categoria.Categoria;

import java.util.Objects;

public class CategoriaVideo extends Categoria {

    private int IDCategoriaVideo;

    //Object Default Constructor
    public CategoriaVideo() {
    }

    //Object Complete Constructor
    public CategoriaVideo(String nombre, String descripcion) {
        super(nombre, descripcion);
    }

    //Object Third Alternative Constructor
    public CategoriaVideo(String nombre){
        super(nombre);
    }

    //Object Getter Access Methods
    public int getIDCategoriaVideo() {
        return IDCategoriaVideo;
    }

    //Object Setter Access Methods
    public void setIDCategoriaVideo(int IDCategoriaVideo) {
        this.IDCategoriaVideo = IDCategoriaVideo;
    }

    //Object To String Overridden Method
    @Override
    public String toString() {
        return super.toString();
    }

    //Object Equals Overridden Method
    //Compares with the name and the id.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CategoriaVideo that = (CategoriaVideo) o;
        return IDCategoriaVideo == that.IDCategoriaVideo;
    }
}
