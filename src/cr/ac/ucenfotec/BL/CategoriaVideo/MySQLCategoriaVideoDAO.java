package cr.ac.ucenfotec.BL.CategoriaVideo;

import cr.ac.ucenfotec.Connector;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Clase que se encarga de manipular todas las consultas a nivel de
 * base de datos en cuanto a categorías de videos se refiera.
 * Implementa la interface ICategoria para poder comportarse como tal.
 * @author Pablo Fonseca
 * @version 1.0
 */
public class MySQLCategoriaVideoDAO implements ICategoriaVideo {

    /**
     * Método que retorna todas las categorías definidas en la base de datos, la información se retorna como objetos.
     * @return HashMap con la llave numérica y con objetos de tipo CategoriaVideo.
     * o null si ocurrió algún fallo en el proceso.
     */
    @Override
    public HashMap<Integer, CategoriaVideo> obtenerCategorias(){
        final String QUERY = "SELECT IDCategoria, DescripcionCategoria, NombreCategoria FROM categoria_video";
        HashMap<Integer, CategoriaVideo> hashMap = new HashMap<>();
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();

            int index = 0;

            int IDCategoria;
            String descripcionCategoria;
            String nombreCategoria;

            while(resultSet.next()){
                CategoriaVideo tempCategoria;
                IDCategoria = resultSet.getInt("IDCategoria");
                descripcionCategoria = resultSet.getString("DescripcionCategoria");
                nombreCategoria = resultSet.getString("NombreCategoria");

                //Se crea el objeto temporal
                tempCategoria = new CategoriaVideo(nombreCategoria, descripcionCategoria);
                tempCategoria.setIDCategoriaVideo(IDCategoria);

                hashMap.put(index, tempCategoria);

                index++;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hashMap;
    }

    /**
     * Método que retorna el ID de la categoría seleccionada o -1 en caso de que la categoría no pudiera ser
     * encontrada.
     *
     * @param descripcionCategoria : Nombre de la categoría que será consultada.
     * @return el ID de la categoría o -1 si no pudo ser encontrada.
     */
    @Override
    public int obtenerIDCategoria(String descripcionCategoria) {
        final String QUERY = "SELECT IDCategoria FROM categoria_video WHERE NombreCategoria = ?";
        int PKCategoria = -1;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, descripcionCategoria);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                PKCategoria = resultSet.getInt("IDCategoria");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return PKCategoria;
    }

    /**
     * Consulta el nombre de la categoría de un video.
     *
     * @param IDCategoria : ID de la categoría.
     * @Return el nombre de la categoría.
     */
    @Override
    public String obtenerNombreCategoria(int IDCategoria) {
        final String QUERY = "SELECT NombreCategoria FROM categoria_video WHERE IDCategoria  = ?";
        String nombreCategoria = null;
        try{

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDCategoria);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                nombreCategoria = resultSet.getString("NombreCategoria");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return nombreCategoria;
    }

    /**
     * Registra una categoría en la base de datos.
     *
     * @param nombre      : Nombre de la categoría que será registrada.
     * @param descripcion : Descripción de la categoría que será registrada.
     * @return true si la categoría pudo ser registrada.
     */
    @Override
    public boolean registrarCategoria(String nombre, String descripcion) {
        final String QUERY = "INSERT INTO categoria_video SET NombreCategoria = ?, DescripcionCategoria = ?;";
        boolean categoriaRegistrada = false;
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombre);
            preparedStatement.setString(2, descripcion);

            preparedStatement.execute();

            categoriaRegistrada = true;

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return categoriaRegistrada;

    }

    /**
     * Elimina una categoría en la base de datos.
     *
     * @param nombreCategoria : Nombre de la categoría que se va a eliminar.
     * @return true si la categoría pudo ser eliminada.
     */
    @Override
    public boolean eliminarCategoria(String nombreCategoria) {
        final String QUERY = "DELETE FROM categoria_video WHERE NombreCategoria = ?;";
        boolean categoriaEliminada = false;
        try{

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreCategoria);

            preparedStatement.execute();

            categoriaEliminada = true;

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return categoriaEliminada;
    }
}
