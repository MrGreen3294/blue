package cr.ac.ucenfotec.BL.Categoria;

import cr.ac.ucenfotec.BL.Clasificacion.Clasificacion;

public abstract class Categoria extends Clasificacion {

    //Object Default Constructor
    public Categoria() {
        super();
    }
    //Object Complete Constructor
    public Categoria(String nombre, String descripcion) {
        super(nombre, descripcion);
    }
    //Object Complete Second Constructor
    public Categoria(String nombre){super(nombre);}
    //Object To String Overridden Method
    @Override
    public String toString(){
        return super.toString();
    }

}
