package cr.ac.ucenfotec.BL.Rol;

import java.util.Objects;

public class Rol {

    //Object Attributes
    private int IDRol;
    private String nombre;
    private String descripcion;

    //Object Default Constructor
    public Rol(){
        this.nombre = "";
        this.descripcion = "";
    }

    //Object Complete Constructor
    public Rol(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    //Object Getter Access Methods
    public int getIDRol() {
        return IDRol;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    //Object Setter Access Methods
    public void setIDRol(int IDRol) {
        this.IDRol = IDRol;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //Object To String Overridden Method
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("-").append("Tipo de Rol").append(":").append("\t").append(this.getNombre()).append("\n");
        builder.append("-").append("Descripción").append(":").append("\t").append(this.getDescripcion()).append("\n");
        return builder.toString();
    }

    //Object Equals Overridden Method
    //Compares with the name
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rol rol = (Rol) o;
        return Objects.equals(nombre, rol.nombre);
    }

}
