package cr.ac.ucenfotec.BL.Rol;

import cr.ac.ucenfotec.Connector;
import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MySQLRolDAO implements IRol {
    /**
     * Valida que el rol del usuario final se encuentre en la base de datos.
     *
     * @return true si el rol existe.
     */
    @Override
    public boolean validarExistenciaRol(String valorRol) {
        boolean rolExiste = false;
        final String QUERY = "SELECT IDRol FROM ROL WHERE ValorRol = ?";
        try {
            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, valorRol);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                rolExiste = true;
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return rolExiste;
        }catch(Exception e){
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setTitle("ERROR");
            errorAlert.setHeaderText("ERROR INTERNO");
            errorAlert.setContentText("No se pudo establecer una conexión con la base de datos.");
            errorAlert.showAndWait();
            System.exit(1);
            Platform.exit();
        }
        return false;
    }

    /**
     * Inserta un rol predeterminado en la base de datos.
     *
     * @param valorRol : Nombre del rol que se va a insertar.
     */
    @Override
    public void insertarRol(String valorRol) {
        final String QUERY = "INSERT INTO ROL SET ValorRol = ?";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, valorRol);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);
            errorAlert.setTitle("ERROR");
            errorAlert.setHeaderText("ERROR INTERNO");
            errorAlert.setContentText("No se pudo establecer una conexión con la base de datos.");
            errorAlert.showAndWait();
            System.exit(1);
            Platform.exit();
        }
    }
}
