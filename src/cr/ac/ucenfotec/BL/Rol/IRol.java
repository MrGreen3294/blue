package cr.ac.ucenfotec.BL.Rol;

/**
 * Inteface que va a obligar que todos sus hijos implementen las funcionalidades necesarias para manipular
 * los roles de usuario en el sistema.
 */
public interface IRol {

    /**
     * Valida que un rol se encuentre registrado en la base de datos.
     * @param valorRol: Nombre del rol que se desea validar.
     * @return true si el rol existe.
     */
    public boolean validarExistenciaRol(String valorRol);

    /**
     * Inserta un rol predeterminado en la base de datos.
     * @param valorRol: Nombre del rol que se va a insertar.
     */
    public void insertarRol(String valorRol);
}
