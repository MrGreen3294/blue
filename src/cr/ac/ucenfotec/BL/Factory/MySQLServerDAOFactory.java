package cr.ac.ucenfotec.BL.Factory;

import cr.ac.ucenfotec.BL.Administrador.IAdministrador;
import cr.ac.ucenfotec.BL.Administrador.MySQLAdministradorDAO;
import cr.ac.ucenfotec.BL.CategoriaVideo.ICategoriaVideo;
import cr.ac.ucenfotec.BL.CategoriaVideo.MySQLCategoriaVideoDAO;
import cr.ac.ucenfotec.BL.Direccion.IDireccion;
import cr.ac.ucenfotec.BL.Direccion.MySQLDireccionDAO;
import cr.ac.ucenfotec.BL.ListaReproduccion.IListaReproduccion;
import cr.ac.ucenfotec.BL.ListaReproduccion.MySQLListaReproduccionDAO;
import cr.ac.ucenfotec.BL.Rol.IRol;
import cr.ac.ucenfotec.BL.Rol.MySQLRolDAO;
import cr.ac.ucenfotec.BL.TemaLista.ITemaLista;
import cr.ac.ucenfotec.BL.TemaLista.MySQLTemaListaDAO;
import cr.ac.ucenfotec.BL.Usuario.IUsuario;
import cr.ac.ucenfotec.BL.Usuario.MySQLUsuarioDAO;
import cr.ac.ucenfotec.BL.Video.IVideo;
import cr.ac.ucenfotec.BL.Video.MySQLVideoDAO;

public class MySQLServerDAOFactory extends DAOFactory{

    /**
     * @author Pablo Fonseca.
     * @return el constructor de la clase MySQLAdministradorDAO con el sello de la interface IAdministrador.
     */
    //Tira una clase llamada MySQLAdministradorDAO que implementa todas las normas
    //definidas dentro de la interface IAdministrador.
    public IAdministrador getAdministradorDAO(){
        //La clase MySQLAdministradorDAO será hija de la interface IAdministrador, que por ende
        //firma que implementará cada una de las normas aplicadas en la interface.
        return new MySQLAdministradorDAO(); //Este es el constructor.
    }

    /**
     * @author Pablo Fonseca.
     * @return el contructor de la clase MySQLDireccionDAO con el sello de la interface IDireccion.
     */
    @Override
    public IDireccion getDireccionDAO() {
        return new MySQLDireccionDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el construtor de la clase MySQLUsuarioDAO con el sello de la interface IUsuario.
     */
    @Override
    public IUsuario getUsuarioDAO() {
        return new MySQLUsuarioDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el constructor de la clase MySQLRolDAO con el sello de la interface IRol.
     */
    public IRol getRolDAO(){
        return new MySQLRolDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el constructor de la clase MySQLCategoriaVideoDAO con el sello de la interface ICategoriaVideo.
     */
    public ICategoriaVideo getCategoriaVideoDAO(){
        return new MySQLCategoriaVideoDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el contructor de la clase MySQLVideoDAO con el sello de la interface IVideo.
     */
    @Override
    public IVideo getVideoDAO() {
        return new MySQLVideoDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el constructor de la clase MySQLTemaListaDAO con el sello de la interface ITemaLista.
     */
    @Override
    public ITemaLista getTemaListaDAO(){
        return new MySQLTemaListaDAO();
    }

    /**
     * @author Pablo Fonseca.
     * @return el constructor de la clase MySQLListaReproduccionDAO con el sello de la interface IListaReproduccion.
     */
    public IListaReproduccion getListaReproduccionDAO(){ return new MySQLListaReproduccionDAO();
    }
}
