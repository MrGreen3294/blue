package cr.ac.ucenfotec.BL.Factory;

import cr.ac.ucenfotec.BL.Administrador.IAdministrador;
import cr.ac.ucenfotec.BL.CategoriaVideo.ICategoriaVideo;
import cr.ac.ucenfotec.BL.Direccion.IDireccion;
import cr.ac.ucenfotec.BL.ListaReproduccion.IListaReproduccion;
import cr.ac.ucenfotec.BL.Rol.IRol;
import cr.ac.ucenfotec.BL.TemaLista.ITemaLista;
import cr.ac.ucenfotec.BL.Usuario.IUsuario;
import cr.ac.ucenfotec.BL.Video.IVideo;

public abstract class DAOFactory {
    public static final int MY_SQL_SERVER = 1;
    public static final int SQL_SERVER = 2;

    public static DAOFactory getDAOFactory(int DAOFactoryType){
        switch(DAOFactoryType){
            case MY_SQL_SERVER:
                return new MySQLServerDAOFactory();
            case SQL_SERVER:
                return new SQLServerDAOFactory();
            default:
                throw new NullPointerException();
        }
    }

    public abstract IAdministrador getAdministradorDAO();

    public abstract IDireccion getDireccionDAO();

    public abstract IUsuario getUsuarioDAO();

    public abstract IRol getRolDAO();

    public abstract ICategoriaVideo getCategoriaVideoDAO();

    public abstract IVideo getVideoDAO();

    public abstract ITemaLista getTemaListaDAO();

    public abstract IListaReproduccion getListaReproduccionDAO();


}
