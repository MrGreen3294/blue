package cr.ac.ucenfotec.BL.Factory;

import cr.ac.ucenfotec.BL.Administrador.IAdministrador;
import cr.ac.ucenfotec.BL.CategoriaVideo.ICategoriaVideo;
import cr.ac.ucenfotec.BL.Direccion.IDireccion;
import cr.ac.ucenfotec.BL.ListaReproduccion.IListaReproduccion;
import cr.ac.ucenfotec.BL.Rol.IRol;
import cr.ac.ucenfotec.BL.TemaLista.ITemaLista;
import cr.ac.ucenfotec.BL.Usuario.IUsuario;
import cr.ac.ucenfotec.BL.Video.IVideo;

public class SQLServerDAOFactory extends DAOFactory{
    @Override
    public IAdministrador getAdministradorDAO() {
        return null;
    }

    @Override
    public IDireccion getDireccionDAO() {
        return null;
    }

    @Override
    public IUsuario getUsuarioDAO() {
        return null;
    }

    @Override
    public IRol getRolDAO() {
        return null;
    }

    @Override
    public ICategoriaVideo getCategoriaVideoDAO() {
        return null;
    }

    @Override
    public IVideo getVideoDAO() {
        return null;
    }

    @Override
    public ITemaLista getTemaListaDAO() {
        return null;
    }

    @Override
    public IListaReproduccion getListaReproduccionDAO() {
        return null;
    }
}
