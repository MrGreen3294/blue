package cr.ac.ucenfotec.BL.Lista;

import java.time.LocalDate;

public abstract class Lista {

    //Object Attributes
    protected String nombre;
    protected String descripcion;
    protected LocalDate fechaCreacion;

    //Object Default Constructor
    public Lista() {
        this.nombre = "";
        this.descripcion = "";
        this.fechaCreacion = LocalDate.now();
    }

    //Object Complete Constructor
    public Lista(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    //Object Constructor Third Alternative
    public Lista(String nombre, String descripcion, LocalDate fechaCreacion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
    }

    //Object Getter Methods
    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    //Object Setter Methods
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    //Object to string overriden method
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Nombre").append(":").append("\t").append(this.getNombre()).append("\n");
        builder.append("Descripción").append(":").append("\t").append(this.getDescripcion()).append("\n");
        if(this.fechaCreacion != null)
            builder.append("Fecha de Creación").append(":").append("\t").append(this.getFechaCreacion()).append("\n");
        return builder.toString();
    }

    //Object equals overridden method
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lista lista = (Lista) o;
        return nombre.equals(lista.nombre);
    }

}
