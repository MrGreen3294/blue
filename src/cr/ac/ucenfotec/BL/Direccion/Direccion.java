package cr.ac.ucenfotec.BL.Direccion;

import java.util.Objects;

public class Direccion {

    //Object Attributes
    private int IDDireccion;
    private String pais;
    private int IDPais;
    private String provincia;
    private int IDProvincia;
    private String canton;
    private int IDCanton;
    private String distrito;
    private int IDDistrito;
    private String ciudad;
    private int codigoPostal;

    //Default Constructor
    public Direccion(){
        this.pais = "";
        this.provincia = "";
        this.canton = "";
        this.distrito = "";
    }

    //Complete Constructor
    public Direccion(String provincia, String canton, String distrito) {
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.pais = "Costa Rica"; //Default Country Because the App is National
    }

    //Getter Methods

    public int getIDDireccion() {
        return IDDireccion;
    }

    public String getPais() {
        return pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getCanton() {
        return canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public String getCiudad() {
        return ciudad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public int getIDPais() {
        return IDPais;
    }

    public int getIDProvincia() {
        return IDProvincia;
    }

    public int getIDCanton() {
        return IDCanton;
    }

    public int getIDDistrito() {
        return IDDistrito;
    }

    //Setter Methods
    public void setIDDireccion(int IDDireccion) {
        this.IDDireccion = IDDireccion;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public void setIDPais(int IDPais) {
        this.IDPais = IDPais;
    }

    public void setIDProvincia(int IDProvincia) {
        this.IDProvincia = IDProvincia;
    }

    public void setIDCanton(int IDCanton) {
        this.IDCanton = IDCanton;
    }

    public void setIDDistrito(int IDDistrito) {
        this.IDDistrito = IDDistrito;
    }

    //Object To String Overridden Method
    public String toString(){
        StringBuilder builder = new StringBuilder();
        if(this.getPais() != null)
            builder.append("País").append(":").append("\t").append(this.getPais()).append("\n");
        if(this.getProvincia() != null)
            builder.append("Provincia").append(":").append("\t").append(this.getProvincia()).append("\n");
        if(this.getCanton() != null)
            builder.append("Cantón").append(":").append("\t").append(this.getCanton()).append("\n");
        if(this.getDistrito() != null)
            builder.append("Distrito").append(":").append("\t").append(this.getDistrito()).append("\n");
        if(this.ciudad != null){
            builder.append("Ciudad").append(":").append("\t").append(this.getCiudad()).append("\n");
        }
        if(this.codigoPostal > 0){
            builder.append("Código Postal").append(":").append("\t").append(this.getCodigoPostal()).append("\n");
        }
        return builder.toString();
    }

    //Object Equals Overridden Method
    //Compares with the id.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direccion direccion = (Direccion) o;
        return IDDireccion == direccion.IDDireccion;
    }
}
