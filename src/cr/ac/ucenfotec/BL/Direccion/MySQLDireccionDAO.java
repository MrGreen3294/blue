package cr.ac.ucenfotec.BL.Direccion;

import cr.ac.ucenfotec.Connector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;

public class MySQLDireccionDAO implements IDireccion{

    /**
     * Lista los paises creados por el administrador.
     *
     * @return hasmap de tipo String donde se almacenarán los paises.
     */
    @Override
    public HashMap<Integer, String> listarPaises() {
        final String QUERY = "SELECT * FROM PAIS";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();
            HashMap<Integer, String> map = new HashMap<>();
            int index = 1;
            while(resultSet.next()){
                map.put(index, resultSet.getString("DESCRIPCION"));
                index++;
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return map;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Selecciona de la base de datos el ID del país solicitado.
     *
     * @param nombrePais nombre del país a solicitar.
     * @return un entero con el ID del país consultado o -1, si el país no pudo ser encontrado.
     */
    @Override
    public int obtenerIDPais(String nombrePais) {
        final String QUERY = "SELECT IDPAIS from pais WHERE PAIS.Descripcion = ?";
        int IDPais = -1;
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                IDPais = resultSet.getInt("IDPAIS");
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return IDPais;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Lista las provincias creadas por el administrador.
     *
     * @return hashmap de tipo String donde se almacenarán las provincias.
     */
    @Override
    public HashMap<Integer, String> listarProvincias(int IDPais) {
        final String QUERY = "SELECT Descripcion FROM PROVINCIA WHERE IDPais = ?";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);
            preparedStatement.setInt(1, IDPais);
            ResultSet resultSet = preparedStatement.executeQuery();
            HashMap<Integer, String> map = new HashMap<>();
            int index = 1;
            while(resultSet.next()){
                map.put(index, resultSet.getString("DESCRIPCION"));
                index++;
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return map;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Selecciona de la base de datos el ID de la provincia solicitada.
     *
     * @param IDPais          : ID del país al que se le consultará el ID de su provincia seleccionada.
     * @param nombreProvincia nombre de la provincia que se va a solicitar.
     * @return un entero con el ID de la provincia que será consultada o -1, si la provincia no pudo ser encontrada.
     */
    @Override
    public int obtenerIDProvincia(int IDPais, String nombreProvincia) {
        final String QUERY = "SELECT IDProvincia FROM Provincia WHERE IDPais = ? AND Descripcion = ?";
        int IDProvincia = -1;
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setString(2, nombreProvincia);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                IDProvincia = resultSet.getInt("IDProvincia");
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return IDProvincia;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Lista los cantones creados por el administrador.
     *
     * @param IDPais      : ID del país al que se le consultarán sus cantones.
     * @param IDProvincia : ID de la provincia a la que se le consultarán sus cantones.
     * @return hasmap de tipo String donde se almacenarán los cantones.
     */
    @Override
    public HashMap<Integer, String> listarCantones(int IDPais, int IDProvincia) {
        final String QUERY = "SELECT Descripcion FROM Canton WHERE IDPais = ? AND IDProvincia = ?";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);

            ResultSet resultSet = preparedStatement.executeQuery();
            HashMap<Integer, String> map = new HashMap<>();
            int index = 1;
            while(resultSet.next()){
                map.put(index, resultSet.getString("DESCRIPCION"));
                index++;
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return map;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param IDPais     : ID del país al que se le consultará el ID del cantón.
     * @param IDProvincia  : ID de la provincia a la que se le consultará el ID del cantón.
     * @param nombreCanton : Nombre del cantón al que se le consultará su ID.
     * @return un entero con el ID del cantón consultado o -1, si el cantón no pudo ser encontrado.
     */
    @Override
    public int obtenerIDCanton(int IDPais, int IDProvincia, String nombreCanton) {
        final String QUERY = "SELECT IDCanton FROM canton WHERE IDPais = ? AND IDProvincia = ? AND Descripcion = ?";
        int IDCanton = -1;
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setString(3, nombreCanton);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                IDCanton = resultSet.getInt("IDCanton");
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return IDCanton;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Lista los distritos creados por el administrador.
     *
     * @param IDPais      : ID del país al que se le consultarán sus distritos.
     * @param IDProvincia : ID de la provincia a la que se le consultarán sus distritos.
     * @param IDCanton    : ID del cantón al que se le consultarán sus distritos.
     * @return HashMap de tipo String donde se almacenarán los distritos disponibles.
     */
    @Override
    public HashMap<Integer, String> listarDistritos(int IDPais, int IDProvincia, int IDCanton) {
        final String QUERY = "SELECT Descripcion from distrito WHERE IDPais = ? AND IDProvincia = ? AND IDCanton = ?";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setInt(3, IDCanton);

            ResultSet resultSet = preparedStatement.executeQuery();
            HashMap<Integer, String> map = new HashMap<>();
            int index = 1;
            while(resultSet.next()){
                map.put(index, resultSet.getString("DESCRIPCION"));
                index++;
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return map;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param IDPais         : ID del país al que se le consultará el ID del distrito.
     * @param IDProvincia    : ID de la provincia a la que se le consultará el ID del distrito.
     * @param IDCanton       : ID del cantón al que se le consultará el ID del distrito.
     * @param nombreDistrito : Nombre del cantón al que se le consultará su ID.
     * @return un entero con el ID del distrito consultado o -1, si el distrito no pudo ser encontrado.
     */
    @Override
    public int obtenerIDDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito) {
        final String QUERY = "SELECT IDDistrito from distrito WHERE IDPais = ? AND IDProvincia = ? AND IDCanton = ? " +
                "AND Descripcion= ?";
        int IDDistrito = -1;
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setInt(3, IDCanton);
            preparedStatement.setString(4, nombreDistrito);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                IDDistrito= resultSet.getInt("IDDistrito");
            }
            preparedStatement.close();
            Connector.cerrarConexion();
            return IDDistrito;
        }catch(Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Elimina un país de la base de datos.
     *
     * @param nombrePais : Nombre del país a eliminar.
     * @return true si el país pudo ser eliminado.
     */
    @Override
    public boolean eliminarPais(String nombrePais) {
        final String QUERY = "DELETE FROM PAIS WHERE Descripcion = ?";
        boolean procesoCompletado = false;
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            procesoCompletado = true;

        }catch(Exception e){
            e.printStackTrace();
        }
        return procesoCompletado;
    }

    /**
     * Agrega un país en la base de datos.
     *
     * @param nombrePais         : Nombre del país a eliminar.
     * @param codigoNumericoPais : Código numérico del país que se va a registrar.
     */
    @Override
    public void agregarPais(String nombrePais, String codigoNumericoPais) {
        final String QUERY = "INSERT INTO pais SET Descripcion = ?, IDPais = ?;";
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);
            preparedStatement.setString(2, codigoNumericoPais);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }
        catch(SQLIntegrityConstraintViolationException ignored){

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Agrega una provincia en la base de datos.
     *
     * @param nombreProvincia         : Nombre de la provincia.
     * @param codigoNumericoProvincia : Código numérico de la provincia.
     * @param IDPais                  : ID del país al que pertenece la provincia.
     */
    @Override
    public void agregarProvincia(String nombreProvincia, String codigoNumericoProvincia, int IDPais) {
        final String QUERY = "INSERT INTO Provincia SET Descripcion = ?, IDPais = ?, IDProvincia = ?;";
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreProvincia);
            preparedStatement.setInt(2, IDPais);
            preparedStatement.setString(3, codigoNumericoProvincia);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }
        catch(SQLIntegrityConstraintViolationException ignored){

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Elimina una provincia.
     * @param nombrePais: Nombre del país.
     * @param nombreProvincia: Nombre de la provincia.
     */
    @Override
    public void eliminarProvincia(String nombrePais, String nombreProvincia) {

        final String QUERY = "DELETE FROM provincia WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = " +
                "?) AND provincia.Descripcion = ?;";


        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);
            preparedStatement.setString(2, nombreProvincia);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Agrega un cantón.
     *
     * @param IDPais       : ID del país al que pertenece el cantón.
     * @param IDProvincia  : ID de la provincia a la que pertenece el cantón.
     * @param nombreCanton : Nombre del cantón.
     * @param codigoCanton : Código del cantón.
     */
    @Override
    public void agregarCanton(int IDPais, int IDProvincia, String nombreCanton, String codigoCanton) {
        final String QUERY = "INSERT INTO canton set IDPais = ?, IDProvincia = ?, Descripcion = ?, IDCanton = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setString(3, nombreCanton);
            preparedStatement.setString(4, codigoCanton);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(SQLIntegrityConstraintViolationException ignored){

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Elimina un cantón.
     *
     * @param nombrePais      : Nombre del país al que pertenece el cantón.
     * @param nombreProvincia : Nombre de la provincia a l aque pertenece el cantón.
     * @param nombreCanton    : Nombre del cantón al que pertenece el cantón.
     */
    @Override
    public void eliminarCanton(String nombrePais, String nombreProvincia, String nombreCanton) {
        final String QUERY = "DELETE FROM canton WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = ?) AND\n" +
                "                         IDProvincia = (SELECT IDProvincia FROM provincia WHERE provincia.Descripcion = ?) AND \n" +
                "                         canton.Descripcion = ?;";


        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);
            preparedStatement.setString(2, nombreProvincia);
            preparedStatement.setString(3, nombreCanton);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Agrega un distrito.
     *
     * @param IDPais         : ID del país al que pertenece el distrito.
     * @param IDProvincia    : ID de la provincia a la que pertenece el distrito.
     * @param IDCanton       : ID del cantón al que pertenece el distrito.
     * @param nombreDistrito : Nombre del distrito.
     * @param codigoDistrito : Código del distrito.
     */
    @Override
    public void agregarDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito, String codigoDistrito) {
        final String QUERY = "INSERT INTO distrito set IDPais = ?, IDProvincia = ?, IDCanton = ?, Descripcion = ?, IDDistrito = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setInt(3, IDCanton);
            preparedStatement.setString(4, nombreDistrito);
            preparedStatement.setString(5, codigoDistrito);
            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(SQLIntegrityConstraintViolationException ignored){

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Elimina un distrito.
     *
     * @param nombrePais      : Nombre del país al que pertenece el distrito.
     * @param nombreProvincia : Nombre de la provincia a la que pertenece el distrito.
     * @param nombreCanton    : Nombre del cantón a la que pertenece el distrito.
     * @param nombreDistrito  : Nombre del distrito a la que pertenece el distrito.
     */
    @Override
    public void eliminarDistrito(String nombrePais, String nombreProvincia, String nombreCanton, String nombreDistrito) {
        final String QUERY = "DELETE FROM distrito WHERE IDPais = (SELECT IDPais FROM pais WHERE pais.Descripcion = ?) AND \n" +
                "                           IDProvincia  = (SELECT IDProvincia FROM provincia WHERE provincia.Descripcion = ?) AND \n" +
                "                           IDCanton = (SELECT IDCanton FROM canton WHERE canton.Descripcion = ?) AND \n" +
                "                           distrito.Descripcion = ?;";


        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombrePais);
            preparedStatement.setString(2, nombreProvincia);
            preparedStatement.setString(3, nombreCanton);
            preparedStatement.setString(4, nombreDistrito);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Valida si al menos existe una dirección en la base de datos.
     *
     * @return true
     */
    @Override
    public boolean validarExistenciaDireccionCompleta() {
        final String QUERY = "SELECT IDDistrito FROM distrito WHERE 1=1;";
        boolean direccionExiste = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                direccionExiste = true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return direccionExiste;
    }


}
