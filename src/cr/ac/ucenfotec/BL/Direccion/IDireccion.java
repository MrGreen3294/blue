package cr.ac.ucenfotec.BL.Direccion;

import java.util.HashMap;

/**
 * Interface que va a obligar que todos sus hijos implementen las funcionalidades necesarias para manipular
 * las direcciones en el sistema.
 */
public interface IDireccion {
    /**
     * Lista los paises creados por el administrador.
     * @return hasmap de tipo String donde se almacenarán los paises.
     */
    public HashMap<Integer, String> listarPaises();

    /**
     * Selecciona de la base de datos el ID del país solicitado.
     * @param nombrePais nombre del país a solicitar.
     * @return un entero con el ID del país consultado o -1, si el país no pudo ser encontrado.
     */
    public int obtenerIDPais(String nombrePais);
    /**
     * Lista las provincias creadas por el administrador.
     * @param IDPais: ID del país al que se le consultarán sus provincias.
     * @return hashmap de tipo String donde se almacenarán las provincias.
     */
    public HashMap<Integer, String> listarProvincias(int IDPais);

    /**
     * Selecciona de la base de datos el ID de la provincia solicitada.
     * @param IDPais: ID del país al que se le consultará el ID de su provincia seleccionada.
     * @param nombreProvincia nombre de la provincia que se va a solicitar.
     * @return un entero con el ID de la provincia que será consultada o -1, si la provincia no pudo ser encontrada.
     */
    public int obtenerIDProvincia(int IDPais, String nombreProvincia);

    /**
     * Lista los cantones creados por el administrador.
     * @param IDPais: ID del país al que se le consultarán sus cantones.
     * @param IDProvincia: ID de la provincia a la que se le consultarán sus cantones.
     * @return hasmap de tipo String donde se almacenarán los cantones.
     */
    public HashMap<Integer, String> listarCantones(int IDPais, int IDProvincia);

    /**
     *
     * @param IDPais: ID del país al que se le consultará el ID del cantón.
     * @param IDProvincia: ID de la provincia a la que se le consultará el ID del cantón.
     * @param nombreCanton: Nombre del cantón al que se le consultará su ID.
     * @return un entero con el ID del cantón consultado o -1, si el cantón no pudo ser encontrado.
     */
    public int obtenerIDCanton(int IDPais, int IDProvincia, String nombreCanton);


    /**
     * Lista los distritos creados por el administrador.
     * @param IDPais: ID del país al que se le consultarán sus distritos.
     * @param IDProvincia: ID de la provincia a la que se le consultarán sus distritos.
     * @param IDCanton: ID del cantón al que se le consultarán sus distritos.
     * @return HashMap de tipo String donde se almacenarán los distritos disponibles.
     */
    public HashMap<Integer, String> listarDistritos(int IDPais, int IDProvincia, int IDCanton);


    /**
     * Obtiene el ID de un distrito seleccionado. 
     * @param IDPais: ID del país al que se le consultará el ID del distrito.
     * @param IDProvincia: ID de la provincia a la que se le consultará el ID del distrito.
     * @param IDCanton: ID del cantón al que se le consultará el ID del distrito.
     * @param nombreDistrito: Nombre del cantón al que se le consultará su ID.
     * @return un entero con el ID del distrito consultado o -1, si el distrito no pudo ser encontrado.
     */
    public int obtenerIDDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito);

    /**
     * Elimina un país de la base de datos.
     * @param nombrePais: Nombre del país a eliminar.
     * @return true si el país pudo ser eliminado.
     */
    public boolean eliminarPais(String nombrePais);

    /**
     * Agrega un país en la base de datos.
     * @param nombrePais: Nombre del país a eliminar.
     * @param codigoNumericoPais: Código numérico del país que se va a registrar.
     */
    public void agregarPais(String nombrePais, String codigoNumericoPais);

    /**
     * Agrega una provincia en la base de datos.
     * @param nombreProvincia: Nombre de la provincia.
     * @param codigoNumericoProvincia: Código numérico de la provincia.
     * @param IDPais: ID del país al que pertenece la provincia.
     */
    public void agregarProvincia(String nombreProvincia, String codigoNumericoProvincia, int IDPais);

    /**
     * Elimina una provincia.
     * @param nombrePais: Nombre del país.
     * @param nombreProvincia: Nombre de la provincia.
     */
    public void eliminarProvincia(String nombrePais, String nombreProvincia);

    /**
     * Agrega un cantón.
     * @param IDPais: ID del país al que pertenece el cantón.
     * @param IDProvincia: ID de la provincia a la que pertenece el cantón.
     * @param nombreCanton: Nombre del cantón.
     * @param codigoCanton: Código del cantón.
     */
    public void agregarCanton(int IDPais, int IDProvincia, String nombreCanton, String codigoCanton);

    /**
     * Elimina un cantón.
     * @param nombrePais: Nombre del país al que pertenece el cantón.
     * @param nombreProvincia: Nombre de la provincia a l aque pertenece el cantón.
     * @param nombreCanton: Nombre del cantón al que pertenece el cantón.
     */
    public void eliminarCanton(String nombrePais, String nombreProvincia, String nombreCanton);
    /**
     * Agrega un distrito.
     * @param IDPais: ID del país al que pertenece el distrito.
     * @param IDProvincia: ID de la provincia a la que pertenece el distrito.
     * @param IDCanton: ID del cantón al que pertenece el distrito.
     * @param nombreDistrito: Nombre del distrito.
     * @param codigoDistrito: Código del distrito.
     */
    public void agregarDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito,
                                String codigoDistrito);

    /**
     * Elimina un distrito.
     * @param nombrePais: Nombre del país al que pertenece el distrito.
     * @param nombreProvincia: Nombre de la provincia a la que pertenece el distrito.
     * @param nombreCanton: Nombre del cantón a la que pertenece el distrito.
     * @param nombreDistrito: Nombre del distrito a la que pertenece el distrito.
     */
    public void eliminarDistrito(String nombrePais, String nombreProvincia, String nombreCanton, String nombreDistrito);

    /**
     * Valida si al menos existe una dirección en la base de datos.
     * @return true
     */
    public boolean validarExistenciaDireccionCompleta();

}
