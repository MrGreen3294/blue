package cr.ac.ucenfotec.BL.TemaLista;

import cr.ac.ucenfotec.BL.CategoriaVideo.CategoriaVideo;

import java.util.HashMap;

/**
 * Interface que se encargará que de todos sus hijos definan obligatoriamente
 * todos los métodos necesarios para la manipulación de los temas de las listas en
 * la base de datos.
 * @author Pablo Fonseca
 * @version 1.0
 */

public interface ITemaLista {
    /**
     * Método que retorna todas las listas definidas en la base de datos, la información se retorna como objetos.
     * @return HashMap con la llave numérica y con objetos de tipo TemaLista.
     * o null si ocurrió algún fallo en el proceso.
     */
    public HashMap<Integer, TemaLista> obtenerTemas();

    /**
     * Método que retorna el ID de una lista enviando a la consulta el nombre de la lista.
     * @param nombreTema: Nombre del tema de la lista que se enviará a la consulta.
     */
    public int obtenerIDTema(String nombreTema);

    /**
     * Método que retorna el nombre de un tema.
     * @param IDTema: ID del tema que se va a consultar.
     * @return String con el nombre del tema o
     */
    public String obtenerNombreTema(int IDTema);

    /**
     * Método que registra un tema.
     * @param nombre: Nombre del tema que se va a registrar.
     * @param descripcion: Descripción del tema que se va a registrar.
     * @return true si el tema pudo ser agregado.
     */
    public boolean agregarTema(String nombre, String descripcion);

    /**
     * Método que elimina un tema.
     * @param nombreTema: Nombre del tema que se va a eliminar.
     * @return true si el tema pudo ser eliminado.
     */
    boolean eliminarTema(String nombreTema);
}
