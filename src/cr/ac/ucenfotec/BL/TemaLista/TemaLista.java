package cr.ac.ucenfotec.BL.TemaLista;

import cr.ac.ucenfotec.BL.Tema.Tema;

public class TemaLista extends Tema {
    //Object Attributes
    private int IDTemaLista;

    //Object Default Constructor
    public TemaLista() {
        super();
    }

    //Object Complete Constructor
    public TemaLista(String nombre, String descripcion) {
        super(nombre, descripcion);
    }

    //Object Getter Access Methods
    public int getIDTemaLista() {
        return IDTemaLista;
    }

    //Object Setter Access Methods
    public void setIDTemaLista(int IDTemaLista) {
        this.IDTemaLista = IDTemaLista;
    }

    //Object To String Overridden Method
    @Override
    public String toString(){
        return super.toString();
    }

    //Object Equals Overridden Method
    //Compare with the name and the id.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TemaLista temaLista = (TemaLista) o;
        return IDTemaLista == temaLista.IDTemaLista;
    }

}
