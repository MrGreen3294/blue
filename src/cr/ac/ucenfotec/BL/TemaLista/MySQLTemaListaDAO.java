package cr.ac.ucenfotec.BL.TemaLista;

import cr.ac.ucenfotec.Connector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

public class MySQLTemaListaDAO implements ITemaLista{

    /**
     * Método que retorna todas las listas definidas en la base de datos, la información se retorna como objetos.
     *
     * @return HashMap con la llave numérica y con objetos de tipo TemaLista.
     * o null si ocurrió algún fallo en el proceso.
     */
    @Override
    public HashMap<Integer, TemaLista> obtenerTemas() {
        final String QUERY = "select NombreTema from tema_lista;";
        HashMap<Integer, TemaLista> map;
        int index = 0;
        try{

            map = new HashMap<>();

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                TemaLista objTemaLista = new TemaLista();
                objTemaLista.setNombre(resultSet.getString("NombreTema"));
                map.put(index, objTemaLista);
                index++;
            }

            preparedStatement.close();
            Connector.cerrarConexion();

            return map;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Método que retorna el ID de una lista enviando a la consulta el nombre de la lista.
     *
     * @param nombreTema : Nombre del tema de la lista que se enviará a la consulta.
     */
    @Override
    public int obtenerIDTema(String nombreTema) {
        final String QUERY = "SELECT IDTema FROM tema_lista Where NombreTema = ?";
        int llaveTema = -1;
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreTema);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                llaveTema = resultSet.getInt("IDTema");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return llaveTema;
    }

    /**
     * Método que retorna el nombre de un tema.
     *
     * @param IDTema : ID del tema que se va a consultar.
     * @return String con el nombre del tema o
     */
    @Override
    public String obtenerNombreTema(int IDTema) {
        final String QUERY = "SELECT NombreTema FROM tema_lista WHERE IDTema  = ?";
        String nombreTema = null;
        try{

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDTema);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                nombreTema = resultSet.getString("NombreTema");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return nombreTema;
    }

    /**
     * Método que registra un tema.
     *
     * @param nombre      : Nombre del tema que se va a registrar.
     * @param descripcion : Descripción del tema que se va a registrar.
     * @return true si el tema pudo ser agregado.
     */
    @Override
    public boolean agregarTema(String nombre, String descripcion) {
        final String QUERY = "INSERT INTO TEMA_LISTA SET NombreTema = ?, DescripcionTema = ?;";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombre);
            preparedStatement.setString(2, descripcion);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Método que elimina un tema.
     *
     * @param nombreTema : Nombre del tema que se va a eliminar.
     * @return true si el tema pudo ser eliminado.
     */
    @Override
    public boolean eliminarTema(String nombreTema) {
        final String QUERY = "DELETE FROM TEMA_LISTA WHERE NombreTema = ?; ";
        try{
            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreTema);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

}
