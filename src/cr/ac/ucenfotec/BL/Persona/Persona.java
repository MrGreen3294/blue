package cr.ac.ucenfotec.BL.Persona;

import cr.ac.ucenfotec.BL.Direccion.Direccion;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Objects;

public abstract class Persona {
    //Object Attributes
    protected String nombre;
    protected String primerApellido;
    protected String segundoApellido;
    protected LocalDate fechaNacimiento;
    protected String identificacion;
    protected int edad;

    //Object Relations Attributes
    protected Direccion localizacion;

    //Object Default Constructor
    public Persona(){
        this.nombre = "";
        this.primerApellido = "";
        this.segundoApellido = "";
        this.fechaNacimiento = LocalDate.now();
        this.identificacion = "";
        this.edad = 18;
    }

    //Object Complete Constructor
    public Persona(
            String nombre,
            String primerApellido,
            String segundoApellido,
            LocalDate fechaNacimiento,
            String identificacion) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaNacimiento = fechaNacimiento;
        this.identificacion = identificacion;
        this.edad = calculateAge(fechaNacimiento);
    }

    //Object Third Constructor Alternative
    public Persona(String nombre, String primerApellido, String segundoApellido) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
    }

    //Object Getter Methods
    public String getNombre() {
        return nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public int getEdad() {
        return edad;
    }

    //Object Getter Relational Methods
    public String getDireccion(){
        if(this.localizacion != null)
            return localizacion.toString();
        return "Dirección no definida";
    }

    //Object Setter methods
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
        this.edad = calculateAge(fechaNacimiento);
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    //Object Setter Relational Methods
    public void setLocalizacion(String provincia, String canton, String distrito){
        this.localizacion = new Direccion(provincia, canton, distrito);
    }

    //Object to string method override
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();

        builder.append("Nombre").append(":").append("\t").append(this.getNombre()).append("\n");
        builder.append("Primer Apellido").append(":").append("\t").append(this.getPrimerApellido()).append("\n");
        builder.append("Segundo Apellido").append(":").append("\t").append(this.getSegundoApellido()).append("\n");
        if(this.fechaNacimiento != null)
            builder.append("Fecha de Nacimiento").append(":").append("\t").append(this.getFechaNacimiento()).append("\n");
        if(this.edad > 0)
            builder.append("Edad").append(":").append("\t").append(this.getEdad()).append(" ").append("años").append(
                    "\n");
        if(this.identificacion != null)
            builder.append("Identificación").append(":").append("\t").append(this.getIdentificacion()).append("\n");
        if(this.localizacion != null){
            builder.append("Localización").append("\n");
            builder.append(localizacion.toString());
        }
        return builder.toString();
    }

    //Object equals method override
    //Compare with the identification.
    @Override
    public boolean equals(Object o) {
        //Compare if both obj are in the same memory position.
        if (this == o) return true;
        //Compare if "o" is null or if the class is different.
        if (o == null || getClass() != o.getClass()) return false;
        //If "o" is not in the same memory position but also is part of this class and is initialized so,
        //compare with custom definitions.
        Persona persona = (Persona) o;
        return identificacion.equals(persona.identificacion);
    }

    //Object Additions
    private int calculateAge(LocalDate birthday){
        LocalDate today = LocalDate.now();
        Period period = Period.between(birthday, today);
        return period.getYears();
    }

}
