package cr.ac.ucenfotec.BL.ListaReproduccion;

import cr.ac.ucenfotec.BL.Video.Video;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.PlayListModelTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interface que se encargará que de todos sus hijos definan obligatoriamente
 * todos los métodos necesarios para la manipulación de las listas de reproducción en
 * la base de datos.
 * @author Pablo Fonseca
 * @version 1.0
 */
public interface IListaReproduccion {
    /**
     * Inserta una lista en la base de datos.
     * @param listaGenerada: Objeto de tipo lista con la información necesaria de la lista.
     * @param IDUsuario: ID del usuario al que se le va a asignar la lista de reproducción.
     * @return ID de la lista que fue insertada en la base de datos o -1 si hubo alguna falla.
     */
    public int insertarLista(ListaReproduccion listaGenerada, int IDUsuario);

    /**
     * Actualiza el tema de una lista.
     * @param IDLista: ID del tema que se va a apegar a la lista de reproducción.
     */
    public void actualizarTema(int IDLista, int IDTema);

    /**
     * Método que obtiene se encarga de enlazar una lista con un video
     * @param IDLista: ID de la lista a la que se le va a asignar el video.
     * @param IDVideo: ID del video que se le va a asignar a la lista.
     * @return true si el enlace pudo ser completado.
     */
    public boolean enlazarVideo(int IDLista, int IDVideo);

    /**
     * Método que valida si existe una lista enviando el ID del usuario y el nombre de la lista a la base de datos.
     * @param IDUsuario : ID del usuario que está utilizando esa lista.
     * @param URLLista : URL Relativo de la lista de reproducción.
     * @return true si la lista existe o false si la lista puede ocupar ese identificador.
     */
    public boolean validarExistenciaLista(int IDUsuario, String URLLista);

    /**
     * Método que se encarga de actualizar la duración total de una lista.
     * @param IDLista: ID de la lista a la que se le va a actualizar el tiempo.
     */
    public void actualizarTiempoLista(int IDLista);

    /**
     * Método que se encarga de actualizar la calificación promedio de una lista.
     * @param IDLista: ID de la lista a la que se le va a actualizar la calificación.
     */
    public void actualizarCalificacionLista(int IDLista);

    /**
     * Método que se encarga de retornar la información requerida para la tabla que presentará las listas
     * de reproducción.
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    public HashMap<Integer, PlayListModelTable> obtenerListas(int IDUsuario);

    /**
     * Método que retorna el ID de una lista de reproducción.
     * @param nombreLista: Nombre de la lista de reproducción a la que se le consultará su ID.
     * @param IDUsuario: ID del usuario que utilizar
     */
    public int obtenerIDLista(String nombreLista, int IDUsuario);

    /**
     * Método que retorna el ID del tema de una lista.
     * @param IDListaReproduccion ID de la lista ligada al tema.
     * @return ID del tema o -1 si la lista no tiene un tema.
     */
    public int obtenerIDTema(int IDListaReproduccion);

    /**
     * Método que retorna los videos que pertenecen a una lista de reproducción.
     * @param IDListaReproduccion: ID de una lista de reproducción.
     * @return HashMap Collection con el nombre de los videos de la lista de reproducción.
     */
    public HashMap<Integer, String> obtenerNombreVideos(int IDListaReproduccion);

    /**
     * Método que se encarga de retorna los nombres de la listas disponibles en la base de datos.
     * @param IDUsuario: ID del usuario al que se le consultarán sus listas de reproducción.
     * @return ArrayList Collection con el nombre de todas las listas.
     */
    public ArrayList<String> obtenerNombreListas(int IDUsuario);

    /**
     * Método que se encarga de eliminar una lista de reproducción en la base de datos.
     * @param IDUsuario: ID del usuario al que le pertenecen las listas de reproducción.
     * @param nombreLista: Nombre de la lista de reproducción.
     * @return true si el proceso pudo ser completado.
     */
    public boolean eliminarListaReproduccion(int IDUsuario, String nombreLista);
}
