package cr.ac.ucenfotec.BL.ListaReproduccion;

import cr.ac.ucenfotec.BL.Lista.Lista;
import cr.ac.ucenfotec.BL.Tema.Tema;
import cr.ac.ucenfotec.BL.TemaLista.TemaLista;
import cr.ac.ucenfotec.BL.Video.Video;

import java.time.LocalDate;
import java.util.*;

public class ListaReproduccion extends Lista {

    //Object Attributes
    private int IDListaReproduccion;
    private double duracionTotal;
    private int calificacion;
    private String pathRelativo;

    //Object Relational Attributes
    private TemaLista temaLista;
    private HashMap<String, Video> videosAlmacenados;

    //Object Default Constructor
    public ListaReproduccion(){
        super();
        this.duracionTotal = .0;
        this.calificacion = 0;
        videosAlmacenados = new HashMap<>();
        pathRelativo = "";
    }

    //Object Complete Constructor
    public ListaReproduccion(String nombre, String descripcion, String pathRelativo) {
        super(nombre, descripcion);;
        this.pathRelativo = pathRelativo;
        videosAlmacenados = new HashMap<>();
    }

    //Object Third Constructor ALternative
    public ListaReproduccion(String nombre, String descripcion, String pathRelativo, LocalDate fechaCreacion) {
        super(nombre, descripcion, fechaCreacion);
        this.pathRelativo = pathRelativo;
        videosAlmacenados = new HashMap<>();
    }

    //Object Getter Access Methods
    public double getDuracionTotal() {
        return duracionTotal;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public int getIDListaReproduccion() {
        return IDListaReproduccion;
    }

    public String getPathRelativo() {
        return pathRelativo;
    }

    //Object Getters For Relational Attributes
    public String obtenerTemaLista(){
        return this.temaLista.toString();
    }

    public String obtenerVideosAlmacenados(){
        String format = "";
        int videoCounter = 1;
        /* Display content using Iterator*/
        Set set = videosAlmacenados.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            format += "Video (" + videoCounter + ")\n";
            format += mentry.getValue() + "\n";
            videoCounter++;
        }
        return format;
    }

    //Object Setter Access Methods
    public void setDuracionTotal(double duracionTotal) {
        this.duracionTotal = duracionTotal;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public void setIDListaReproduccion(int IDListaReproduccion) {
        this.IDListaReproduccion = IDListaReproduccion;
    }

    public void setPathRelativo(String pathRelativo) {
        this.pathRelativo = pathRelativo;
    }

    //Object Setters For Relational Attributes
    public void agregarTemaLista(TemaLista tempTemaLista){
        this.temaLista = tempTemaLista;
    }

    /**
     * Valida que el video que se va a incluir no esté dentro del HashMap
     * @param videoTemporal video a agregar
     * @return true si el video no puede ser agregado.
     */
    public boolean agregarVideo(Video videoTemporal){
        boolean videoExistence = false;
        Set set = videosAlmacenados.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry mentry = (Map.Entry) iterator.next();
            Video temp = (Video) mentry.getValue();
            if(videoTemporal.equals(temp)){
                videoExistence = true;
                break;
            }
        }
        if(!videoExistence){
            this.videosAlmacenados.put(videoTemporal.getNombre(), videoTemporal);
        }
        return videoExistence;
    }

    //Object to String Overridden Method
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Lista de Reproducción").append("\n");
        //Primitive and Composed Objects
        builder.append("Nombre").append(":").append("\t").append(this.getNombre()).append("\n");
        builder.append("Descripción").append(":").append("\t").append(this.getDescripcion()).append("\n");
        if (this.getDuracionTotal() > .0) {
            builder.append("Duración Total").append(":").append("\t").append(this.getDuracionTotal()).append("\n");
        }
        if (this.getFechaCreacion() != null) {
            builder.append("Fecha de Creación").append(":").append("\t").append(this.getFechaCreacion()).append("\n");
        }
        if (this.getCalificacion() > 0){
            builder.append("Calificación").append(":").append("\t").append(this.getCalificacion()).append("\n");
        }
        if(this.temaLista != null){
            builder.append("Tema").append("\n");
            builder.append(this.obtenerTemaLista()).append("\n");
        }
        if(this.videosAlmacenados != null){
            builder.append("Videos Almacenados").append("\n");
            builder.append(obtenerVideosAlmacenados()).append("\n");
        }
        if(this.pathRelativo != null){
            builder.append("Enlace Relativo de").append(" ").append(this.getNombre()).append(":").append(" ").append(this.getPathRelativo()).append(
                    "\n");
        }
        return builder.toString();
    }

    //Object Equals Overridden Method
    //Compares with the name and the id.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ListaReproduccion that = (ListaReproduccion) o;
        return IDListaReproduccion == that.IDListaReproduccion;
    }

}
