package cr.ac.ucenfotec.BL.ListaReproduccion;

import cr.ac.ucenfotec.BL.Video.Video;
import cr.ac.ucenfotec.Connector;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.PlayListModelTable;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class MySQLListaReproduccionDAO implements IListaReproduccion {

    /**
     * Inserta una lista en la base de datos.
     *
     * @param listaGenerada : Objeto de tipo lista con la información necesaria de la lista.
     * @param IDUsuario     : ID del usuario al que se le va a asignar la lista de reproducción.
     * @return ID de la lista que fue insertada en la base de datos o -1 si hubo una falla.
     */
    @Override
    public int insertarLista(ListaReproduccion listaGenerada, int IDUsuario) {

        final String QUERY = "INSERT INTO lista_videos SET Nombre = ?, Descripcion = ?, FechaCreacion = ?, RutaLista = ?, IDUsuario = ?;";
        int llaveGenerada = -1;

        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearKeyPreparedStatement(QUERY);

            preparedStatement.setString(1, listaGenerada.getNombre());
            preparedStatement.setString(2, listaGenerada.getDescripcion());
            preparedStatement.setDate(3, Date.valueOf(LocalDate.now()));
            preparedStatement.setString(4, listaGenerada.getPathRelativo());
            preparedStatement.setInt(5, IDUsuario);

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                llaveGenerada = resultSet.getInt(1);
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return llaveGenerada;
    }

    /**
     * Actualiza el tema de una lista.
     *
     * @param IDLista : ID del tema que se va a apegar a la lista de reproducción.
     */
    @Override
    public void actualizarTema(int IDLista, int IDTema) {

        final String QUERY = "UPDATE lista_videos SET IDTema = ? WHERE IDLista = ?;";

        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDTema);
            preparedStatement.setInt(2, IDLista);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que obtiene se encarga de enlazar una lista con un video
     *
     * @param IDLista : ID de la lista a la que se le va a asignar el video.
     * @param IDVideo : ID del video que se le va a asignar a la lista.
     * @return true si el enlace pudo ser completado.
     */
    @Override
    public boolean enlazarVideo(int IDLista, int IDVideo) {
        final String QUERY = "INSERT INTO videos_x_lista SET IDLista = ?, IDVideo = ?;";
        boolean procesoCompletado = false;

        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDLista);
            preparedStatement.setInt(2, IDVideo);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            procesoCompletado = true;

            //Se actualizan los tiempos de la lista
            actualizarTiempoLista(IDLista);

            //Se actualiza la calificación de la lista utilizando un promedio de la calificación del video.
            actualizarCalificacionLista(IDLista);
        } catch (Exception e) {
            return false;
        }
        return procesoCompletado;
    }


    /**
     * Método que valida si existe una lista enviando el ID del usuario y el nombre de la lista con el formato
     * correcto a la base de datos.
     *
     * @param IDUsuario   : ID del usuario que está utilizando el sistema.
     * @param nombreLista : URL Relativo de la lista de reproducción.
     * @return true si la lista existe o false si la lista puede ocupar ese identificador.
     */
    @Override
    public boolean validarExistenciaLista(int IDUsuario, String nombreLista) {
        final String QUERY = "SELECT IDLista FROM lista_videos WHERE IDUsuario = ? AND Nombre = ?;";
        boolean listaExiste = false;
        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreLista);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                listaExiste = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaExiste;
    }

    /**
     * Método que se encarga de actualizar la duración total de una lista.
     *
     * @param IDLista: ID de la lista a la que se le va a actualizar el tiempo.
     */
    @Override
    public void actualizarTiempoLista(int IDLista) {

        //SE OBTIENEN TODOS LOS TIEMPOS DE DURACIÓN DE CADA VIDEO.
        final String QUERY = "SELECT DuracionTotalVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM " +
                "videos_x_lista WHERE IDLista = ?);";

        try {
            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDLista);

            ResultSet resultSet = preparedStatement.executeQuery();

            double timeCounter = .0;
            while (resultSet.next()) {
                timeCounter += resultSet.getDouble("DuracionTotalVideo");
            }

            Connector.cerrarConexion();

            final String UPDATE_TIME = "UPDATE lista_videos SET DuracionTotal = ? WHERE IDLista = ?;";

            PreparedStatement timePreparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(UPDATE_TIME);

            timePreparedStatement.setDouble(1, timeCounter);
            timePreparedStatement.setInt(2, IDLista);

            timePreparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que se encarga de actualizar la calificación promedio de una lista.
     *
     * @param IDLista : ID de la lista a la que se le va a actualizar la calificación.
     */
    @Override
    public void actualizarCalificacionLista(int IDLista) {
        //SE OBTIENEN TODAS LAS CALIFICACIONES DE CADA VIDEO.
        final String QUERY = "SELECT CalificacionVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM " +
                "videos_x_lista WHERE IDLista = ?);";
        try {
            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDLista);

            ResultSet resultSet = preparedStatement.executeQuery();

            double ratingCounter = .0;
            double videoCount = 0;
            while (resultSet.next()) {
                ratingCounter += resultSet.getDouble("CalificacionVideo");
                videoCount++;
            }

            Connector.cerrarConexion();
            /*
             * The java.lang.Math.ceil(double a) returns
             * the smallest (closest to negative infinity) double value that is
             * greater than or equal to the argument and is equal to a mathematical integer.
             */
            double rating = Math.ceil(ratingCounter / videoCount);

            final String UPDATE_TIME = "UPDATE lista_videos SET Calificacion = ? WHERE IDLista = ?;";

            PreparedStatement timePreparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(UPDATE_TIME);

            timePreparedStatement.setDouble(1, rating);
            timePreparedStatement.setInt(2, IDLista);

            timePreparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que se encarga de retornar la información requerida para la tabla que presentará las listas
     * de reproducción.
     *
     * @param IDUsuario
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    @Override
    public HashMap<Integer, PlayListModelTable> obtenerListas(int IDUsuario) {
        final String QUERY = "SELECT Nombre, Descripcion, FechaCreacion, Calificacion, DuracionTotal FROM " +
                "lista_videos where IDUsuario = ?;";
        HashMap<Integer, PlayListModelTable> map = new HashMap<>();
        try {
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            int index = 0;
            while (resultSet.next()) {
                PlayListModelTable objPlayListModelTable = new PlayListModelTable(
                        resultSet.getString("Nombre"),
                        resultSet.getString("Descripcion"),
                        String.valueOf(resultSet.getDate("FechaCreacion")),
                        String.valueOf(resultSet.getDouble("Calificacion")),
                        String.valueOf(resultSet.getDouble("DuracionTotal")));
                map.put(index, objPlayListModelTable);
                index++;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    /**
     * Método que retorna el ID de una lista de reproducción.
     *
     * @param nombreLista : Nombre de la lista de reproducción a la que se le consultará su ID.
     * @param IDUsuario   : ID del usuario que utilizar
     */
    @Override
    public int obtenerIDLista(String nombreLista, int IDUsuario) {
        final String QUERY = "SELECT IDLista FROM lista_videos WHERE IDUsuario = ? AND Nombre = ?;";
        int llave = -1;
        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreLista);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                llave = resultSet.getInt("IDLista");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return llave;
    }

    /**
     * Método que retorna el ID del tema de una lista.
     *
     * @param IDListaReproduccion ID de la lista ligada al tema.
     * @return ID del tema o -1 si la lista no tiene un tema.
     */
    @Override
    public int obtenerIDTema(int IDListaReproduccion) {
        final String QUERY = "SELECT IDTema FROM lista_videos WHERE IDLista= ?";
        int llaveTema = -1;
        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDListaReproduccion);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next() && !(resultSet.wasNull())) {
                llaveTema = resultSet.getInt("IDTema");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return llaveTema;
    }

    /**
     * Método que retorna los videos que pertenecen a una lista de reproducción.
     *
     * @param IDListaReproduccion : ID de una lista de reproducción.
     * @return HashMap Collection con el nombre de los videos de la lista de reproducción.
     */
    @Override
    public HashMap<Integer, String> obtenerNombreVideos(int IDListaReproduccion) {
        final String QUERY = "SELECT NombreVideo FROM video WHERE IDVideo IN (SELECT IDVideo FROM videos_x_lista " +
                "WHERE IDLista = ?);";
        HashMap<Integer, String> map = new HashMap<>();
        try {

            if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDListaReproduccion);

            ResultSet resultSet = preparedStatement.executeQuery();

            int index = 0;

            while (resultSet.next()) {
                map.put(index, resultSet.getString("NombreVideo"));
                index++;
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * Método que se encarga de retorna los nombres de la listas disponibles en la base de datos.
     *
     * @param IDUsuario : ID del usuario al que se le consultarán sus PlayList.
     * @return ArrayList Collection con el nombre de todas las listas.
     */
    @Override
    public ArrayList<String> obtenerNombreListas(int IDUsuario) {
        final String QUERY = "SELECT Nombre FROM lista_videos WHERE IDUsuario = ?;";
        ArrayList<String> listas = null;
        try{

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            listas = new ArrayList<>();

            while(resultSet.next()){
                listas.add(resultSet.getString("Nombre"));
            }

            Connector.cerrarConexion();
            preparedStatement.close();

        }catch(Exception e){
            e.printStackTrace();
        }

        return listas;
    }

    /**
     * Método que se encarga de eliminar una lista de reproducción en la base de datos.
     *
     * @param IDUsuario   : ID del usuario al que le pertenecen las listas de reproducción.
     * @param nombreLista : Nombre de la lista de reproducción.
     * @return true si el proceso pudo ser completado.
     */
    @Override
    public boolean eliminarListaReproduccion(int IDUsuario, String nombreLista) {
        final String QUERY = "DELETE FROM lista_videos WHERE IDUsuario = ? AND Nombre = ?;";
        boolean listaEliminada = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreLista);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            listaEliminada = true;
        }catch(Exception e){
            e.printStackTrace();
        }

        return listaEliminada;
    }


}
