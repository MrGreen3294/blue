package cr.ac.ucenfotec.BL.Administrador;

/**
 * Esta interface va a contener todos los métodos en cuanto a Administradores se refiera,
 * para los procedimientos y consultas que deban hacerse en el transcurso de la ejecución en la aplicación
 * , los cuales va a heredar a quién implemente esta interface.
 * @author Pablo Fonseca
 * @version 1.0
 */
public interface IAdministrador {

    /**
     * Valida si ya existe un administrador en el sistema.
     * @return true si existe un usuario que ya tiene este rol.
     */
    public boolean validarExistenciaAdministrador();

    /**
     * Le asigna a un usuario el rol de administrador.
     * @param IDUsuario: ID del usuario al que se le asignará el rol
     * @return true si el rol pudo ser asignado.
     */
    public boolean asignarAdministrador(int IDUsuario);

    /**
     * Valida si un usuario es un Administrador o si no lo es.
     * La cuenta debe estar activa, en caso contrario el usuario no existe.
     * @param IDUsuario ID del usuario que se va a consultar.
     * @return true si el usuario consultado sí es un administrador.
     */
    public boolean validarUsuarioAdministrador(int IDUsuario);
}
