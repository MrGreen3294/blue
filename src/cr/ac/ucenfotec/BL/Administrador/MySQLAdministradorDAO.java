package cr.ac.ucenfotec.BL.Administrador;

import cr.ac.ucenfotec.Connector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MySQLAdministradorDAO implements IAdministrador{

    /**
     * Valida si ya existe un administrador en el sistema.
     *
     * @return true si existe un usuario que ya tiene este rol.
     */
    @Override
    public boolean validarExistenciaAdministrador() {
        final String QUERY = "SELECT IDUsuario FROM usuario_roles WHERE IDRol = (SELECT IDRol FROM ROL WHERE ROL" +
                ".ValorRol = 'Administrador');";
        boolean administradorExiste = false;
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                administradorExiste = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return administradorExiste;
    }

    /**
     * Le asigna a un usuario el rol de administrador.
     *
     * @param IDUsuario : ID del usuario al que se le asignará el rol
     * @return true si el rol pudo ser asignado.
     */
    @Override
    public boolean asignarAdministrador(int IDUsuario) {
        final String QUERY = "INSERT INTO usuario_roles SET IDUsuario = ?,  IDRol = (SELECT IDRol FROM Rol WHERE Rol" +
                ".ValorRol = 'Administrador');";
        boolean procesoCompletado = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            preparedStatement.execute();

            procesoCompletado = true;

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return procesoCompletado;
    }

    /**
     * Valida si un usuario es un Administrador o si no lo es.
     * La cuenta debe estar activa, en caso contrario el usuario no existe.
     * @param IDUsuario ID del usuario que se va a consultar.
     * @return true si el usuario consultado sí es un administrador.
     */
    @Override
    public boolean validarUsuarioAdministrador(int IDUsuario) {
        final String QUERY = "SELECT NombreUsuario FROM Usuario WHERE IDUsuario = ? AND IDUsuario IN (SELECT IDUsuario FROM Usuario_roles WHERE IDRol = (SELECT IDRol FROM ROL WHERE ValorRol = 'Administrador')) AND Estado = 'Y';";
        boolean usuarioAdmin  = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                usuarioAdmin = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return usuarioAdmin;
    }
}
