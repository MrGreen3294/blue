package cr.ac.ucenfotec.BL.Administrador;

import cr.ac.ucenfotec.BL.Codigo.Codigo;
import cr.ac.ucenfotec.BL.Rol.Rol;
import cr.ac.ucenfotec.BL.Usuario.Usuario;

public class Administrador extends Usuario {

    //Object Default Constructor
    public Administrador() {
        super();
    }

    //Object Complete Constructor
    public Administrador(
            String nombre,
            String primerApellido,
            String segundoApellido,
            String avatar,
            String claveSeguridad,
            String apodo,
            String correoElectronico) {
        super(nombre, primerApellido, segundoApellido, avatar, claveSeguridad, apodo, correoElectronico);
        super.agregarRol(new Rol("Administrador", "Tiene el control total del sistema"));
    }

    //Object To String Overridden Method
    @Override
    public String toString() {
        return super.toString();
    }
}
