package cr.ac.ucenfotec.BL.Video;

import cr.ac.ucenfotec.UI.ApplicationCustomClasses.VideoModelTable;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Interface que se encargará que de todos sus hijos definan obligatoriamente
 * todos los métodos necesarios para la manipulación de los videos en
 * la base de datos.
 * @author Pablo Fonseca
 * @version 1.0
 */
public interface IVideo {

    /**
     * Inserta un video en la base de datos.
     * @param videoGenerado: Objeto de tipo video con la información necesaria de los videos.
     * @param IDUsuario: ID del usuario al que se le va a asignar el video.
     * @return ID del video que fue insertado en la base de datos.
     */
    public int insertarVideo(Video videoGenerado, int IDUsuario);

    /**
     * Lista todos los videos asociados a un usuario.
     * @param IDUsuario: ID del usuario al que se le realizará la consulta.
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    public HashMap<Integer, VideoModelTable> obtenerVideos(int IDUsuario);

    /**
     * Valida si un video existe en la base de datos consultando sus datos mediante el nombre.
     * @param nombreVideo: Nombre del video que se enviará a la consulta.
     * @param IDUsuario: ID del usuario al que se le consultará el dato.
     * @return true si el video existe.
     */
    public boolean validarExistenciaVideo(int IDUsuario, String nombreVideo);

    /**
     * Lista todos los nombres de los videos asociados a un usuario.
     * @param IDUsuario: ID del usuario al que se le realizará la consulta.
     * @return ArrayList de tipo String con todos los nombres encontrados o null si ocurrió algún inconveniente.
     */
    public ArrayList<String> obtenerNombreVideos(int IDUsuario);

    /**
     * Retorna el URL Relativo de un video.
     * @param IDUsuario: ID del usuario al que se le consultará el dato.
     * @param nombreVideo: Nombre del video que se enviará a la consulta.
     * @return URL del Video.
     */
    public String obtenerURLRelativo(int IDUsuario, String nombreVideo);

    /**
     * Elimina un video.
     * @param IDUsuario: ID del usuario al que se le eliminará el video.
     * @param nombreVideo: Nombre del video que se eliminará de su perfil.
     * @return true si el proceso pudo ser completado exitosamente.
     */
    public boolean eliminarVideo(int IDUsuario, String nombreVideo);

    /**
     * Actualiza un video para asignarle una categoría.
     * @param IDCategoria ID de la categoría que se va a actualizar.
     * @param IDVideo ID del video que se va a actualizar.
     */
    public void actualizarCategoriaVideo(int IDCategoria, int IDVideo);

    /**
     * Retorna el ID de la categoría de un video.
     * @return ID de la categoría o -1 si no pudo ser encontrada.
     */
    public int obtenerIDCategoria(int IDVideo);

    /**
     * Retorna el ID de un video.
     * @param nombreVideo: Nombre del video.
     * @param IDUsuario: ID del usuario.
     * @return ID del video o -1 si no pudo ser encontrado.
     */
    public int obtenerIDVideo(String nombreVideo, int IDUsuario);

    /**
     * Se encarga de actualizar el último tiempo de reproducción de un vídeo para que el usuario no tenga que
     * ajustarlo nuevamente.
     * @param ultimoTiempoReproduccion: Último tiempo de reproducción de un video.
     * @param IDVideo: ID del video.
     */
    public void actualizarUltimoTiempoVideo(double ultimoTiempoReproduccion, int IDVideo);

    /**
     * Se encarga de obtener el último tiempo de reproducción de un video para que el usuario no tenga que
     * ajustarlo nuevamente.
     * @param IDVideo: ID del video del que se obtendrá su último tiempo de reproducción.
     * @return último tiempo de reproducción del video.
     */
    public double obtenerUltimoTiempoReproduccion(int IDVideo);

}