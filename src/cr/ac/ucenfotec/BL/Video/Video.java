package cr.ac.ucenfotec.BL.Video;

import cr.ac.ucenfotec.BL.Categoria.Categoria;
import cr.ac.ucenfotec.BL.CategoriaVideo.CategoriaVideo;

import java.time.LocalDate;
import java.util.ArrayList;

public class Video{

    //Object Attributes
    private int IDVideo;
    private String nombre;
    private LocalDate fechaCreacion;
    private String descripcion;
    private int calificacion;
    private double duracionTotal;
    private double tiempoReproduccion;
    private String pathRelativo;

    //Object Relational Attributes
    private CategoriaVideo categoriaVideo;

    //Object Default Constructor
    public Video(){
        this.nombre = "";
        this.fechaCreacion = LocalDate.now();
        this.descripcion = "";
        this.calificacion = 0;
        this.duracionTotal = .0;
        this.tiempoReproduccion = .0;
        this.pathRelativo = "";
    }

    //Object Complete Constructor
    public Video(String nombre, String descripcion, String pathRelativo) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.pathRelativo = pathRelativo;
    }

    //Object Alternative Third Constructor
    public Video(String nombre, String descripcion, String pathRelativo, int calificacion) {
        this.nombre = nombre;
        this.fechaCreacion = LocalDate.now();
        this.descripcion = descripcion;
        this.pathRelativo = pathRelativo;
        this.calificacion = calificacion;
    }

    //Object Getter Access Methods
    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public double getDuracionTotal() {
        return duracionTotal;
    }

    public double getTiempoReproduccion() {
        return tiempoReproduccion;
    }

    public int getIDVideo() {
        return IDVideo;
    }

    public String getPathRelativo() {
        return pathRelativo;
    }

    //Object Getters For Relational Attributes
    public String obtenerCategoria(){
        return categoriaVideo.toString();
    }

    public int obtenerIDCategoria() {return categoriaVideo.getIDCategoriaVideo();}
    //Object Setter Access Methods
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public void setDuracionTotal(double duracionTotal) {
        this.duracionTotal = duracionTotal;
    }

    public void setTiempoReproduccion(double tiempoReproduccion) {
        this.tiempoReproduccion = tiempoReproduccion;
    }

    public void setIDVideo(int IDVideo) {
        this.IDVideo = IDVideo;
    }

    public void setPathRelativo(String pathRelativo) {
        this.pathRelativo = pathRelativo;
    }

    //Object Setters For Relational Attributes
    public void agregarCategoria(CategoriaVideo tempCategoria){
        this.categoriaVideo = tempCategoria;
    }

    public void agregarIDCategoria(int IDCategoria){
        this.categoriaVideo.setIDCategoriaVideo(IDCategoria);
    }

    //Object To String Overridden Method
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Nombre").append(":").append("\t").append(this.getNombre()).append("\n");
        if(this.getFechaCreacion() != null) {
            builder.append("Fecha de Creación").append(":").append("\t").append(this.getFechaCreacion()).append("\n");
        }
        builder.append("Descripción").append(":").append("\t").append(this.getDescripcion()).append("\n");
        builder.append("Calificación").append(":").append("\t").append(this.getCalificacion()).append("\n");
        if(this.duracionTotal > 0)
            builder.append("Duración Total").append(":").append("\t").append(this.getDuracionTotal()).append("\n");
        if(this.tiempoReproduccion > 0)
            builder.append("Tiempo de la Última Reproducción").append(":").append("\t").append(this.getTiempoReproduccion()).append("\n");
        if(this.categoriaVideo != null){
            builder.append("Categoría del Video").append("\n");
            builder.append(this.obtenerCategoria()).append("\n");
        }
        if(this.pathRelativo != null){
            builder.append("Enlace relativo local del video").append(" ").append(this.getNombre()).append(":").append(
                    "\t").append(this.getPathRelativo()).append("\n");
        }
        return builder.toString();
    }

    //Object Equals Overridden Method
    //Compares only with the name of the video.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Video video = (Video) o;
        return nombre.equals(video.nombre);
    }

}
