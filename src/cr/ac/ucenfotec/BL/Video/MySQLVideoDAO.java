package cr.ac.ucenfotec.BL.Video;

import cr.ac.ucenfotec.Connector;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.VideoModelTable;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que implementará todos los métodos necesarios para manipular los videos en la base de datos.
 * @author Pablo Fonseca
 * @version 1.0
 */
public class MySQLVideoDAO implements IVideo{

    /**
     * Inserta un video en la base de datos.
     *
     * @param videoGenerado : Objeto de tipo video con la información necesaria de los videos.
     * @param IDUsuario     : ID del usuario al que se le va a asignar el video.
     * @return ID del video insertado en la base de datos.
     */
    @Override
    public int insertarVideo(Video videoGenerado, int IDUsuario) {
        final String QUERY = "INSERT INTO video SET IDVideo = ?, IDUsuario = ?, NombreVideo = ?, DescripcionVideo = " +
                "?, FechaCreacionVideo = ?, CalificacionVideo = ?, DuracionTotalVideo = ?, UltimoTiempoVideo =  ?, " +
                "URLVideo = ?";
        boolean procesoCompletado = false;
        int IDVideo = -1;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearKeyPreparedStatement(QUERY);

            preparedStatement.setInt(1, videoGenerado.getIDVideo());
            preparedStatement.setInt(2, IDUsuario);
            preparedStatement.setString(3, videoGenerado.getNombre());
            preparedStatement.setString(4, videoGenerado.getDescripcion());
            preparedStatement.setDate(5, Date.valueOf(LocalDate.now()));
            preparedStatement.setInt(6, videoGenerado.getCalificacion());
            preparedStatement.setDouble(7, videoGenerado.getDuracionTotal());
            preparedStatement.setDouble(8, .0);
            preparedStatement.setString(9, videoGenerado.getPathRelativo());

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()){
                IDVideo = resultSet.getInt(1);
            }

            procesoCompletado = true;

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return IDVideo;
    }

    /**
     * Lista todos los videos asociados a un usuario.
     *
     * @param IDUsuario : ID del usuario al que se le realizará la consulta.
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    @Override
    public HashMap<Integer, VideoModelTable> obtenerVideos(int IDUsuario) {
        final String QUERY = "SELECT NombreVideo, DescripcionVideo, FechaCreacionVideo, CalificacionVideo, " +
                "DuracionTotalVideo\n" +
                "FROM video WHERE IDUsuario = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement  preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            HashMap<Integer, VideoModelTable> map = new HashMap<>();
            int index = 0;

            while(resultSet.next()){
               VideoModelTable objVideoModelTable = new VideoModelTable(
                       resultSet.getString("NombreVideo"),
                       resultSet.getString("DescripcionVideo"),
                       resultSet.getDate("FechaCreacionVideo").toString(),
                       String.valueOf(resultSet.getInt("CalificacionVideo")),
                       String.valueOf(resultSet.getDouble("DuracionTotalVideo")));
               map.put(index, objVideoModelTable);
               index++;
            }

            preparedStatement.close();
            Connector.cerrarConexion();

            return map;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Valida si un video existe en la base de datos consultando sus datos mediante el nombre.
     *
     * @param IDUsuario   : ID del usuario al que se le consultará el dato.
     * @param nombreVideo : Nombre del video que se enviará a la consulta.
     * @return true si el video existe.
     */
    @Override
    public boolean validarExistenciaVideo(int IDUsuario, String nombreVideo) {
        final String QUERY = "SELECT IDVideo FROM Video WHERE IDUsuario = ? AND NombreVideo = ?";
        boolean videoExiste = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreVideo);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                videoExiste = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return videoExiste;
    }

    /**
     * Lista todos los nombres de los videos asociados a un usuario.
     *
     * @param IDUsuario : ID del usuario al que se le realizará la consulta.
     * @return ArrayList de tipo String con todos los nombres encontrados o null si ocurrió algún inconveniente.
     */
    @Override
    public ArrayList<String> obtenerNombreVideos(int IDUsuario) {
        final String QUERY = "SELECT NombreVideo FROM Video WHERE IDUsuario = ?";
        ArrayList<String> arrayList = new ArrayList<>();
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                arrayList.add(resultSet.getString("NombreVideo"));
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return arrayList;
    }

    /**
     * Retorna el URL Relativo de un video.
     *
     * @param IDUsuario   : ID del usuario al que se le consultará el dato.
     * @param nombreVideo : Nombre del video que se enviará a la consulta.
     * @return URL del Video.
     */
    @Override
    public String obtenerURLRelativo(int IDUsuario, String nombreVideo) {

        final String QUERY = "SELECT URLVideo  FROM video WHERE NombreVideo = ? AND IDUsuario = ?;";
        String URLVideo = null;

        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreVideo);
            preparedStatement.setInt(2, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                URLVideo = resultSet.getString("URLVideo");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return URLVideo;
    }

    /**
     * Elimina un video.
     *
     * @param IDUsuario   : ID del usuario al que se le eliminará el video.
     * @param nombreVideo : Nombre del video que se eliminará de su perfil.
     * @return true si el proceso pudo ser completado exitosamente.
     */
    @Override
    public boolean eliminarVideo(int IDUsuario, String nombreVideo) {
        final String QUERY =  "DELETE FROM VIDEO WHERE IDUsuario = ? AND NombreVideo = ?;";
        boolean procesoCompletado = false;

        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreVideo);

            preparedStatement.execute();

            procesoCompletado = true;

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return procesoCompletado;
    }

    /**
     * Actualiza un video para asignarle una categoría.
     *
     * @param IDCategoria ID de la categoría que se va a actualizar.
     * @param IDVideo     ID del video que se va a actualizar.
     */
    @Override
    public void actualizarCategoriaVideo(int IDCategoria, int IDVideo) {
        final String QUERY = "UPDATE VIDEO SET IDCategoria = ? WHERE IDVideo = ?;";

        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDCategoria);
            preparedStatement.setInt(2, IDVideo);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Retorna el ID de la categoría de un video.
     *
     * @param IDVideo ID del video al que se le consultará su categoría.
     * @return ID de la categoría o -1 si no pudo ser encontrada.
     */
    @Override
    public int obtenerIDCategoria(int IDVideo) {
        final String QUERY = "SELECT IDCategoria FROM Video WHERE IDVideo = ?";
        int IDCategoria = -1;
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDVideo);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next() && !(resultSet.wasNull())){
                IDCategoria = resultSet.getInt("IDCategoria");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return IDCategoria;
    }

    /**
     * Retorna el ID de un video.
     *
     * @param nombreVideo : Nombre del video.
     * @param IDUsuario   : ID del usuario.
     * @return ID del video o -1 si no pudo ser encontrado.
     */
    @Override
    public int obtenerIDVideo(String nombreVideo, int IDUsuario) {
        final String QUERY = "SELECT IDVideo FROM Video WHERE IDUsuario = ? AND NombreVideo = ?";
        int IDVideo = -1;

        try {

            if(Connector.estadoConexion()) {
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, nombreVideo);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                IDVideo = resultSet.getInt("IDVideo");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return IDVideo;
    }

    /**
     * Se encarga de actualizar el útlimo tiempo de reproducción de un vídeo para que el usuario no tenga que
     * ajustarlo nuevamente.
     *
     * @param ultimoTiempoReproduccion : Último tiempo de reproducción de un video.
     */
    @Override
    public void actualizarUltimoTiempoVideo(double ultimoTiempoReproduccion, int IDVideo) {
        final String QUERY = "UPDATE video set UltimoTiempoVideo = ? WHERE IDVideo = ?;";
        try{

            if(Connector.estadoConexion())
            {
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setDouble(1, ultimoTiempoReproduccion);
            preparedStatement.setInt(2, IDVideo);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Se encarga de obtener el último tiempo de reproducción de un video para que el usuario no tenga que
     * ajustarlo nuevamente.
     *
     * @param IDVideo : ID del video del que se obtendrá su último tiempo de reproducción.
     * @return último tiempo de reproducción del video.
     */
    @Override
    public double obtenerUltimoTiempoReproduccion(int IDVideo) {
        double ultimoTiempoReproduccion = .0;
        final String QUERY = "SELECT UltimoTiempoVideo from video where IDVideo = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDVideo);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                ultimoTiempoReproduccion = resultSet.getDouble("UltimoTiempoVideo");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return ultimoTiempoReproduccion;
    }


}
