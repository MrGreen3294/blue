package cr.ac.ucenfotec.BL.Tema;

import cr.ac.ucenfotec.BL.Clasificacion.Clasificacion;

public abstract class Tema extends Clasificacion {
    //Object Default Constructor
    public Tema() {
        super();
    }

    //Object Complete Constructor
    public Tema(String nombre, String descripcion) {
        super(nombre, descripcion);
    }

    //Object To String Overridden Method
    @Override
    public String toString(){
        return super.toString();
    }
}
