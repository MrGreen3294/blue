package cr.ac.ucenfotec.BL.UsuarioFinal;
import cr.ac.ucenfotec.BL.Codigo.Codigo;
import cr.ac.ucenfotec.BL.Rol.Rol;
import cr.ac.ucenfotec.BL.Usuario.Usuario;

import java.time.LocalDate;

public class UsuarioFinal extends Usuario{


    //Object Default Constructor
    public UsuarioFinal() {
        super();
    }

    //Object Complete Constructor
    public UsuarioFinal(
            String nombre,
            String primerApellido,
            String segundoApellido,
            LocalDate fechaNacimiento,
            String identificacion,
            String avatar,
            String claveSeguridad,
            String apodo,
            String correoElectronico) {
        super(nombre, primerApellido, segundoApellido, fechaNacimiento, identificacion, avatar, claveSeguridad, apodo, correoElectronico);
    }

    //Object To String Overridden Method
    @Override
    public String toString() {
        return super.toString();
    }
}
