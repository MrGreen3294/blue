package cr.ac.ucenfotec.BL.Usuario;


import cr.ac.ucenfotec.BL.UsuarioFinal.UsuarioFinal;

/**
 * Esta interface va a contener todos los métodos que podrán ser operados para cualquier tipo de usuario,
 * dichos métodos tienen cómo objetivo primordial operar los procedimientos y consultas que deban hacerse
 * en el transcurso de la ejecución en la aplicación, los cuales van a heredar a quién implemente esta interface.
 */
public interface IUsuario{

    /**
     * Método que actualiza la dirección propia de un usuario en la base de datos.
     * @param IDUsuario: Usuario al que se le insertará la dirección.
     * @param IDPais: País donde vive el usuario.
     * @param IDProvincia: Provincia donde vive el usuario.
     * @param IDCanton: Cantón donde vive el usuario.
     * @param IDDistrito: Distrito donde vive el usuario.
     * @return true si el proceso pudo ser completado exitosamente o false si hubo algún error.
     */
    public boolean insertarDireccion(int IDUsuario, int IDPais, int IDProvincia, int IDCanton, int IDDistrito);

    /**
     * Método que inserta un código de confirmación para un usuario en general.
     * @param IDUsuario: ID del usuario al que se le va a insertar el código.
     * @param codigoGenerado: Codigo generado que se desea insertar.
     * @return true si el código pudo ser insertado.
     */
    public boolean insertarCodigoConfirmacion(int IDUsuario, String codigoGenerado);

    /**
     * Método que actualiza un código de confirmación para un usuario en general.
     * @param IDUsuario: ID del usuario al que se le va a actualizar el código.
     * @param codigoGenerado: Codigo generado que se desea insertar.
     * @return true si el código pudo ser actualizado.
     */
    public boolean actualizarCodigoConfirmacion(int IDUsuario, String codigoGenerado);

    /**
     * Método que acutaliza la clave de seguridad de un usuario general.
     * @param IDUsuario: ID del usuario al que se la va a actualizar su clave de seguridad.
     * @param claveSeguridad: Nueva clave de seguridad que reemplazará la clave vieja.
     * @return true si la clave pudo ser actualizada.
     */
    public boolean actualizarClaveSeguridad(int IDUsuario, String claveSeguridad);
    /**
     * Método que retorna el código generico insertado para un usuario en la base de datos.
     * @param IDUsuario: ID del usuario al que se le consultará el código.
     * @return el código de confirmación o null si la operación no pudo ser completada.
     */
    public String obtenerCodigoConfirmacion(int IDUsuario);

    /**
     * Método que valida si el código enviado por parámetro es equivalente al código
     * insertado para un usuario en la base de datos.
     * @param IDUsuario: ID del usuario al que se le validará el código.
     * @param codigoUsuario: Codigo que se validará con el de la base de datos.
     * @return true si el código es válido.
     */
    public boolean validarCodigoConfirmacion(int IDUsuario, String codigoUsuario);

    /**
     * Método que se encarga de activar la cuenta de un usuario.
     * @param IDUsuario: ID del usuario al que se le activará la cuenta.
     * @return true si la cuenta pudo ser activada.
     */
    public boolean activarCuentaUsuario(int IDUsuario);

    /**
     * Método que se encarga de validar los credenciales de cualquier usuario para su inicio de sesión.
     * @param nombreUsuario nombre del usuario que se va a validar.
     * @param claveSeguridad clave del usuario que se va a validar.
     * @return true si los datos ingresados son correctos.
     */
    public boolean validarCredencialesUsuario(String nombreUsuario, String claveSeguridad);

    /**
     * Se encarga de validar la existencia de un usuario final mediante la revisión de su identificación.
     * NO SELECCIONA LAS CUENTAS QUE NO ESTÁN ACTIVAS.
     * @param identificacion: identificación del usuario que se intenta registrar.
     * @return true si el usuario ya existe en la base de datos.
     */
    public boolean validarExistenciaUsuarioFinal(String identificacion);

    /**
     * Se encarga de registrar un usuario final en la base de datos.
     * @param objUsuarioFinal objeto temporal generado de tipo UsuarioFinal.
     * @return ID de usuario final insertado.
     */
    public int registrarUsuarioFinal(UsuarioFinal objUsuarioFinal);

    /**
     * Se encarga de validar si existe un usuario activo que tenga un correo electrónico en la base de datos.
     * @param correoElectronico: Correo electrónico que se valida.
     * @return true si existe un usuario que tenga su cuenta activa y que tenga ese correo electrónico.
     *
     */
    public boolean validarExistenciaCorreoUsuario(String correoElectronico);

    /**
     * Se encarga de validar si existe un usuario activo que tenga el apodo indicado en la base de datos.
     * @param apodoUsuario: Apodo del usuario.
     * @return true si existe un usuario que tenga su cuenta activa y que tenga ese nombre de usuario.
     */
    boolean validarExistenciaApodoUsuario(String apodoUsuario);
    /**
     * Método que se encarga de retornar el ID de un usuario mediante el correo electrónico.
     * El usuario deberá tener su cuenta activada.
     * @return ID del usuario.
     */
    public int obtenerPKCorreo(String correoElectronico);

    /**
     * Método que se encarga de retorna el ID de un usuario mediante su clave de seguridad y su nombre de usuario.
     * El usuario deberá tener su cuenta activada.
     * @param nombreUsuario: nombre del usuario.
     * @param claveSeguridad: clave de seguridad del usuario.
     * @return ID del usuario o -1 si la operación no pudo ser completada.
     */
    public int obtenerPKUsuario(String nombreUsuario, String claveSeguridad);

    /**
     * Método que se encarga de retornar el correo electrónico de un usuario mediante su ID de usuario.
     * @param IDUsuario: ID del usuario.
     * @return correo electrónico del usuario.
     */
    public String obtenerCorreoElectronico(int IDUsuario);

    /**
     * Método que se encarga de retornar el apodo de un usuario mediante su ID de usuario.
     * @param IDUsuario: ID del usuario.
     * @return Apodo del usuario o null si no fue encontrado.
     */
    public String obtenerApodoUsuario(int IDUsuario);

    /**
     * Método que se encarga de retornar la identificación de un usuario mediante su ID de usuario.
     * @param IDUsuario: ID del usuario
     * @return Identificación del usuario o null si no fue encontrada.
     */
    public String obtenerIdentificacion(int IDUsuario);

    /**
     * Método que se encarga de eliminar una cuenta de un usuario en la base de datos 
     * y su contenido. 
      * @param IDUsuario: ID del usuario.
     * @return true si se pudo eliminar la cuenta.
     */
    public boolean eliminarCuentaUsuario(int IDUsuario);

    /**
     * Método que se encarga de eliminar todos los videos de un usuario.
     * @param IDUsuario: ID del usuario.
     * @return true si se pudo eliminar el video.
     */
    public boolean eliminarVideosUsuario(int IDUsuario);

    /**
     *
     * @param IDUsuario: ID del usuario.
     * @return true si las listas de reproducción pudieron ser eliminadas.
     */
    public boolean eliminarListasUsuario(int IDUsuario);

}
