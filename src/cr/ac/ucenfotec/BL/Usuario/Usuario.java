package cr.ac.ucenfotec.BL.Usuario;


import cr.ac.ucenfotec.BL.ListaReproduccion.ListaReproduccion;
import cr.ac.ucenfotec.BL.Persona.Persona;
import cr.ac.ucenfotec.BL.Rol.Rol;
import cr.ac.ucenfotec.BL.Video.Video;

import java.time.LocalDate;
import java.util.*;

public abstract class Usuario extends Persona {
    //Object Attributes
    protected int IDUsuario;
    protected String avatar;
    protected String claveSeguridad;
    protected String apodo;
    protected String correoElectronico;
    protected boolean activo;

    //Object Relations Attributes
    protected HashMap<String, Rol> roles;
    protected HashMap<String, Video> videosAsociados;
    protected HashMap<String, ListaReproduccion> listasAsociadas;

    //Object Default Constructor
    public Usuario() {
        super();
        IDUsuario = 0;
        avatar = "";
        claveSeguridad = "";
        apodo = "";
        correoElectronico = "";
        activo = false;
        roles = new HashMap<>();
        //Todo usuario tendrá el rol de usuario final cuando sea creado.
        Rol rolTemporal = new Rol("Usuario", "El usuario está autorizado de ejecutar las funcionalidades " +
                "básicas" +
                " del sistema.");
        roles.put("UsuarioFinal", rolTemporal);
        //Inicialización del HashMap de Videos.
        videosAsociados = new HashMap<>();
        //Inicialización del HashMap de las Listas de Reproducción.
        listasAsociadas = new HashMap<>();
    }

    //Object Complete Constructor
    public Usuario(
            String nombre,
            String primerApellido,
            String segundoApellido,
            String avatar,
            String claveSeguridad,
            String apodo,
            String correoElectronico) {
        super(nombre, primerApellido, segundoApellido);
        this.avatar = avatar;
        this.claveSeguridad = claveSeguridad;
        this.apodo = apodo;
        this.correoElectronico = correoElectronico;
        this.roles = new HashMap<>();
        //Todo usuario tendrá el rol de usuario final cuando sea creado.
        Rol rolTemporal = new Rol("Usuario", "El usuario está autorizado de ejecutar las funcionalidades " +
                "básicas" +
                " del sistema.");
        roles.put("UsuarioFinal", rolTemporal);
        //Inicialización del HashMap de videos.
        videosAsociados = new HashMap<>();
        //Inicialización del HashMap de las Listas de Reproducción.
        listasAsociadas = new HashMap<>();
    }

    //Object Third Constructor Alternative
    public Usuario(String nombre,
                   String primerApellido,
                   String segundoApellido,
                   LocalDate fechaNacimiento,
                   String identificacion,
                   String avatar,
                   String claveSeguridad,
                   String apodo,
                   String correoElectronico) {
        super(nombre, primerApellido, segundoApellido, fechaNacimiento, identificacion);
        this.avatar = avatar;
        this.claveSeguridad = claveSeguridad;
        this.apodo = apodo;
        this.correoElectronico = correoElectronico;
        this.roles = new HashMap<>();
        //Todo usuario tendrá el rol de usuario final cuando sea creado.
        Rol rolTemporal = new Rol("Usuario", "El usuario está autorizado de ejecutar las funcionalidades básicas" +
                " del sistema.");
        roles.put("UsuarioFinal", rolTemporal);
        //Inicialización del HashMap de videos.
        videosAsociados = new HashMap<>();
        //Inicialización del HashMap de las Listas de Reproducción.
        listasAsociadas = new HashMap<>();
    }

    //Object Getter Methods
    public int getIDUsuario() {
        return IDUsuario;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getApodo() {
        return apodo;
    }

    public boolean isActivo() {
        return activo;
    }

    public String getClaveSeguridad() {
        return claveSeguridad;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    //Object Getter For Relational Attributes
    public String obtenerRoles() {
        String format = "";
        Set set = roles.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            format += mentry.getValue() + "\n";
        }
        return format;
    }

    public String obtenerVideos(){
        int videoCounter = 1;
        String format = "";
        Set set = videosAsociados.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry mentry = (Map.Entry)iterator.next();
            format += "Video (" + videoCounter + ")" + "\n";
            format += mentry.getValue() + "\n";
            videoCounter++;
        }
        return format;
    }

    public String obtenerListas(){
        int listCounter = 1;
        String format = "";
        Set set = listasAsociadas.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry mentry = (Map.Entry)iterator.next();
            format += "Lista (" + listCounter + ")" + "\n";
            format += mentry.getValue() + "\n";
            listCounter++;
        }
        return format;
    }

    //Object Setter Methods
    public void setIDUsuario(int IDUsuario) {
        this.IDUsuario = IDUsuario;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setClaveSeguridad(String claveSeguridad) {
        this.claveSeguridad = claveSeguridad;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    //Object Setters For Relational Attributes
    /**
     * Valida que el rol a asignar no esté dentro del HashMap, luego lo registra.
     * La validación del equals de los roles se realiza con el nombre de ellos solamente.
     * @param rolTemporal rol a agregar para ese usuario.
     * @return true si el rol no puede ser agregado.
     */
    public boolean agregarRol(Rol rolTemporal){
        boolean rolExistence = false;
        Set set = roles.entrySet();
        Iterator iterator = set.iterator();
        //Itera y verifica si el rol se encuentra dentro del HashMap usando el equals de los roles.
        //Si el rol no existe entonces lo agrega, pero si el rol existe no lo agrega.
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            Rol temp = (Rol) mentry.getValue();
            if(rolTemporal.equals(temp)){
                rolExistence = true;
                break;
            }
        }
        if(!rolExistence)
            this.roles.put(rolTemporal.getNombre(), rolTemporal);
        return rolExistence;
    }

    /**
     * Valida que el video a asignar no esté dentro del HashMap, luego lo registra.
     * La validación del equals de los videos se realiza con el nombre de ellos solamente.
     * @param videoTemporal video a agregar para ese usuario.
     * @return true si el video no puede ser agregado.
     */
    public boolean agregarVideo(Video videoTemporal){
        boolean videoExistence = false;
        Set set  = videosAsociados.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry mentry = (Map.Entry) iterator.next();
            Video temp = (Video) mentry.getValue();
            if(videoTemporal.equals(temp)){
                videoExistence = true;
                break;
            }
        }
        if(!videoExistence){
            this.videosAsociados.put(videoTemporal.getNombre(), videoTemporal);
        }
        return videoExistence;
    }
    /**
     * Valida que la lista a asignar no esté dentro del HashMap, luego lo registra.
     * La validación del equals de los videos se realiza con el nombre de ellos solamente.
     * @param listaTemporal lista de reproducción a agregar para ese usuario.
     * @return true si la lista de reproducción no puede ser agregada.
     */
    public boolean agregarLista(ListaReproduccion listaTemporal){
        boolean playlistExistence = false;
        Set set  = listasAsociadas.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()){
            Map.Entry mentry = (Map.Entry) iterator.next();
            ListaReproduccion temp = (ListaReproduccion) mentry.getValue();
            if(listaTemporal.equals(temp)){
                playlistExistence = true;
                break;
            }
        }
        if(!playlistExistence){
            this.listasAsociadas.put(listaTemporal.getNombre(), listaTemporal);
        }
        return playlistExistence;
    }

    //Object to string overridden method
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString()).append("\n");
        builder.append("Avatar").append(":").append("\t").append(this.getAvatar()).append("\n");
        builder.append("Correo Electrónico").append(":").append("\t").append(this.getCorreoElectronico()).append("\n");
        builder.append("Nombre de Usuario").append(":").append("\t").append(this.getApodo()).append("\n");
        builder.append("Estado del Usuario").append(":").append("\t");
        if(isActivo())
            builder.append("Activo").append("\n");
        else
            builder.append("Inactivo").append("\n");
        /* Display content using Iterator*/
        //Los roles nunca estarán nulos porque un usuario siempre será un usuario final.
        builder.append("Roles del Usuario").append("\n");
        builder.append(this.obtenerRoles()).append("\n");
        if(this.videosAsociados != null){
            builder.append("Videos Asociados").append("\n");
            builder.append(this.obtenerVideos()).append("\n");
        }
        if(this.listasAsociadas != null){
            builder.append("Listas Asociadas").append("\n");
            builder.append(this.obtenerListas()).append("\n");
        }
        return builder.toString();
    }

    //Object equals method override
    //Compare with the identification and the id.
    @Override
    public boolean equals(Object o) {
        //Compare if both obj are in the same memory position.
        if (this == o) return true;
        //Compare if "o" is null or if the class is different.
        if (o == null || getClass() != o.getClass()) return false;
        //If "o" is not in the same memory position but also is part of this class and is initialized so,
        //compare with custom definitions.
        if (!super.equals(o)) return false;
        Usuario usuario = (Usuario) o;
        return IDUsuario == usuario.IDUsuario;
    }
}
