package cr.ac.ucenfotec.BL.Usuario;
import cr.ac.ucenfotec.BL.UsuarioFinal.UsuarioFinal;
import cr.ac.ucenfotec.Connector;
import java.io.IOException;
import java.sql.*;


public class MySQLUsuarioDAO implements IUsuario{


    /**
     * Método que actualiza la dirección propia de un usuario en la base de datos.
     *
     * @param IDUsuario   : Usuario al que se le insertará la dirección.
     * @param IDPais      : País donde vive el usuario.
     * @param IDProvincia : Provincia donde vive el usuario.
     * @param IDCanton    : Cantón donde vive el usuario.
     * @param IDDistrito  : Distrito donde vive el usuario.
     * @return true si el proceso pudo ser completado exitosamente o false si hubo algún error.
     */
    @Override
    public boolean insertarDireccion(int IDUsuario, int IDPais, int IDProvincia, int IDCanton, int IDDistrito) {
        boolean operacionCompletada = false;
        final String QUERY = "UPDATE USUARIO SET IDPais = ?, IDProvincia = ?, IDCanton = ?, IDDistrito = ? " +
                "WHERE IDUsuario = ?;";
        try {

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDPais);
            preparedStatement.setInt(2, IDProvincia);
            preparedStatement.setInt(3, IDCanton);
            preparedStatement.setInt(4, IDDistrito);
            preparedStatement.setInt(5, IDUsuario);
            preparedStatement.executeUpdate();

            //Se cierra la conexión
            preparedStatement.close();
            Connector.cerrarConexion();

            operacionCompletada = true;

            return operacionCompletada;
        } catch(IOException e){
            System.err.println(e.getMessage());
            System.err.println("Ha ocurrido una complicación de tipo IOException");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return operacionCompletada;
    }

    /**
     * Método que inserta un código de confirmación para un usuario en general.
     * @param IDUsuario: ID del usuario al que se le va a insertar el código.
     * @param codigoGenerado : Codigo generado que se desea insertar.
     * @return true si el código pudo ser insertado.
     */
    @Override
    public boolean insertarCodigoConfirmacion(int IDUsuario, String codigoGenerado) {
        boolean operacionCompletada = false;
        final String QUERY = "INSERT INTO codigo_generico SET IDUsuario = ?, ValorCodigo = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);
            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.setString(2, codigoGenerado);
            preparedStatement.execute();
            operacionCompletada = true;

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return operacionCompletada;
    }

    /**
     * Método que actualiza un código de confirmación para un usuario en general.
     *
     * @param IDUsuario      : ID del usuario al que se le va a actualizar el código.
     * @param codigoGenerado : Codigo generado que se desea insertar.
     * @return true si el código pudo ser actualizado.
     */
    @Override
    public boolean actualizarCodigoConfirmacion(int IDUsuario, String codigoGenerado) {
        boolean operacionCompletada = false;
        final String QUERY = "UPDATE codigo_generico SET ValorCodigo = ? WHERE IDUsuario = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);
            preparedStatement.setString(1, codigoGenerado);
            preparedStatement.setInt(2, IDUsuario);
            preparedStatement.execute();
            operacionCompletada = true;

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return operacionCompletada;
    }

    /**
     * Método que acutaliza la clave de seguridad de un usuario general.
     *
     * @param IDUsuario      : ID del usuario al que se la va a actualizar su clave de seguridad.
     * @param claveSeguridad : Nueva clave de seguridad que reemplazará la clave vieja.
     * @return true si la clave pudo ser actualizada.
     */
    @Override
    public boolean actualizarClaveSeguridad(int IDUsuario, String claveSeguridad) {
        boolean operacionCompletada = false;
        final String QUERY = "UPDATE Usuario SET ClaveUsuario = ? WHERE IDUsuario = ?;";
        try{
          if(Connector.estadoConexion()){
              Connector.cerrarConexion();
          }

          PreparedStatement preparedStatement =
                  Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

          preparedStatement.setString(1, claveSeguridad);
          preparedStatement.setInt(2, IDUsuario);

          preparedStatement.execute();

          operacionCompletada = true;

          preparedStatement.close();
          Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return operacionCompletada;
    }


    /**
     * Método que retorna el código generico insertado para un usuario en la base de datos.
     *
     * @param IDUsuario : ID del usuario al que se le consultará el código.
     * @return el código de confirmación o null si la operación no pudo ser completada.
     */
    @Override
    public String obtenerCodigoConfirmacion(int IDUsuario) {
        String codigoGenerico = null;
        final String QUERY = "SELECT ValorCodigo FROM Codigo_generico Where IDUsuario  = ?;";
       try {
           if (Connector.estadoConexion()) {
                Connector.cerrarConexion();
           }
           PreparedStatement preparedStatement =
                   Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

           preparedStatement.setInt(1, IDUsuario);

           ResultSet resultSet = preparedStatement.executeQuery();

           if(resultSet.next()){
               codigoGenerico = resultSet.getString("ValorCodigo");
           }

           preparedStatement.close();
           Connector.cerrarConexion();
       }catch(Exception e){
           e.printStackTrace();
       }
       return codigoGenerico;
    }

    /**
     * Método que valida si el código enviado por parámetro es equivalente al código
     * insertado para un usuario en la base de datos.
     *
     * @param IDUsuario     : ID del usuario al que se le validará el código.
     * @param codigoUsuario : Codigo que se validará con el de la base de datos.
     * @return true si el código es válido.
     */
    @Override
    public boolean validarCodigoConfirmacion(int IDUsuario, String codigoUsuario) {
        final String QUERY = "SELECT IDCodigo FROM codigo_generico WHERE ValorCodigo = ? AND IDUsuario = ?;";
        boolean codigoCorrecto = false;
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, codigoUsuario);
            preparedStatement.setInt(2, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                codigoCorrecto = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }
        return codigoCorrecto;
    }

    /**
     * Método que se encarga de activar la cuenta de un usuario.
     *
     * @param IDUsuario : ID del usuario al que se le activará la cuenta.
     * @return true si la cuenta pudo ser activada.
     */
    @Override
    public boolean activarCuentaUsuario(int IDUsuario) {
        boolean cuentaActivada = false;
        final String QUERY = "UPDATE Usuario SET ESTADO = 'Y' WHERE IDUsuario = ?;";
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            cuentaActivada = true;
        }catch(Exception e) {
            e.printStackTrace();
        }

        return cuentaActivada;
    }

    /**
     * Método que se encarga de validar los credenciales de cualquier usuario para su inicio de sesión.
     *
     * @param nombreUsuario  nombre del usuario que se va a validar.
     * @param claveSeguridad clave del usuario que se va a validar.
     * @return true si los datos ingresados son correctos.
     */
    @Override
    public boolean validarCredencialesUsuario(String nombreUsuario, String claveSeguridad) {
        final String QUERY = "SELECT NombreUsuario FROM USUARIO WHERE ApodoUsuario = ? AND ClaveUsuario = ? AND " +
                "Estado = 'Y';";
        boolean credencialesValidos  = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreUsuario);
            preparedStatement.setString(2, claveSeguridad);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                credencialesValidos = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return credencialesValidos;
    }

    /**
     * Se encarga de validar la existencia de un usuario mediante la revisión de su identificación.
     * NO SELECCIONA LAS CUENTAS QUE NO ESTÁN ACTIVAS.
     * @param identificacion: identificación del usuario que se intenta registrar.
     * @return true si el usuario ya existe en la base de datos.
     */
    @Override
    public boolean validarExistenciaUsuarioFinal(String identificacion) {
        boolean usuarioExiste = false;
        final String QUERY = "SELECT IDUsuario FROM Usuario WHERE IDUsuario IN (SELECT IDUsuario FROM " +
                "usuario_datos_adicionales WHERE Identificacion = ?) AND Estado = 'Y';";
        try {

            if(Connector.estadoConexion())
                Connector.cerrarConexion();
            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);
            preparedStatement.setString(1, identificacion);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                usuarioExiste = true;
            }
            //Se cierra la conexión
            preparedStatement.close();
            Connector.cerrarConexion();

            return usuarioExiste;
        } catch(IOException e){
            System.err.println(e.getMessage());
            System.err.println("Ha ocurrido una complicación de tipo IOException");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.err.println("Ha ocurrido una complicación de tipo SQLException");
        } catch(Exception e) {
            e.printStackTrace();
        }
        return usuarioExiste;
    }

    /**
     * Se encarga de registrar un usuario en la base de datos.
     * @param objUsuarioFinal objeto temporal generado de tipo UsuarioFinal.
     * @return ID de usuario final insertado.
     */
    @Override
    public int registrarUsuarioFinal(UsuarioFinal objUsuarioFinal) {
        int llaveUsuario = -1;

        //Primero se insertan los datos fundamentales.
        final String QUERY = "INSERT INTO usuario SET ApodoUsuario = ?," +
                "NombreUsuario = ?," +
                "PrimerApellidoUsuario = ?," +
                "SegundoApellidoUsuario = ?," +
                "CorreoElectronicoUsuario = ?," +
                "AvatarUsuario = ?," +
                "ClaveUsuario = ?;";
        try {

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearKeyPreparedStatement(QUERY);

            preparedStatement.setString(1, objUsuarioFinal.getApodo());
            preparedStatement.setString(2, objUsuarioFinal.getNombre());
            preparedStatement.setString(3, objUsuarioFinal.getPrimerApellido());
            preparedStatement.setString(4, objUsuarioFinal.getSegundoApellido());
            preparedStatement.setString(5, objUsuarioFinal.getCorreoElectronico());
            preparedStatement.setString(6, objUsuarioFinal.getAvatar());
            preparedStatement.setString(7, objUsuarioFinal.getClaveSeguridad());


            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if(resultSet.next()){
                llaveUsuario = resultSet.getInt(1);
            }

            //Se cierra la conexión
            preparedStatement.close();
            Connector.cerrarConexion();

            //Una vez obtenida la llave se agregan los datos adicionales, la identificación y la fecha de nacimiento.
            final String INSERT_ADDITIONAL_DATA = "INSERT INTO usuario_datos_adicionales SET IDUsuario = ?," +
                    " Identificacion = ?, FechaNacimiento = ?";

            preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(INSERT_ADDITIONAL_DATA);

            preparedStatement.setInt(1, llaveUsuario);
            preparedStatement.setString(2, objUsuarioFinal.getIdentificacion());
            preparedStatement.setDate(3, Date.valueOf(objUsuarioFinal.getFechaNacimiento()));

            preparedStatement.execute();

            //Se cierra la conexión
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            //Ahora se le agrega el rol de usuario final
            final String ROL_QUERY = "INSERT INTO usuario_roles SET IDUsuario = ?, IDRol = (SELECT IDRol FROM ROL " +
                    "WHERE ValorRol = ?);";

            preparedStatement = Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(ROL_QUERY);

            preparedStatement.setInt(1, llaveUsuario);
            preparedStatement.setString(2, "UsuarioFinal");

            preparedStatement.execute();

            //Se cierra la conexión
            preparedStatement.close();
            Connector.cerrarConexion();

            return llaveUsuario;
        } catch(IOException e){
            System.err.println(e.getMessage());
            System.err.println("Ha ocurrido una complicación de tipo IOException");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Se encarga de validar si existe un usuario activo que tenga un correo electrónico en la base de datos.
     *
     * @param correoElectronico : Correo electrónico que se valida.
     * @return true si existe un usuario que tenga su cuenta activa y que tenga ese correo electrónico.
     */
    @Override
    public boolean validarExistenciaCorreoUsuario(String correoElectronico) {
        boolean usuarioExiste = false;
        final String QUERY = "SELECT IDUsuario FROM Usuario WHERE CorreoElectronicoUsuario = ? AND Estado = 'Y';";
        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, correoElectronico);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                usuarioExiste = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return usuarioExiste;
    }

    /**
     * Se encarga de validar si existe un usuario activo que tenga el apodo indicado en la base de datos.
     *
     * @param apodoUsuario : Apodo del usuario.
     * @return true si existe un usuario que tenga su cuenta activa y que tenga ese nombre de usuario.
     */
    @Override
    public boolean validarExistenciaApodoUsuario(String apodoUsuario) {

        boolean usuarioExiste = false;
        final String QUERY = "SELECT IDUsuario FROM Usuario WHERE ApodoUsuario = ? AND Estado = 'Y';";

        try{

            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, apodoUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                usuarioExiste = true;
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return usuarioExiste;
    }

    /**
     * Método que se encarga de retornar el ID de un usuario mediante la indicación el correo electrónico.
     * El usuario siempre deberá tener su cuenta activada.
     * @param correoElectronico
     * @return ID del usuario o -1 si el usuario no existe.
     */
    @Override
    public int obtenerPKCorreo(String correoElectronico) {
        final String QUERY = "SELECT IDUsuario FROM Usuario WHERE CorreoElectronicoUsuario = ? AND Estado = 'Y';";
        int IDUsuario = -1;

        try{

            if(Connector.estadoConexion())
                Connector.cerrarConexion();

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, correoElectronico);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                IDUsuario = resultSet.getInt("IDUsuario");
            }

            preparedStatement.close();
            Connector.cerrarConexion();

        }catch(Exception e){
            e.printStackTrace();
        }

        return IDUsuario;
    }

    /**
     * Método que se encarga de retorna el ID de un usuario mediante su clave de seguridad y su nombre de usuario.
     * El usuario deberá tener su cuenta activada.
     *
     * @param nombreUsuario  : nombre del usuario.
     * @param claveSeguridad : clave de seguridad del usuario.
     * @return ID del usuario o -1 si la operación no pudo ser completada.
     */
    @Override
    public int obtenerPKUsuario(String nombreUsuario, String claveSeguridad) {
        final String QUERY = "SELECT IDUsuario FROM Usuario WHERE ApodoUsuario = ? AND ClaveUsuario = ? AND " +
                "Estado = 'Y';";
        int PKUsuario = -1;

        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setString(1, nombreUsuario);
            preparedStatement.setString(2, claveSeguridad);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                PKUsuario = resultSet.getInt("IDUsuario");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        }catch(Exception e){
            e.printStackTrace();
        }

        return PKUsuario;
    }

    /**
     * Método que se encarga de retornar el correo electrónico de un usuario mediante su ID de usuario.
     *
     * @param IDUsuario : ID del usuario.
     * @return correo electrónico del usuario.
     */
    @Override
    public String obtenerCorreoElectronico(int IDUsuario) {
        final String QUERY = "SELECT CorreoElectronicoUsuario FROM Usuario WHERE IDUsuario = ?";
        String correoElectronico = null;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                correoElectronico = resultSet.getString("CorreoElectronicoUsuario");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return correoElectronico;
    }

    /**
     * Método que se encarga de retorna el apodo de un usuario mediante su ID de usuario.
     *
     * @param IDUsuario : ID del usuario.
     * @return Apodo del usuario o null si no fue encontrado.
     */
    @Override
    public String obtenerApodoUsuario(int IDUsuario) {
        final String QUERY = "SELECT ApodoUsuario FROM Usuario WHERE IDUsuario = ?";
        String apodoUsuario = null;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                apodoUsuario = resultSet.getString("ApodoUsuario");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return apodoUsuario;
    }

    /**
     * Método que se encarga de retornar la identificación de un usuario mediante su ID de usuario.
     *
     * @param IDUsuario : ID del usuario
     * @return Identificación del usuario o null si no fue encontrada.
     */
    @Override
    public String obtenerIdentificacion(int IDUsuario) {
        final String QUERY = "SELECT identificacion from usuario_datos_adicionales where IDUsuario = ?";
        String identificacion = null;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                identificacion = resultSet.getString("identificacion");
            }

            preparedStatement.close();
            Connector.cerrarConexion();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return identificacion;
    }

    /**
     * Método que se encarga de eliminar una cuenta de un usuario en la base de datos
     * y su contenido.
     *
     * @param IDUsuario : ID del usuario.
     * @return true si se pudo eliminar la cuenta.
     */
    @Override
    public boolean eliminarCuentaUsuario(int IDUsuario) {
        final String QUERY = "DELETE FROM Usuario WHERE IDUsuario = ?;";
        boolean procesoCompletado = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            procesoCompletado = true;
        }catch(Exception e){
            e.printStackTrace();
        }

        return procesoCompletado;
    }

    /**
     * Método que se encarga de eliminar todos los videos de un usuario.
     *
     * @param IDUsuario : ID del usuario.
     * @return true si se pudo eliminar el video.
     */
    @Override
    public boolean eliminarVideosUsuario(int IDUsuario) {
        final String QUERY = "DELETE FROM video WHERE IDUsuario = ?;";
        boolean procesoCompletado = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            procesoCompletado = true;

        }catch(Exception e) {
            e.printStackTrace();
        }
        return procesoCompletado;
    }

    /**
     * Método que se encarga de eliminar todas las listas de reproducción de un usuario.
     * @param IDUsuario : ID del usuario.
     * @return true si las listas de reproducción pudieron ser eliminadas.
     */
    @Override
    public boolean eliminarListasUsuario(int IDUsuario) {
        final String QUERY = "DELETE FROM lista_videos WHERE IDUsuario = ?; ";
        boolean procesoCompletado = false;
        try{
            if(Connector.estadoConexion()){
                Connector.cerrarConexion();
            }

            PreparedStatement preparedStatement =
                    Connector.getConnectorFactory(Connector.MYSQLSERVER).crearPreparedStatement(QUERY);

            preparedStatement.setInt(1, IDUsuario);
            preparedStatement.execute();

            preparedStatement.close();
            Connector.cerrarConexion();

            procesoCompletado = true;

        }catch(Exception e) {
            e.printStackTrace();
        }
        return procesoCompletado;
    }

}
