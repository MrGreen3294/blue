package cr.ac.ucenfotec.BL.Clasificacion;

public abstract class Clasificacion {

    //Object Attributes
    protected String nombre;
    protected String descripcion;

    //Object Default Constructor
    public Clasificacion() {
        this.nombre = "";
        this.descripcion = "";
    }

    //Object Complete Constructor
    public Clasificacion(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    //Object Third Alternative Constructor
    public Clasificacion(String nombre){
        this.nombre = nombre;
    }
    //Object Setter Access Methods
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //Object Getter Access Methods
    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    //Object To String Overridden Method
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("Nombre").append(":").append("\t").append(this.getNombre()).append("\n");
        builder.append("Descripción").append(":").append("\t").append(this.getDescripcion()).append("\n");
        return builder.toString();
    }

    //Object Equals Overriden Method.
    //Compare with the name.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clasificacion that = (Clasificacion) o;
        return nombre.equals(that.nombre);
    }

}
