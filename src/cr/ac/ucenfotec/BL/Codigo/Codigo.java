package cr.ac.ucenfotec.BL.Codigo;

import java.util.Objects;
import java.util.Random;

public class Codigo {
    //Object Attributes
    private int IDCodigo;
    private String valor;

    //Object Default Constructor
    public Codigo(){
        this.valor = "";
    }

    //Object Complete Constructor
    public Codigo(int alphaLength, int numberLength){
        setValor(alphaLength, numberLength);
    }

    //Object Getter Access Methods
    public String getValor() {
        return valor;
    }

    public int getIDCodigo() {
        return IDCodigo;
    }

    //Object Setter Access Methods
    public void setIDCodigo(int IDCodigo) {
        this.IDCodigo = IDCodigo;
    }

    public void setValor(int alphaLength, int numberLength) {
        StringBuilder stringCode = new StringBuilder();
        int totalLength = alphaLength + numberLength;
        for(int index = 0; index < totalLength; index++) {
            //Primero genero un número del uno al nueve, para determinar si genera una letra o un número.
            int desition = generateRandomInteger(10);
            //Si el numero es mayor a 5 se generará una letra
            if (desition > 5) {
                int randomInteger = generateRandomInteger(26);
                int ascii_letter = 'A';
                for(int eggs = 0; eggs < randomInteger; eggs++){
                    ascii_letter++;
                }
                char c = (char) ascii_letter;
                stringCode.append(c);
            }else{//Si no lo es se generará un número entre 0 y 9
                int randomInteger = generateRandomInteger(10);
                stringCode.append(randomInteger);
            }
        }
        this.valor = stringCode.toString();
    }
    //Object To String Overridden Method
    @Override
    public String toString(){
         StringBuilder builder = new StringBuilder();
         builder.append("Valor").append(":").append("\t").append(this.getValor()).append("\n");
         return builder.toString();
    }

    //Object Equals Overridden Method
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Codigo codigo = (Codigo) o;
        return Objects.equals(valor, codigo.valor);
    }

    //Object Additional Functionalities
    //Generates a random integer between two ranges
    private static int generateRandomInteger(int max){
        //create an instance of Random Class
        Random rand = new Random();

        //Generates a random integer in range 0 to max - 1 inclusive
        int randomReal = rand.nextInt(max);

        return randomReal;
    }
}
