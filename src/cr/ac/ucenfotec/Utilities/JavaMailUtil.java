package cr.ac.ucenfotec.Utilities;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class JavaMailUtil{
    //Receive the receiver of the mail
    public static void sendMail(String fromEmailAccount, String emailPassword, String toEmailAccount,
                                String genericCode) throws MessagingException{
        //Properties is a key: value store like JSON
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        Session session = Session.getInstance(properties, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(fromEmailAccount, emailPassword);
            }
        });

        Message message = prepareMessage(session, fromEmailAccount, toEmailAccount, genericCode);
        Transport.send(message);
        //These are the four fields you need to configure in order to send an email.

        //mail.smtp.auth a note indication is needed to the email server fo for example: if is the smtp of Gmail is
        // mandatory to have a username and password to log into this maid right.

        //this is for TLS Encryption, in Gmail this value need to be true

        //this is the host, in every mail server you have a host and port, so for gmail is
        //mail.smtp.host = smtp.gmail.com

        //The port is 587 for Gmail
        //mail.smtp.port = 587
    }
    private static Message prepareMessage(Session session, String fromEmailAccount, String recipientEmailAccount,
                                          String genericCode) {
        Message message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(fromEmailAccount));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmailAccount));
            message.setSubject("Blue Arts Verification Code");
            message.setText(genericCode);
            return message;
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
