package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.Categoria.Categoria;
import cr.ac.ucenfotec.BL.CategoriaVideo.CategoriaVideo;
import cr.ac.ucenfotec.BL.CategoriaVideo.ICategoriaVideo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller que se encarga de transferir todas las peticiones que estén relacionadas con las categorías de los
 * videos.
 * @author Pablo Fonseca
 * @version 1.0
 */
public class ControllerCategoria extends Controller{
    /**
     * Transfiere la lista de categorías creadas de tipo video.
     */
    public ArrayList<String> obtenerCategorias(){
        ICategoriaVideo categoriaVideo = super.MySQLFactory.getCategoriaVideoDAO();
        HashMap<Integer, CategoriaVideo> hashMap  = categoriaVideo.obtenerCategorias();
        ArrayList<String> categorias = new ArrayList<>();
        for(Map.Entry<Integer, CategoriaVideo> entry: hashMap.entrySet()){
            categorias.add(entry.getValue().getNombre());
        }
        return categorias;
    }

    /**
     * Transfiere el ID de una categoría
     * @param nombreCategoria: Nombre de la categoría que se va a consultar.
     * @return ID de la categoría consultada o -1 si no pudo ser encontrada.
     */
    public int obtenerIDCategoria(String nombreCategoria){
        ICategoriaVideo categoriaVideo = super.MySQLFactory.getCategoriaVideoDAO();
        return categoriaVideo.obtenerIDCategoria(nombreCategoria);
    }

    /**
     * Transfiere el ID de una categoría.
     * Retorna el nombre de la categoría o null si no fue encontrada.
     * @param IDCategoria: ID de la categoria.
     */
    public String obtenerNombreCategoria(int IDCategoria){
        ICategoriaVideo categoriaVideo = super.MySQLFactory.getCategoriaVideoDAO();
        return categoriaVideo.obtenerNombreCategoria(IDCategoria);

    }

    /**
     * Transfiere los datos fundamentales de una categoría para registrarla.
     * Retorna true si la categoría pudo ser registrada.
     * @param nombre: Nombre de la categoría.
     * @param descripcion: Descripción de la categoría.
     * @return true si la categoría pudo ser registrada.
     */
    public boolean agregarCategoria(String nombre, String descripcion){
        ICategoriaVideo categoriaVideo = super.MySQLFactory.getCategoriaVideoDAO();
        return categoriaVideo.registrarCategoria(nombre, descripcion);
    }

    /**
     * Transfiere el nombre de la categoría.
     * Retorna true si la categoría pudo ser eliminada.
     * @param nombreCategoria: Nombre de la categoría.
     * @return true si la categoría pudo ser eliminada.
     */
    public boolean eliminarCategoria(String nombreCategoria) {
        ICategoriaVideo categoriaVideo = super.MySQLFactory.getCategoriaVideoDAO();
        return categoriaVideo.eliminarCategoria(nombreCategoria);
    }

}
