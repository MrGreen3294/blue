package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.ListaReproduccion.IListaReproduccion;
import cr.ac.ucenfotec.BL.ListaReproduccion.ListaReproduccion;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.PlayListModelTable;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que se encarga de transferir todas las peticiones que estén relacionadas con las listas de reproducción.
 */
public class ControllerListaReproduccion extends Controller{

    /**
     * Método que se encarga de transferir los datos fundamentales para realizar el registro de una lista de
     * reproducción.
     * @param listaMap HashMap Collection con la información del objeto que se va a convertir para enviarlo a la base
     *                de datos.
     * @return ID de la lista registrada.
     */
    public int registrarListaReproduccion(HashMap<String, Object> listaMap){
        ListaReproduccion objListaReproduccion = new ListaReproduccion(
                String.valueOf(listaMap.get("NombreLista")),
                String.valueOf(listaMap.get("DescripcionLista")),
                String.valueOf(listaMap.get("URLLista"))
        );

        int IDUsuario = Integer.parseInt(String.valueOf(listaMap.get("IDUsuario")));

        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.insertarLista(objListaReproduccion, IDUsuario);
    }

    /**
     * Método que se encarga de transferir los datos fundamentales para actualizar el ID del tema de la lista de
     * reproducción.
     * @param IDTema: ID del tema que se va a incluir en la lista de reproducción.
     * @param IDLista: ID de la lista a la que se le asignará el cambio.
     */
    public void agregarTemaLista(int IDTema, int IDLista){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        listaReproduccion.actualizarTema(IDLista, IDTema);

    }

    /**
     * Método que se encarga de transferir los datos fundamentales para enlazar un video con una lista de reproducción.
     * @param IDLista: ID de la lista a la que se le asignará el video.
     * @param IDVideo: ID del video que se va a enlazar.
     * @return true si el enlace pudo ser realizado.
     */
    public boolean enlazarVideo(int IDLista, int IDVideo){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.enlazarVideo(IDLista, IDVideo);
    }

    /**
     * Método que se encarga de transferir el ID del usuario y el nombre de la lista para verificar si ya existe una
     * lista registrada con ese mismo nombre para ese usuario.
     * @param IDUsuario: ID del usuario.
     * @param nombreLista: Nombre de la lista de reproducción.
     * @return true si la lista ya existe o false si la lista puede alojarse con ese nombre.
     */
    public boolean validarExistenciaLista(int IDUsuario, String nombreLista){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        String formato = nombreLista.replace(" ", "_").trim();
        return listaReproduccion.validarExistenciaLista(IDUsuario, formato);
    }

    /**
     * Método que se encarga de retornar la información requerida para la tabla que presentará las listas
     * de reproducción.
     * @param IDUsuario: ID del usuario al que le pertenecen las listas.
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    public HashMap<Integer, PlayListModelTable> obtenerListas(int IDUsuario){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.obtenerListas(IDUsuario);
    }

    /**
     * Método que se encarga de obtener el ID de una lista.
     * @param nombreLista nombre de la lista a la que se le consultará su ID.
     * @param IDUsuario del usuario al que le pertenece la lista
     * @return ID de la lista o -1 si la lista no es encontrada.
     */
    public int obtenerIDLista(String nombreLista, int IDUsuario){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.obtenerIDLista(nombreLista, IDUsuario);
    }

    /**
     * Retorna el ID del tema de una lista.
     * @param IDListaReproduccion ID de la lista ligada al tema.
     * @return ID del tema o -1 si la lista no tiene un tema.
     */
    public int obtenerIDTema(int IDListaReproduccion) {
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.obtenerIDTema(IDListaReproduccion);
    }

    /**
     * Método que se encarga de retornar el nombre de los videos enlazados a una lista de reproducción.
     * @param IDListaReproduccion: ID de la lista al que le pertenecen los videos.
     * @return HashMap Collection con los datos o null si hubo alguna falla.
     */
    public HashMap<Integer, String> obtenerNombreVideosEnlazados(int IDListaReproduccion) {
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.obtenerNombreVideos(IDListaReproduccion);

    }

    /**
     * Método que retorna el nombre de las listas que registró un usuario en su cuenta.
     * @param IDUsuario: ID del usuario al que le pertenecen las listas de reproducción.
     * @return ArrayList Collection con los datos o null si hubo alguna falla.
     */
    public ArrayList<String> obtenerNombreListas(int IDUsuario){
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.obtenerNombreListas(IDUsuario);

    }

    /**
     * Método que se encarga de eliminar una lista de reproducción.
     * @param IDUsuario: ID del usuario al que le pertenecen las listas de reproducción.
     * @param nombreLista: Nombre de la lista que se va a eliminar.
     */
    public boolean eliminarLista(int IDUsuario, String nombreLista) {
        IListaReproduccion listaReproduccion = super.MySQLFactory.getListaReproduccionDAO();
        return listaReproduccion.eliminarListaReproduccion(IDUsuario, nombreLista);
    }
}
