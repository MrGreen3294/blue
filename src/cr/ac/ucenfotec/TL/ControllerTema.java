package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.TemaLista.ITemaLista;
import cr.ac.ucenfotec.BL.TemaLista.TemaLista;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller que se encarga de transferir todas las peticiones que estén relacionadas con los temas de las
 * listas de reproducción.
 * @author Pablo Fonseca
 * @version 1.0
 */
public class ControllerTema extends Controller{

    /**
     * Método que se encarga de retornar un conjunto de temas de las listas registradas.
     * @return ArrayList Collection de String con el contenido de los temas.
     */
    public ArrayList<String> obtenerTemas() {
        ITemaLista temaLista = super.MySQLFactory.getTemaListaDAO();
        HashMap<Integer, TemaLista> map = temaLista.obtenerTemas();
        ArrayList<String> arrayList = new ArrayList<>();

        for(Map.Entry<Integer, TemaLista> entry : map.entrySet()){
            arrayList.add(entry.getValue().getNombre());
        }

        return arrayList;
    }

    /**
     * Método que se encarga de retornar el ID del tema proporcionando el nombre del tema.
     */
    public int obtenerIDTema(String nombreTema){
        ITemaLista temaLista = super.MySQLFactory.getTemaListaDAO();
        return temaLista.obtenerIDTema(nombreTema);
    }

    /**
     * Método que retorna el nombre de un tema.
     * @param IDTema: ID del tema que se va a consultar.
     * @return String con el nombre del tema o
     */
    public String obtenerNombreTema(int IDTema){
        ITemaLista temaLista = super.MySQLFactory.getTemaListaDAO();
        return temaLista.obtenerNombreTema(IDTema);
    }

    /**
     * Método que transfiere el contenido de un tema.
     * @param nombre: Nombre del tema que se va a registrar.
     * @param descripcion: Descripci[on del tema que se va a registrar.
     * @return true si el tema pudo ser registrado satisfactoriamente.
     */
    public boolean agregarTema(String nombre, String descripcion){
        ITemaLista temaLista = super.MySQLFactory.getTemaListaDAO();
        return temaLista.agregarTema(nombre, descripcion);
    }

    /**
     * Método que transfiere el nombre de un tema.
     * @param nombreTema: nombre del tema que se va a eliminar.
     * @return true si el tema pudo ser eliminado correctamente.
     */
    public boolean eliminarTema(String nombreTema) {
        ITemaLista temaLista = super.MySQLFactory.getTemaListaDAO();
        return temaLista.eliminarTema(nombreTema);
    }
}
