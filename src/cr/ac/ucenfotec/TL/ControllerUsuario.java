package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.Direccion.Direccion;
import cr.ac.ucenfotec.BL.Usuario.IUsuario;
import cr.ac.ucenfotec.BL.UsuarioFinal.UsuarioFinal;
import cr.ac.ucenfotec.Utilities.CodeGenerator;

import java.time.LocalDate;
import java.util.HashMap;

public class ControllerUsuario extends Controller {

    /**
     * Valida si un usuario ya existe en la base de datos.
     * NO SELECCIONA LAS CUENTAS QUE NO ESTÁN ACTIVAS.
     * @param identificacion identificacion del usuario.
     * @return true si el usuario existe en la base de datos.
     */
    public boolean validarExistenciaUsuario(String identificacion){

        boolean existencia;
        assert MySQLFactory != null;
        IUsuario usuarioGeneral = MySQLFactory.getUsuarioDAO();
        existencia = usuarioGeneral.validarExistenciaUsuarioFinal(identificacion);
        return existencia;

    }

    /**
     * Envia la información a la base de datos.
     * @param datos datos contenidos en un Collection de tipo HashMap para crear el objeto.
     * @return el ID autoincremental del usuario registrado. El ID lo genera el motor de la BD.
     */
    public int registrarUsuario(HashMap<String, String> datos){
        int llaveUsuario;
        UsuarioFinal objUsuarioFinal = new UsuarioFinal(
                datos.get("Nombre"),
                datos.get("PrimerApellido"),
                datos.get("SegundoApellido"),
                LocalDate.parse(datos.get("FechaNacimiento")),
                datos.get("Identificacion"),
                datos.get("Avatar"),
                datos.get("ClaveSeguridad"),
                datos.get("NombreUsuario"),
                datos.get("CorreoElectronico")
        );
        //Ahora se enviará el objeto a la base de datos
        IUsuario usuarioGeneral  = MySQLFactory.getUsuarioDAO();
        llaveUsuario = usuarioGeneral.registrarUsuarioFinal(objUsuarioFinal);
        return llaveUsuario;
    }



    /**
     * Se encarga de insertar una dirección para cualquier tipo de usuario, independientemente de su rol.
     * @param datos: datos de la dirección a insertar.
     * @return true si los datos pudieron ser insertados.
     */
    public boolean insertarDireccionUsuario(HashMap<String, String> datos){
        boolean operacionCompletada;
        Direccion objDireccion = new Direccion(
                datos.get("NombreProvincia"),
                datos.get("NombreCanton"),
                datos.get("NombreDistrito")
        );
        int IDUsuario = Integer.parseInt(datos.get("IDUsuario"));
        int IDPais = Integer.parseInt(datos.get("IDPais"));
        int IDProvincia = Integer.parseInt(datos.get("IDProvincia"));
        int IDCanton = Integer.parseInt(datos.get("IDCanton"));
        int IDDistrito = Integer.parseInt(datos.get("IDDistrito"));
        //Ahora se envía la información a la base de datos.
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        operacionCompletada  = usuarioGeneral.insertarDireccion(IDUsuario,
                IDPais,
                IDProvincia,
                IDCanton,
                IDDistrito);
        return operacionCompletada;
    }

    /**
     * Se encarga de generar un código alfanumérico para cualquier tipo de usuario.
     * Luego lo envía a la base de datos para insertarlo.
     * @param IDUsuario: ID del usuario al que se le insertará el código.
     * @return true si el código ha sido generado e insertado satisfactoriamente en la base de datos.
     */
    public boolean insertarCodigoConfirmacion(int IDUsuario){
        String codigoGenerico = CodeGenerator.generateAlphaCode(6);
        //Se envía el código a la base de datos.
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.insertarCodigoConfirmacion(IDUsuario, codigoGenerico);
    }

    /**
     * Se encarga de generar un código alfanumérico para cualquier tipo de usuario.
     * Luego lo envía a la abse de datos para actualizar el código.
     * Este método se utiliza primordialmente para recuperar la clave de seguridad.
     * @param IDUsuario: ID del usuario al que se le actualizará el código.
     * @return true si el código ha sido generado y actualizado satisfactoriamente en la base de datos.
     */
    public boolean actualizarCodigoConfirmacion(int IDUsuario){
        String codigoGenerico = CodeGenerator.generateAlphaCode(6);
        //Se envía el código a la base de datos.
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.actualizarCodigoConfirmacion(IDUsuario, codigoGenerico);
    }

    /**
     * Método que se encarga de actualizar la clave de seguridad de un usuario.
     * @param claveSeguridad: nueva clave de seguridad que reemplazará la vieja.
     * @param IDUsuario: ID del usuario que actualizará su clave.
     * @return true si la actualización pudo ser completada.
     */
    public boolean actualizarClaveSeguridad(String claveSeguridad, int IDUsuario) {
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.actualizarClaveSeguridad(IDUsuario, claveSeguridad);
    }

    /**
     * Se encarga de consultar en la base de datos el código alfanumérico generado para un usuario.
     * @param IDUsuario: ID del usuario al que se le consultará el código.
     * @return el código consultado o null si la operación no pudo ser completada.
     */
    public String consultarCodigoConfirmacion(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerCodigoConfirmacion(IDUsuario);
    }

    /**
     * Se encarga de validar que el código pasado por parámetro sea equivalente al código insertado.
     * @param IDUsuario: ID del usuario al que se le comparará el código.
     * @param codigo: Código a comparar con el de la base de datos.
     * @return true si el estado de la comparación es correcto.
     */
    public boolean validarCodigoConfirmacion(int IDUsuario, String codigo){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.validarCodigoConfirmacion(IDUsuario, codigo);
    }

    /**
     * Activa la cuenta de un usuario.
     * @param IDUsuario ID del usuario al que se le activará la cuenta.
     * @return true si la cuenta pudo ser activada.
     */
    public boolean activarCuentaUsuario(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.activarCuentaUsuario(IDUsuario);
    }

    /**
     * Se encarga de validar los credenciales del usuario.
     * @param nombreUsuario: Nombre del usuario que se va a validar.
     * @param claveUsuario: Clave del usuario que se va a validar.
     * @return true si los credenciales son válidos.
     */
    public boolean validarCredencialesUsuario(String nombreUsuario, String claveUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.validarCredencialesUsuario(nombreUsuario, claveUsuario);
    }

    /**
     * Se encarga de validar si existe un usuario activo que tenga un correo electrónico en la base de datos.
     * @param correoElectronico: Correo electrónico que se valida.
     * @return true si existe un usuario que tenga su cuenta activa y que tenga ese correo electrónico.
     *
     */
    public boolean validarExistenciaCorreo(String correoElectronico){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.validarExistenciaCorreoUsuario(correoElectronico);
    }

    public boolean validarExistenciaApodo(String apodoUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.validarExistenciaApodoUsuario(apodoUsuario);
    }

    /**
     * Se encarga de retornar el ID de un usuario mediante la realización de la consulta con el correo del mismo.
     * El usuario debe estár activo, en caso de que no lo esté el resultado será -1.
     * @param correoElectronico: Correo electrónico con el que se realizará la consulta.
     * @return el ID del usuario o -1 si el usuario no fue encontrado.
     */
    public int obtenerPKUsuarioCorreo(String correoElectronico){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerPKCorreo(correoElectronico);
    }

    /**
     * Se encarga de retornar el ID de un usuario mediante la realización de la consulta con el nombre y la clave del
     * mismo.
     */
    public int obtenerPKUsuario(String nombreUsuario, String claveSeguridad){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerPKUsuario(nombreUsuario, claveSeguridad);
    }

    /**
     * Se encarga de transferir el ID de un usuario a la base de datos.
     * Recibe su correo electrónico o null en caso de que no lo encuentre.
     * @param IDUsuario: ID del usuario al que se le consultará su correo electrónico.
     * @return el correo electrónico o null en caso de que no lo encuentre.
     */
    public String obtenerCorreoElectronicoUsuario(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerCorreoElectronico(IDUsuario);
    }

    /**
     * Se encarga de transferir el ID de un usuario a la base de datos.
     * Recibe su nombre de usuario o apodo o null en caso de que no lo encuentre.
     * @param IDUsuario: ID del usuario al que se le consultará el correo electrónico.
     * @return el nombre del usuario o null en caso de que no lo encuentre.
     */
    public String obtenerApodoUsuario(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerApodoUsuario(IDUsuario);
    }

    /**
     * Se encarga de transferir el ID de un usuario a la base de datos.
     * Recibe su identificación o null en caso de que no la encuentre.
     * @param IDUsuario: ID del usuario al que se le consultará su identificación.
     * @return la identificación del usuario o null en caso de que no la encuentre.
     */
    public String obtenerIdentificacionUsuario(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.obtenerIdentificacion(IDUsuario);
    }

    /**
     * Se encarga de transerir el ID de un usuario a la base de datos.
     * Elimina por completo la cuenta.
     * @param IDUsuario: ID del usuario.
     * @return true si la cuenta pudo se eliminada.
     */
    public boolean eliminarCuentaUsuario(int IDUsuario){
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.eliminarCuentaUsuario(IDUsuario);
    }

    /**
     * Se encarga de transferir el ID del usuario a la base de datos.
     * Elimina por completo los videos del usuario.
     * @param IDUsuario: ID del usuario.
     * @return true si los videos pudieron ser eliminados.
     */
    public boolean eliminarVideosUsuario(int IDUsuario) {
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.eliminarVideosUsuario(IDUsuario);
    }

    /**
     * Se encarga de transferir el ID del usuario a la base de datos.
     * Elimina por completo las listas de reproducción del usuario.
     * @param IDUsuario: ID del usuario.
     * @return true si las listas de reproducción pudieron ser eliminadas.
     */
    public boolean eliminarListasUsuario(int IDUsuario) {
        IUsuario usuarioGeneral = super.MySQLFactory.getUsuarioDAO();
        return usuarioGeneral.eliminarListasUsuario(IDUsuario);
    }
}
