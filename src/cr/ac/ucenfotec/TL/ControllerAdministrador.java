package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.Administrador.IAdministrador;
import cr.ac.ucenfotec.BL.Factory.MySQLServerDAOFactory;
import cr.ac.ucenfotec.TL.Controller;

public class ControllerAdministrador extends Controller{

    /**
     * Valida si existe un usuario administrador.
     * @return true si el usuario ya existe.
     */
    public boolean validarExistenciaAdministrador() {
        IAdministrador administrador = super.MySQLFactory.getAdministradorDAO();
        return administrador.validarExistenciaAdministrador();
    }

    /**
     * Asigna el rol de Administrador para un usuario
     * @param IDUsuario: ID del Usuario al que se hará administrador.
     * @return true si la operación pudo ser completada.
     */
    public boolean asignarAdministrador(int IDUsuario){
        IAdministrador administrador = super.MySQLFactory.getAdministradorDAO();
        return administrador.asignarAdministrador(IDUsuario);
    }

    /**
     * Valida si un usuario es un Administrador o si no lo es.
     * La cuenta debe estar activa, en caso contrario el usuario no existe.
     * @param IDUsuario ID del usuario que se va a consultar.
     * @return true si el usuario consultado sí es un administrador.
     */
    public boolean validarUsuarioAdministrador(int IDUsuario){
        IAdministrador administrador = super.MySQLFactory.getAdministradorDAO();
        return administrador.validarUsuarioAdministrador(IDUsuario);
    }

}