package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.Rol.IRol;

public class ControllerRol extends Controller{
    /**
     * Valida si un rol ya existe en la base de datos.
     * @param valorRol nombre del rol que se va a validar.
     * @return true si el rol existe.
     */
    public boolean validarExistenciaRol(String valorRol){
        boolean existenciaRol;
        assert MySQLFactory != null;
        IRol rol = MySQLFactory.getRolDAO();
        existenciaRol = rol.validarExistenciaRol(valorRol);
        return existenciaRol;
    }

    /**
     * Inserta un rol en la base de datos.
     * @param valorRol nombre del rol que se va a insertar.
     */
    public void insertarRol(String valorRol){
        assert MySQLFactory != null;
        IRol rol = MySQLFactory.getRolDAO();
        rol.insertarRol(valorRol);
    }
}

