package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.CategoriaVideo.CategoriaVideo;
import cr.ac.ucenfotec.BL.Video.IVideo;
import cr.ac.ucenfotec.BL.Video.Video;
import cr.ac.ucenfotec.UI.ApplicationCustomClasses.VideoModelTable;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que se encarga de transferir todas las peticiones que estén relacionadas con los videos.
 */
public class ControllerVideo extends Controller{
    /**
     * Inserta un video en la base de datos
     * @param videoMap map con el contenido requerido para realizar la inserción.
     * @return ID del video que fue insertado en la base de datos.
     */
    public int registrarVideo(HashMap<String, Object> videoMap){

        String nombreVideo = "";
        String descripcionVideo = "";
        String URLVideo = "";

        int calificacionVideo = 1;
        double duracionTotalVideo = .0;

        int IDUsuario = -1;

        if(videoMap.get("NombreVideo") != null)
            nombreVideo = videoMap.get("NombreVideo").toString();
        if(videoMap.get("DescripcionVideo") != null)
            descripcionVideo = videoMap.get("DescripcionVideo").toString();
        if(videoMap.get("CalificacionVideo") != null)
            if(Integer.parseInt((videoMap.get("CalificacionVideo").toString())) > 0)
                calificacionVideo = Integer.parseInt(videoMap.get("CalificacionVideo").toString());
        if(videoMap.get("DuracionTotalVideo") != null)
            if(Double.parseDouble(videoMap.get("DuracionTotalVideo").toString()) > .0)
                duracionTotalVideo = Double.parseDouble(videoMap.get("DuracionTotalVideo").toString());
        if(videoMap.get("IDUsuario") != null)
            IDUsuario = Integer.parseInt(videoMap.get("IDUsuario").toString());
        if(videoMap.get("URLVideo") != null)
            URLVideo = videoMap.get("URLVideo").toString();

        Video videoCapsula = new Video(nombreVideo, descripcionVideo, URLVideo, calificacionVideo);

        videoCapsula.setCalificacion(calificacionVideo);
        videoCapsula.setDuracionTotal(duracionTotalVideo);

        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.insertarVideo(videoCapsula, IDUsuario);
    }

    /**
     * Valida si un video existe en la base de datos consultando sus datos mediante el nombre.
     * @param nombreVideo: Nombre del video que se enviará a la consulta.
     * @param IDUsuario: ID del usuario al que se le consultará el dato.
     * @return true si el video existe.
     */
    public boolean validarExistenciaVideo(int IDUsuario, String nombreVideo){
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.validarExistenciaVideo(IDUsuario, nombreVideo);

    }

    /**
     * Transfiere el ID del usuario a la base de datos.
     * Retorna un HashMap collection con los datos que requiere la tabla o null en caso de que la
     * operación no haya podido ser completada.
     * @param IDUsuario: ID del usuario.
     */
    public HashMap<Integer, VideoModelTable> obtenerVideos(int IDUsuario) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerVideos(IDUsuario);
    }

    /**
     * Lista todos los nombres de los videos asociados a un usuario.
     * @param IDUsuario: ID del usuario al que se le realizará la consulta.
     * @return ArrayList de tipo String con todos los nombres encontrados o null si ocurrió algún inconveniente.
     */
    public ArrayList<String> obtenerNombreVideos(int IDUsuario){
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerNombreVideos(IDUsuario);
    }

    /**
     * Transfiere el nombre de un video a la base de datos.
     * Retorna el URL Relativo del video o null en caso de que la operación no haya podido ser completada.
     * @param IDUsuario: ID del usuario al que se le consultará el dato.
     * @param nombreVideo: nombre del video.
     * @return URL del video.
     */
    public String obtenerURLRelativo(int IDUsuario, String nombreVideo) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerURLRelativo(IDUsuario, nombreVideo);

    }

    /**
     * Transfiere el nombre de un video y el ID del usuario a la base de datos.
     * Se encarga de eliminar un video.
     * @param IDUsuario: ID del usuario al que se le consultará el dato.
     * @param nombreVideo: Nombre del video.
     * @return true en caso de que el video haya sido eliminado.
     */
    public boolean eliminarVideo(int IDUsuario, String nombreVideo) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.eliminarVideo(IDUsuario, nombreVideo);
    }

    /**
     * Actualiza o inserta el campo categoría de un video.
     * @param IDCategoria: ID de la categoría que se va a insertar;
     * @param IDVideo: ID del video al que se le asignará esa categoría.
     */
    public void actualizarCategoria(int IDCategoria, int IDVideo) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        video.actualizarCategoriaVideo(IDCategoria, IDVideo);
    }

    /**
     * Consulta el ID de la categoría de un video.
     * @return ID de la categoría o -1 si no pudo ser encontrada.
     */
    public int obtenerIDCategoria(int IDVideo) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerIDCategoria(IDVideo);
    }

    /**
     * Consulta el ID de un video, retorna -1 si el video no pudo ser encontrado.
     * @param nombreVideo: nombre del video.
     * @param IDUsuario: ID del usuario al que le corresponde ese video.
     * @return ID del video o -1 si el video no pudo ser encontrado.
     */
    public int obtenerIDVideo(String nombreVideo, int IDUsuario) {
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerIDVideo(nombreVideo, IDUsuario);
    }

    /**
     * Se encarga de actualizar el útlimo tiempo de reproducción de un vídeo para que el usuario no tenga que
     * ajustarlo nuevamente.
     * @param ultimoTiempoReproduccion: Último tiempo de reproducción de un video.
     * @param IDVideo: ID del video.
     */
    public void actualizarUltimoTiempoVideo(double ultimoTiempoReproduccion, int IDVideo){
        IVideo  video = super.MySQLFactory.getVideoDAO();
        video.actualizarUltimoTiempoVideo(ultimoTiempoReproduccion, IDVideo);
    }

    /**
     * Se encarga de obtener el último tiempo de reproducción de un video para que el usuario no tenga que
     * ajustarlo nuevamente.
     * @param IDVideo: ID del video del que se obtendrá su último tiempo de reproducción.
     * @return último tiempo de reproducción del video.
     */
    public double obtenerUltimoTiempoReproduccion(int IDVideo){
        IVideo video = super.MySQLFactory.getVideoDAO();
        return video.obtenerUltimoTiempoReproduccion(IDVideo);
    }
}
