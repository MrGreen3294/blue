package cr.ac.ucenfotec.TL;

import cr.ac.ucenfotec.BL.Direccion.IDireccion;

import java.util.HashMap;
import java.util.Map;

public class ControllerDireccion extends Controller{

    /**
     * Lista todos los paises disponibles en la base de datos.
     * @return Array con los paises registrados o null si no hay paises registrados.
     */
    public String[] listarPaises() {

        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        HashMap<Integer, String> paises = direccion.listarPaises();
        String[] paisesArray;
        if (paises == null){
            return null;
        }else {
            paisesArray = new String[paises.size()];
            int index = 0;
            for(Map.Entry<Integer, String> entry : paises.entrySet()){
                paisesArray[index] = entry.getValue();
                index++;
            }
        }
        return paisesArray;
    }

    /**
     * Solicita el ID del país a la base de datos.
     * @param nombrePais nombre del país a consultar.
     * @return el número del ID.
     */
    public int solicitarIDPais(String nombrePais){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.obtenerIDPais(nombrePais);
    }

    /**
     * Lista las provincias disponibles en la base de datos.
     * @param IDPais: ID del país al que se le consultarán sus provincias.
     * @return Array con las provincias registradas o null si no hay provincias registradas.
     */
    public String[] listarProvincias(int IDPais){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        HashMap<Integer, String> provincias = direccion.listarProvincias(IDPais);
        String[] provinciasArray;
        if(provincias == null){
            return null;
        }else{
            provinciasArray = new String[provincias.size()];
            int index = 0;
            for(Map.Entry<Integer, String> entry : provincias.entrySet()){
                provinciasArray[index] = entry.getValue();
                index++;
            }
        }
        return provinciasArray;
    }

    /**
     * Solicita el ID de la provincia a la base de datos.
     * @param IDPais ID del país al que se le consultará el ID de su provincia.
     * @param nombreProvincia nombre de la provincia a consultar.
     * @return el número del ID.
     */
    public int solicitarIDProvincia(int IDPais, String nombreProvincia){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.obtenerIDProvincia(IDPais, nombreProvincia);
    }

    /**
     * Lista los cantones disponibles en la base de datos.
     * @param IDPais: ID del país al que se le consultarán sus cantones.
     * @param IDProvincia: ID de la provincia a la que se le consultarán sus cantones.
     * @return Array con los cantones registrados o null si no hay cantones registrados.
     */
    public String[] listarCantones(int IDPais, int IDProvincia){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        HashMap<Integer, String> cantonesMap = direccion.listarCantones(IDPais, IDProvincia);
        String[] cantones;
        if(cantonesMap == null){
            return null;
        }else{
            cantones = new String[cantonesMap.size()];
            int index = 0;
            for(Map.Entry<Integer, String> entry : cantonesMap.entrySet()){
                cantones[index] = entry.getValue();
                index++;
            }
        }
        return cantones;
    }

    /**
     * Solicita el ID del cantón a la base de datos.
     * @param IDPais: ID del país al que se le consultará el cantón de una de sus provincias.
     * @param IDProvincia: ID de la provincia a la que se le consultará el ID de su cantón.
     * @param nombreCanton: Nombre del cantón al que se le consultará su ID.
     * @return el número del ID.
     */
    public int solicitarIDCanton(int IDPais, int IDProvincia, String nombreCanton) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.obtenerIDCanton(IDPais, IDProvincia, nombreCanton);
    }

    /**
     * Lista los distritos disponibles de la base de datos.
     * @param IDPais: ID del país al que se le consultarán sus distritos.
     * @param IDProvincia: ID de la provincia a la que se le consultarán sus distritos.
     * @param IDCanton: ID del cantón al que se le consultarán sus distritos.
     * @return Array con los cantones registrados o null, en caso de que no se hayan encontrado.
     */
    public String[] listarDistritos(int IDPais, int IDProvincia, int IDCanton) {
        IDireccion direccion = MySQLFactory.getDireccionDAO();
        HashMap<Integer, String> distritosMap = direccion.listarDistritos(IDPais, IDProvincia, IDCanton);
        String[] distritos;
        if(distritosMap == null){
            return null;
        }else{
            distritos = new String[distritosMap.size()];
            int index = 0;
            for(Map.Entry<Integer, String> entry : distritosMap.entrySet()){
                distritos[index] = entry.getValue();
                index++;
            }
        }
        return distritos;
    }

    /**
     * Solicita el ID del distrito a la base de datos.
     * @param IDPais: ID del país al que se le consultará el ID del distrito.
     * @param IDProvincia: ID de la provincia a la que se le consultará el ID del distrito.
     * @param IDCanton: ID del cantón al que se le consultará el ID del distrito.
     * @param nombreDistrito: Nombre del distrito al que se le consultará su ID.
     * @return el número del ID.
     */
    public int solicitarIDDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.obtenerIDDistrito(IDPais, IDProvincia, IDCanton, nombreDistrito);
    }

    /**
     * Transfiere el nombre de un país y lo elimina de la base de datos.
     * @param nombrePais: Nombre del país que se va a eliminar.
     * @return true si el proceso pudo ser completado.
     */
    public boolean eliminarPais(String nombrePais) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.eliminarPais(nombrePais);
    }

    /**
     * Transfiere los datos necesarios para registrar un país en la base de datos.
     * @param nombrePais: Nombre del país.
     * @param codigoNumericoPais: Código numérico que representa al país que se va a registrar.
     */
    public void agregarPais(String nombrePais, String codigoNumericoPais) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.agregarPais(nombrePais, codigoNumericoPais);
    }

    /**
     * Transfiere los datos necesarios para registrar una provincia en la base de datos.
     * @param nombreProvincia: Nombre de la provincia que se va a registrar.
     * @param codigoNumericoProvincia: Código de la provincia.
     * @param IDPais: Código numérico del país al que pertenece la provincia.
     */
    public void agregarProvincia(String nombreProvincia, String codigoNumericoProvincia, int IDPais) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.agregarProvincia(nombreProvincia, codigoNumericoProvincia, IDPais);
    }

    public void eliminarProvincia(String nombrePais, String nombreProvincia) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.eliminarProvincia(nombrePais, nombreProvincia);
    }

    public void agregarCanton(int IDPais, int IDProvincia, String nombreCanton, String codigoCanton) {
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.agregarCanton(IDPais, IDProvincia, nombreCanton, codigoCanton);
    }

    public void eliminarCanton(String nombrePais, String nombreProvincia, String nombreCanton){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.eliminarCanton(nombrePais, nombreProvincia, nombreCanton);
    }

    public void agregarDistrito(int IDPais, int IDProvincia, int IDCanton, String nombreDistrito, String codigoDistrito){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.agregarDistrito(IDPais, IDProvincia, IDCanton, nombreDistrito, codigoDistrito);
    }

    public void eliminarDistrito(String nombrePais, String nombreProvincia, String nombreCanton,
                                 String nombreDistrito){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        direccion.eliminarDistrito(nombrePais, nombreProvincia, nombreCanton, nombreDistrito);
    }

    public boolean validarExistenciaDireccionCompleta(){
        IDireccion direccion = super.MySQLFactory.getDireccionDAO();
        return direccion.validarExistenciaDireccionCompleta();
    }
}
